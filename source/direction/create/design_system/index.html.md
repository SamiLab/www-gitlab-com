---
layout: markdown_page
title: "Category Direction - Design System"
description: "Design Systems at GitLab compliments Design Management by adding the ability to manage, document and version your design system." 
canonical_path: "/direction/create/design_system/"
---

- TOC
{:toc}

## Design System

| | |
| --- | --- |
| Stage | [Create](/direction/create/) |
| Maturity | [Planned](/direction/maturity/) |
| Content Last Reviewed | `2020-08-18` |

## Introduction

<!-- A good description of what your category is. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Please include usecases, personas,
and user journeys into this section. -->

The category of *Design System at GitLab* compliments *[Design Management](https://about.gitlab.com/direction/create/design_management/)* category by adding the ability to manage, document and version your design system. 


Please reach out to PM Christen Dybenko ([E-Mail](mailto:cdybenko@gitlab.com)) if you'd like to provide feedback or ask questions about what's coming.

## What is a Design System?

A Design System is a living, breathing repository that sets the look and feel of a production web app. Quite often they incorporate variables and generate production `.css` from [LESS](http://lesscss.org/) or [SASS](https://sass-lang.com/). 

The simplest way to implement a Design System is to start with an open-source library (containing HTML, CSS, and JS) like [Bootstrap](https://getbootstrap.com/) or [Material UI](https://material-ui.com/). From there you can easily include these libraries in your repository and modify the variables to set colors, spacing, and brand-specific look and feel of the styles.

However, since it's hard to preview your changes to these libraries, most companies require a more robust system that lets them view their changes in a separate repository from the production app. This reduces the risk of putting something live that doesn't work and increases code re-use. 

At GitLab, our Design System static site called [Pajamas](https://design.gitlab.com/) and our components are edited (via code) in [Storybook](https://storybook.js.org/) in the [GitLabUI repo](https://gitlab-org.gitlab.io/gitlab-ui/?path=/story/*).

The best Design Systems allow the creation of components in their smallest format - for example, a button (atom) or a more complex layout like a table header (molecule). Code is re-used between atoms, molecules, organisms, templates, and pages for consistency.

It's also important that the design process for creating these new components in a design tool like Figma are connect to their live counterparts in the repository.  


## Long Term Strategy

`TBD`

## Target Audience and Experience
<!-- An overview of the personas involved in this category. An overview
of the evolving user journeys as the category progresses through minimal,
viable, complete and lovable maturity levels.-->

The Design System category is targeted at [product designers](/handbook/marketing/product-marketing/roles-personas/#presley-product-designer) and [software developers](/handbook/marketing/product-marketing/roles-personas/#sasha-software-developer) who are both primary contributors and consumers. The belief that design systems are just for designers is a main hurdle that teams have to get over. Design systems are not fully adopted when this is the belief. Organizations have to understand that a design system improves the efficiency of their development teams while also improving the overall user experience in order for teams to invest. GitLab can help shape this story

These two personas may interact with [product managers](/handbook/marketing/product-marketing/roles-personas/#parker-product-manager) or [software engineers in test](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#simone-software-engineer-in-test) to get these components ready for the actual app.

The **minimal** user journey will provide designers with the ability to: `TBD`


## What's Next & Why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](/handbook/product/product-processes/#epics-for-a-single-iteration) for the MVC or first/next iteration in
the category.-->

*  Consider why this wouldn't just be a feature of Design Management as a category
*  Evaluate the feature set of [zeroheight.com](https://www.zeroheight.com/) and [Invision DSM (Design System Management)](https://www.invisionapp.com/design-system-manager) 
*  Discuss category with analyst in early September

## Future considerations

`TBD`



## Competitive Landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](//handbook/product/product-processes/#customer-meetings). We’re not aiming for feature parity
with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->


- [Zeroheight](https://www.zeroheight.com/)
- [Invision DSM (Design System Management)](https://www.invisionapp.com/design-system-manager)
- [Storybook](https://storybook.js.org/) and [Chromatic](https://www.chromatic.com/)


What is missed in the current market:

-  Linking components to their repo counterparts



## Market Research
<!-- This section should link or highlight any relevant market research you've done that justifies our
entry into the market for the particular category. -->


Forrester now reports that [50% of companies are using some form of a Design System](https://www.forrester.com/report/Digital+CX+And+Design+Trends+2020/-/E-RES160056). 

A [competitive analysis](https://docs.google.com/document/d/1_OpUpozFjMdRPMM7Wd6R9SPJO2PGOuyKCCW-miGDkEE/edit#) *is in progress to define players in the design system market*.

## Business Opportunity
<!-- This section should highlight the business opportunity highlighted by the particular category. -->

Expert from [Design Management Direction](https://about.gitlab.com/direction/create/design_management/#business-opportunity)
> The total market potential of Design Tools is over US $4 billion and growing. There is a significant opportunity for an application that can successfully engage developers and design teams before and during the DevOps lifecycle. If GitLab integrates with the major design tools such as InVision (5,000,000 users), Sketch (1,000,000 users,) and Figma (1,000,000 users) and adds value in the form of visual developer handoffs, we believe product quality will increase.


## Analyst Landscape
<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

`TBD`

## Top Customer Success/Sales issue(s)
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

No customer issues yet, because the feature is so new

## Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

`TBD`

## Top internal customer issue(s)
<!-- These are sourced from internal customers wanting to [dogfood](/handbook/values/#dogfooding)
the product.-->

`TBD`

## Top Vision Item(s)
<!-- What's the most important thing to move your vision forward?-->

`TBD`
