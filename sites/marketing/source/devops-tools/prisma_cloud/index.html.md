---
layout: secure_and_defend_competitors
title: "GitLab vs Prisma Cloud"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

## Summary
   - minimal requirement <-- comment. delete this line
## Strengths
## Weaknesses
## Who buys and why
## Comments/Anecdotes
   - possible customer issues with product  <-- comment. delete this line
   - sample benefits and success stories  <-- comment. delete this line
   - date, source, insight  <-- comment. delete this line
## Resources
   - links to communities, etc  <-- comment. delete this line
   - bulleted list  <-- comment. delete this line
## FAQs
 - about the product  <-- comment. delete this line
## Integrations
## Pricing
   - summary, links to tool website  <-- comment. delete this line
### Value/ROI
   - link to ROI calc?  <-- comment. delete this line
## Questions to ask
   - positioning questions, traps, etc.  <-- comment. delete this line
## Comparison
   - features comparison table will follow this <-- comment. delete this line

<!------------------Begin page additions below this line ------------------ -->

## On this page
{:.no_toc}

- TOC
{:toc}

<div class="comparison-table comparison-page-content secure-and-defend" markdown="1">

## Summary

Twistlock was recently [acquired](https://www.paloaltonetworks.com/company/press/2019/palo-alto-networks-completes-acquisition-of-twistlock) by Palo Alto and was subsequently rebranded as Prisma Cloud.  Prisma Cloud plays in several categories that overlap with GitLab.  [Pricing](https://aws.amazon.com/marketplace/pp/B07QDBGS6L?qid=1591116481430&sr=0-1&ref_=srh_res_product_title) is based on the number of “workload” (aka. pods) that are protected.

Palo Alto’s Prisma Cloud product provides lifecycle security for containerized environments, “from pipeline to perimeter”. Prisma Cloud capabilities include runtime defense, vulnerability management, cloud native firewalls, and pre-built compliance templates for HIPAA, PCI, GDPR, and NIST SP 800-190. It can be integrated into your CI/CD pipeline. Automated and custom policies can block builds or deployments based on vulnerabilities or compliance requirements. Runtime capabilities were recently expanded from only containerized applications to include VMs.

## Comparison to GitLab

Prisma Cloud's runtime and container security features are robust, but they do not offer the breadth of GitLab’s security scans. GitLab Ultimate automatically includes broad security scanning with every code commit including Static and Dynamic Application Security Testing, along with dependency scanning, container scanning, and license management.  Prisma Cloud is also expensive, while much of the current feature set that GitLab provides is available for free.  Additionally, the heavy operational maintenance burden of Prisma Cloud further adds to the cost.  If what GitLab provides today can be considered ‘good enough’, then customers can potentially save a huge amount of money.

## Security Scanning

Prisma Cloud is a decent choice for customers that only need basic vulnerability scanning; however, their vulnerability management tool only intakes data from a single, limited source: known CVEs.  This leaves them blind to other vulnerabilities that may be identified through SAST or DAST scans.  For customers to properly secure their applications, they should consider a solution that includes good SAST and DAST scanners.  Rather than using separate scanners to meet their needs, it will be much simpler and easier to use GitLab, which both has a wide range of scanning capabilities, a native integration with SCM, and has been recognized in the Gartner Magic Quadrant for Application Security Testing (AST).

### Strengths and Weaknesses

</div>
<div class="comparison-table comparison-page-content secure-and-defend strengths-and-weaknesses" markdown="1">

| | <b>GitLab</b> | <b>Prisma Cloud</b> |
| --- | --- | --- |
| <b>Strengths</b> | <span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Integrated security as part of DevOps workflow for all developers</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;High-quality container security by leveraging all the latest feeds for vulnerabilities</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Security leadership by being a CVE Numbering Authority</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;End-to-end DevOps offering from SCM to CI to CD to Security and more</span> | <span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Provides a basic analysis of installed packages vs. known CVEs</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nice, clean UX and design</span> |
| <b>Weaknesses</b> | <span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Requires users to use GitLab for CI if they are not already</span> | <span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Does not provide a full suite of code scanning to adequately detect all vulnerabilities - no SAST or DAST</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Shifting left and integrating with SCM tools requires an integration to be built with their APIs and does not exist natively</span> |

</div>
<div class="comparison-table comparison-page-content secure-and-defend" markdown="1">

### Feature Lineup

</div>
<div class="comparison-table comparison-page-content secure-and-defend feature-lineup" markdown="1">

| | <b>GitLab</b> | <b>Prisma Cloud</b> |
| --- | :-: | :-: |
| SAST | &#9989; | |
| DAST | &#9989; | |
| SCA: Vulnerability Scanning | &#9989; | &#9989; |
| SCA: Open Source Audit | &#9989; | |
| Fuzz Testing | &#9989; | |

</div>
<div class="comparison-table comparison-page-content secure-and-defend" markdown="1">

## Vulnerability Management

Additionally, using Prima Cloud requires customers to integrate a separate product into their CI/CD pipeline jobs.  Customers can save time and money by instead using GitLab's built-in vulnerability management capabilities that come available out-of-the-box.  For customers who do decide to use Prisma Cloud, it is possible to feed their scan results into GitLab and combine them with the results from other GitLab scans.

### Strengths and Weaknesses

</div>
<div class="comparison-table comparison-page-content secure-and-defend strengths-and-weaknesses" markdown="1">

| | <b>GitLab</b> | <b>Prisma Cloud</b> |
| --- | --- | --- |
| <b>Strengths</b> | <span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;One tool - vulnerability management is integrated out-of-the-box</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Visualizes data from all of GitLab’s scanning engines, including DAST, SAST, and SCA</span> | <span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Capable of enforcing policy rules and preventing vulnerable code from running</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Good visualization of a complex risk analysis</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Capable of showing CVE severity together with the container’s actual exposure to the specific attack</span> |
| <b>Weaknesses</b> | <span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Although the roadmap is robust, current functionality is new and lacks features</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Risk assessment capabilities do not exist and priorities do not take into context the configuration of the  container, which could mitigate some vulnerabilities</span> | <span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lacks good scanners to identify vulnerabilities</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Not natively integrated with SCM tools - an API is available but the integration has to be built into CI/CD</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Limited to containerized applications</span> |

</div>
<div class="comparison-table comparison-page-content secure-and-defend" markdown="1">

### Feature Lineup

</div>
<div class="comparison-table comparison-page-content secure-and-defend feature-lineup" markdown="1">

| | <b>GitLab</b> | <b>Prisma Cloud</b> |
| --- | :-: | :-: |
| Vulnerability Assessment | &#9989; | limited |
| Risk Assessment | | &#9989; |
| Vulnerability Prioritization | limited | limited |

</div>
<div class="comparison-table comparison-page-content secure-and-defend" markdown="1">

## Container Security

As GitLab's container security capabilities are relatively new, Prisma Cloud has a much more robust feature/functionality set than GitLab in securing containerized workloads.  Additionally, Prisma Cloud is capable of protecting serverless code as well as virtual machines.  GitLab's container security roadmap is robust and the feature set is maturing quickly.  Additionally, all of the base security capabilities are available in GitLab's free, Core tier.

### Strengths and Weaknesses

</div>
<div class="comparison-table comparison-page-content secure-and-defend strengths-and-weaknesses" markdown="1">

| | <b>GitLab</b> | <b>Prisma Cloud</b> |
| --- | --- | --- |
| <b>Strengths</b> | <span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Current functionality provides a respectable baseline of security (Web application firewall, container Network Policies, container host monitoring)</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Base security capabilities are currently available in the free Core tier</span> | <span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Extensive set of features and capabilities</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&quot;Radar&quot; capability gives a wow factor in visualizing the network</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Protection for VMs, containers, and serverless code</span> |
| <b>Weaknesses</b> | <span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Although the GitLab roadmap is robust, current functionality is new and lacks features</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Current capabilities are supported in containerized environments only</span> | <span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pricing model can get expensive fast with lots of containers</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Behind the nice UX, the solution is hard to manage and can require a lot of time from a dedicated team</span> |

</div>
<div class="comparison-table comparison-page-content secure-and-defend" markdown="1">

### Feature Lineup

</div>
<div class="comparison-table comparison-page-content secure-and-defend feature-lineup" markdown="1">

| | <b>GitLab</b> | <b>Prisma Cloud</b> |
| --- | :-: | :-: |
| Network Firewall | &#9989; | |
| Machine Learning | | &#9989; |
| Active Vulnerability Scanning (of an application running in production) | | &#9989; |
| Malware Scanning | | &#9989; |
| Exploit Protection | | &#9989; |
| File Integrity Monitoring | &#9989; | &#9989; |
| Application/Binary Allow Listing | &#9989; | &#9989; |
| Log Monitoring | &#9989; | &#9989; |
| Compliance | | &#9989; |
| Identity and Access Management | | &#9989; |
| Active Response / Blocking | &#9989; | &#9989; |
| Virtual machine Support | | &#9989; |
| Kubernetes/Container Support | &#9989; | &#9989; |
| Serverless Support | | &#9989; |

</div>
<div class="comparison-table comparison-page-content secure-and-defend" markdown="1">

## GitLab-Prisma Cloud Comparison Infographic
This summary infographic compares Prisma Cloud and GitLab across several DevOps Stages and Categories.

![GitLab Prisma Cloud Comparison Chart](/devops-tools/prisma_cloud/GitLab_PrismaCloud.jpg)

</div>