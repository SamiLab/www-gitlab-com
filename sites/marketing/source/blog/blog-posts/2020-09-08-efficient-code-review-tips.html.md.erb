---
title: "Tips on how to become a more efficient Code Reviewer"
author: Phil Hughes
author_gitlab: iamphill
author_twitter: iamphill
categories: unfiltered
image_title: '/images/blogimages/gitlab-values-cover.png'
description: "My tips and tricks for how I review code at GitLab efficiently"
tags: frontend, engineering, code review
---

{::options parse_block_html="true" /}

<i class="fab fa-gitlab" style="color:rgb(107,79,187); font-size:.85em" aria-hidden="true"></i>&nbsp;&nbsp;
This blog post is [Unfiltered](/handbook/marketing/blog/unfiltered/#legal-disclaimer)
&nbsp;&nbsp;<i class="fab fa-gitlab" style="color:rgb(107,79,187); font-size:.85em" aria-hidden="true"></i>
{: .alert .alert-webcast}

Reviewing code at GitLab is a major part of our workflow. Any code that gets written needs to get code reviewed. Luckily anyone can review at GitLab so getting reviews is quite quick. Since I have been a maintainer for 3 years I have come up with a good and efficient routine that works for me, that allows me to review and merge code quickly and efficiently to aid in others not being blocked by me.

# How?

Just as a foreword - these tips work great for me. My whole workflow is based around these tips. However, whilst they may work for me, they may not work well for others.

## 1. Time management

My daily routine involves an early wake up call to take our dog for a walk and by the time I have finished this I feel productive and energised for the day ahead. This time is great for me to start reviewing merge requests. I set myself a time, usually first thing in my day, to start reviewing and I will keep reviewing merge requests until my GitLab To-Do List no longer has any merge requests that need reviewing. Mornings work for me, it's the time of the day when I can focus the most and get the reviews done with minimal distractions.

Getting to reviews after this time is hard, I have other work that needs doing as well so I normally say to myself that once I reviewed all merge requests in my To-Do List, then I will leave any new merge requests until the next day. As with all rules, it ends up getting broken. **Depending on the size of merge requests, I may make sure I review them before my day ends to make sure anyone in other timezones aren't blocked by me.**

## 2. Unblock others first

That leads me onto my next point of unblocking others first. 

It's not great for the author of a merge request to have to wait X hours/days before they get feedback. The sooner they get feedback, the sooner the merge request can be merged and shipped. Making authors wait just creates uncertainty and may mean that other work gets held up.

This is why I find it important for me to review a merge request as quickly as possible. At GitLab we have a [2  day Service Level Objective (SLO)](/handbook/engineering/workflow/code-review/#first-response-slo) for feedback from reviewers. For myself, I always try to do better than that and respond within a day.

## 3. Focus on the code, not the feature

This is going to be a point that could create a lot of discussion: Instead of focusing on the feature, focus on the code.

A lot of the merge requests I review are across different groups, with features that I don't fully understand or for features I have no way to test. I could spend a lot of my time reading into the feature and the issue to understand what it is, but that means I will be spending more time away from reviewing everyone else's code. Also, if I did this with **every** merge request, it would be hard for me to keep to my time limit for reviewing merge requests.

Who is better to review the feature itself then? The Product Designer (UX) or the Product Manager. Both of these understand the feature being worked on and are better suited to help find bugs or guide the feature in the correct way. It is important that someone in the UX team review the feature to make sure that it matches the designs and vision they had created for the feature. If a merge request has no UX review by the time I get to reviewing it, I will normally ask the author (or ask a Product Designer myself) to have the UX reviewed _before_ I merge the merge request.

However, this point is also something I don't _always_ stick to. If a merge request is touching an area that I am familiar with and I can tell from the code that a bug exists, I will test it locally and provide as much feedback as I can to help the author understand the bug. The more you&mdash;as a reviewer&mdash;work with the code, the easier finding bugs through the code becomes. I have been working on the GitLab codebase for over 4 years, so seeing where bugs could arise through looking at the code has become natural to me.

## 4. Seek to understand: Ask questions

It is easy to suggest changes to the code that I am reviewing, however sometimes what I suggest may not be right. It is important that instead of just suggesting a change, you always try to ask if the author thinks it is the right change. Having a conversation around a change helps both the reviewer and the author understand the existing code as well as the code being suggested. Maybe the suggestion had already been tried by the author. Being open to talk about it helps get to the final solution.

Sometimes however suggestions for changes happen around legacy code, code that has existed for a long time without being updated to match our documentation. In these cases, the conclusion may end up being that a technical debt issue be created. This is ok. We should strive for [boring solutions](/handbook/values/#boring-solutions) first but also understand that a more optimal solution may be required in the future.

## Conclusion

Reviewing code efficiently is a skill that gets learnt the more you do it. Spending time coming up with a workflow that works for yourself is just as important. Over the years I have been reviewing code, I have stuck to these tips as closely as possible. Yet, I am far from perfect, I am constantly learning about new and different ways to optimise my workflow for code review. I would love to hear other tips and workflows. It is through discussions that we can improve and push ourselves to be the best that we can be.
