---
layout: markdown_page
title: "Product Development Flow"
canonical_path: "/company/team/structure/working-groups/product-development-flow/"
description: "Learn more about the Product Development Flow including problems to solve, business goals, roles and responsibilities."
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property        | Value           |
|-----------------|-----------------|
| Date Created    | August 31, 2020 |
| Target End Date | October 30, 2020 |
| Slack           | [#wg_product_development_flow](https://gitlab.slack.com/archives/C019KM43H4K) (only accessible from within the company) |
| Google Doc      | [Working Group Agenda](https://docs.google.com/document/d/10pCsqXVXakRKYVKPmouX7C5g7T_1kBsufLRcN6nGZQI/edit#) (only accessible from within the company) |
| Docs      | TBD |
| Epics/Issues   | https://gitlab.com/groups/gitlab-com/-/epics/938 |
| Associated KPIs/OKRs | TBD |

## Problems To Solve

For more details please reference the [product development flow survey slides](https://about.gitlab.com/handbook/product-development-flow/).

**Doesn’t Encourage Cross-Functional Collaboration**
- How can we promote optimal cross-functional involvement in the product development flow (e.g. where would we benefit from having TWs be informed earlier or where can QEs participate earlier)?
- How can we ensure tracks and phases don’t silo activities and continually encourage cross-functional contribution #everyonecancontribute?

**Too Many Steps**
- How can we reduce the number of “required” steps in the product development flow  without losing key steps needed for convergence points (such as workflow states and release notes aggregation)? 
- How can we better enable teams to work efficiently by providing the right amount of details in tandem with opportunity for personalization for their specific needs (e.g. an activities/outcomes model rather than just a step by step)?

**Too Prescriptive**
- How can we reduce verbosity in the product development flow (e.g. provide a better balance of visual snapshots and written descriptions)?
- How do we better focus teams on delivering value by enabling them to strike their own balance between when to build new features and when to improve existing features (e.g. detail out the Improve phase in an alternate style as a test run)?


## Business Goals
Iterate on the product development flow to shift away from a "step by step" and more toward being a container/playbook of strategies/tactics to provide teams with:

1. Various activities/outcomes to employ at each phase to identity what to build or improve to solve a problem.

2. Minimal, required processes teams need to follow at each phase to maintain efficient, transparent and predictable workflow cross-functionally.


### Exit Criteria


##### TBD - to be aligned upon by working team members upon first meeting.


## Roles and Responsibilities

| Working Group Role    | Person                | Title                          |
|-----------------------|-----------------------|--------------------------------|
| Executive Sponsor     | Anoop Dawar | VP, Product Management |
| Executive Sponsor     | Christie Lenneville | VP of UX |
| Facilitator           | Farnoosh Seifoddini | Principal PM, Product Operations |
| Functional Lead       | Wayne Haber (Engineering) | Director Engineering, Threat Management |
| Functional Lead       | Vincy Wilson (QE) | Manager, Quality Engineering - Growth & Defend |
| Functional Lead       | Jackie Bauer (UX/Product Design) | UX Manager, Enablement & Growth |
| Functional Lead       | Jeff Crow (UX Research) | Senior UX Researcher, Growth|
| Functional Lead       | Michael Karampalas (Product Management) | Principal PM, Growth |
| Functional Lead       | Justin Farris (Product Management) | GPM, Plan |
| Functional Lead       | Craig Norris (Technical Writing) | Technical Writing Manager |
| Member                | Nadia Udalova (Dev) | Product Design Manager |
| Member                | Keanon O'keefe (Plan) | Senior Product Manager |
| Member                | Tim Hey (Growth) | Principal Product Manager |
| Member                | Eric Schurter (Create) | Senior Product Manager |
| Member                | Mark Wood (Plan) | Senior Product Manager |
| Member                | Sarah Waldner (Monitor) | Senior Product Manager |
| Member                | Kai Armstrong (Create) | Senior Product Manager |
| Member                | Fabian Zimmer (Geo) | Senior Product Manager |
| Member                | Daniel Croft (Release, Package) | Senior Manager, Engineering |
| Member                | Jason Yavorska (CI/CD) | Director Product Management |
| Member                | Thiago Figueiró | Engineering Manager, Threat Management |
| Member                | TBD (Data) | TBD |


## Meetings

Meetings are recorded and available on
YouTube in the [Working Group - Product Development Flow](https://www.youtube.com/playlist?list=PL05JrBw4t0KoBbb2mGv4EYvr8tKKapb_6) playlist. Due to the subject matter of this working group and the high probability that every synchronous meeting will discuss sensitive customer information, the playlist is private and accessible by GitLab team members only.
