---
layout: markdown_page
title: Free Tier CI/CD Minutes Updates FAQ
description: "On this page you can view frequently asked questions for updates to the free tier CI/CD minutes limits"
canonical_path: "/pricing/faq-consumption-cicd/"
---

# Customer FAQ - Free Tier CI/CD minute updates
{:.no_toc}

### On this page
{:.no_toc}

{:toc}
- TOC

## What's new

### Overview

**Q. What is GitLab updating?**

A. For the free tier of GitLab.com, GitLab is updating the CI/CD minutes limit to 400 minutes per group, with the ability to purchase an annual subscription of additional CI/CD minutes at $10 for 1000 minutes. Please note this change is only applicable to our Free Tier SaaS offering and does not impact the Self Managed offering.

**Q. Which GitLab subscribers are impacted by this change?**

A. This change is applicable only to GitLab.com Free tier users. Same as today, the limits are applicable to both public and private projects on GitLab.com.

**Q. Why is GitLab reducing CI/CD Minutes to 400 minutes per month for free users?**

A. At GitLab, we’ve been actively working towards empowering our community to make DevOps a reality for teams of all sizes. We’ve constantly [moved features down](https://about.gitlab.com/blog/2020/03/30/new-features-to-core/) to our free product to enable more users to benefit from it.

As a result, the usage of GitLab has grown significantly over time to almost 6 million free users. While we are excited by this exponential growth, our underlying costs to support this growth have increased significantly as well. To ensure we can continue to maintain our commitment to offer a free single platform for your DevOps success, we are updating the CI/CD minutes limit for free tier users on GitLab.com.

**Q. How will GitLab for Education, Open Source, and Startups subscriptions be impacted?**

Members of our [GitLab for Open Source](https://about.gitlab.com/solutions/open-source), [GitLab for Education](https://about.gitlab.com/solutions/education), and [GitLab for Startups](https://about.gitlab.com/solutions/startups) programs will not be impacted by this change and will continue to enjoy [GitLab Gold Plan](https://about.gitlab.com/pricing/) benefits. For more information on these programs and how to apply, please visit each program’s page.

### Timeline of changes

**Q. What is the effective date of these changes?**

A. These changes will be effective on October 1, 2020.

## Available today

### Purchasing additional CI/CD Minutes

**Q. How much does it cost to buy additional CI/CD Minutes?**

A. CI/CD Minutes per top-level group (or personal namespace) are $10 per 1,000 minutes and it is valid for one year from the date of purchase. CI/CD minutes purchased do not auto-renew and need to be purchased again separately after the 12 month validity or after consuming your purchased minutes.

**Q. Will I have different CI/CD Minute pricing for Windows and Linux?**

A. No. The pricing is the same regardless of the operating systems.

**Q. How do I purchase additional CI/CD Minutes?**

A. You can purchase subscriptions for CI/CD Minutes in the [Customers portal](https://customers.gitlab.com/), or from our sales team.

### Managing your CI/CD Minutes Usage

**Q. How can I view and manage my CI/CD Minutes usage?**

A. A Group Owner can view CI/CD Minutes usage on the Usage page in your Group settings page. Due to an [existing defect](https://gitlab.com/gitlab-org/gitlab/-/issues/243722), CI/CD minutes for public projects is currently not reflecting in the CI/CD minutes usage.

**Q. How can I reduce the amount of CI/CD Minutes consumed?**

A. There are a few methods to consider to reduce the number of CI/CD Minutes consumed:

- Utilize [interruptible](https://docs.gitlab.com/ee/ci/yaml/#interruptible) to abort out of date pipelines;
- Be more selective about when jobs run, for example setting certain jobs to only: run when certain files are changed using [only:changes](https://docs.gitlab.com/ee/ci/yaml/#onlychangesexceptchanges); and
- Optimize your CI jobs to complete more quickly
- Bring your own runners

Watch this deep dive video on how you can manage your CI/CD Minutes usage.

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/GrO-8KtIpRA" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

**Q. What happens if I hit the CI/CD Minutes allotted limit and forget to purchase additional CI/CD Minutes?**

A. You will not be able to run new jobs until you purchase additional CI/CD Minutes, or until the next month when you receive your monthly allotted CI/CD Minutes.

**Q. Will I be notified before I hit my limit on CI/CD Minutes?**

A. You will receive notification banners in-app when your group has less than 30%, 5% or exceeded your total allotted CI/CD minutes.

<table>
 <tr>
 <td>Percentage Remaining
 </td>
 <td>Users Notified
 </td>
 <td>Notification Type
 </td>
 <td>Frequency
 </td>
 </tr>
 <tr>
 <td>30%
 </td>
 <td>Owners
 </td>
 <td>Banner
 </td>
 <td>Permanent
 </td>
 </tr>
 <tr>
 <td>5%
 </td>
 <td>Owners
 </td>
 <td>Banner
 </td>
 <td>Permanent
 </td>
 </tr>
 <tr>
 <td>0%
 </td>
 <td>Owners
 </td>
 <td>Banner
 </td>
 <td>Permanent
 </td>
 </tr>
</table>

**Q. Can I proactively monitor my CI/CD Minutes usage?**

A. Yes, you can use the [REST API](https://docs.gitlab.com/ee/api/) on GitLab.com to monitor your CI/CD Minutes usage and integrate this into your standard monitoring tools. Here are a few examples for check plugins and Prometheus integrations:

- https://gitlab.com/6uellerBpanda/check_gitlab/-/tree/master#ci-runner-jobs-duration
- https://github.com/mvisonneau/gitlab-ci-pipelines-exporter

**Q. Are CI/CD Minutes used on users/customers' own runners accounted into the quota?**

A. No. The CI/CD Minutes limit is for jobs using GitLab shared runners. Users/Customers can bring their own runners. CI/CD minutes used on their own runners are not accounted into the limit.

## More information

To upgrade to GitLab Bronze, [purchase online](https://customers.gitlab.com/plans) or contact [GitLab Sales](https://page.gitlab.com/ci-minutes.html)

To address your questions and feedback, we have created a space in the [GitLab Community Forum](https://forum.gitlab.com/t/ci-cd-minutes-for-free-tier/40241), which is actively monitored by GitLab Team members and Product Managers involved with this change.
