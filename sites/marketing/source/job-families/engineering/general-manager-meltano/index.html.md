---
layout: job_family_page
title: "General Manager, Meltano"
---

The general manager of Meltano is responsible for building a great team, a great product, and a great user and customer base for the first integrated open source data science product. Meltano is a separate business unit.

## Responsibilities

* Build and lead our [Meltano team](https://gitlab.com/meltano/meltano/blob/master/README.md) with the iterative creation of [Meltano](https://meltano.com/), an open source convention-over-configuration product for data engineering, analytics, business intelligence, and data science.
* Hire and manage marketing and sales leaders after first driving awareness and revenue yourself.
* Build a community of Meltano users that will guide future features.
* Build a community of developers that contribute code and documentation to Meltano.
* Build a community of evangelists that talk about Meltano, share best practices and drive adoption.
* Monitor and analyze critical metrics (visits, conversation rate, incremental revenue, return on investment, cost analysis, return rate, etc.)
* Handle the budget for the Meltano team product development, hiring, sales, and marketing.
* Product planning: compare past and current sales trends to shape future product features. Understand the competition and what feature will drive acquisition and reduce churn.
* Communicate effectively both internally and externally to meet customer needs and expectations.
* Manage Meltano website, documentation, and social channels and make sure everything is public by default.

## Requirements

* Expertise in the field of data science and analytics.
* Bachelor’s degree in a technical field.
* Experience in software product development.
* Proven communication and leadership skills.
* Ability to meet deadlines, iterate and get results
* Have initiative, resourcefulness, self-motivation, grit and a high level of ownership.
* Has seen 2x growth year over year in a team or startup in an executive role.
* Degree in computer science is a plus.
* Masters degree or PhD in a technical field is a plus.
* Knowledge of GitLab, CI/CD, and Kubernetes is a plus.
* Knowledge of Python, SQL, and R is a plus.
* [Leadership at GitLab](https://about.gitlab.com/company/team/structure/#management-group)
* Ability to use GitLab


## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team).

* Qualified candidates receive a short questionnaire from our Recruiting team
* Selected candidates will be invited to schedule a 30min [screening call](/handbook/hiring/#screening-call) with our Recruiting team
* Next, candidates will be invited to schedule a first 45 minute interview with the Staff Developer, Meltano
* Next, candidates will be invited to schedule an interview with the Senior Product Manager, Monitoring & Distribution
* Candidates will then be invited to schedule an additional interview with VP of Engineering
* Finally, candidates may be asked to interview with the CEO
* Successful candidates will subsequently be made an offer via email

As always, the interviews and screening call will be conducted via a video call.
See more details about our hiring process on the [hiring handbook](/handbook/hiring).
