---
layout: job_family_page
title: "Field Marketing"
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/A67lWGfue_U" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Field Marketing Manager

The Field Marketing Manager position is responsible for all regional marketing supporting sales in their specific region.

Field Marketing Manager Regions:

Field Marketing Manager - US-West - PacNorWest is responsible for supporting our US Midwest and Pacific North West sales terrority.

Field Marketing Manager - US-West - Cal is responsible for supporting our US West NorCal, Rockies/SoCal sales terrority.

Field Marketing Manager - US-East is responsible for supporting our US East sales terrority (excluding the East-Central).

Field Marketing Manager - US-East-Central is responsible for supporting the East-Central sales terrority.

Field Marketing Manager - US-Public Sector is responsible for supporting the US-Public Sector sales terrority. Our US-Public Sector Field Marketing Manager position has a separate compensation calculator, therefore has [its own job family page](https://about.gitlab.com/job-families/marketing/field-marketing-manager-public-sector/) with additional detail. 

Field Marketing Manager - EMEA is responsible for supporting our Europe, Middle East, and Africa (EMEA) sales team.

Field Marketing Manager - APAC is responsible for supporting our Asia Pacific (APAC) sales team.

A successful Field Marketing Manager has substantial understanding of sales-focused marketing as well as our audiences of enterprise IT leaders, IT ops practitioners, and developers. They enjoy taking charge of regional marketing programs, detailed planning, proactive communication, and flawlessly delivering memorable marketing experiences that support our sales goals.

### Job Grade

The Field Marketing Manager is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

* Adapt digital and content marketing programs to the needs of a regional sales team.
* Create, expand, and accelerate sales opportunities through regional and account-based marketing execution, within marketing defined strategy.
* Be an advocate for the sales region you support and help the rest of the marketing department understand their priorities.
* Be an advocate for the marketing department and help the sales team you support understand the marketing department's priorities.
* Swag management for sponsored events, GitLab owned events, and in support of the sales department.
* Decision making and discretion regarding event selection and planning in support of regional sales goals.
* Regional event strategy, decision making, and onsite management.
* Event logistics in support of the team. From helping to book space for meetings to making sure the booth is staffed, and making sure every aspect of our events are well organized.

### Requirements
- Past experience delivering, accelerating, and expanding sales pipeline through regional marketing.
- 7+ years of experience.
- Exemplary communication skills without a fear of over communication. This role will require effective collaboration and coordination across internal and external stakeholders.
- Capacity to empathize with the needs and experiences of IT leaders, IT ops practitioners, and developers.
- Extremely detail-oriented and organized, and able to meet deadlines.
- You share our [values](/handbook/values/), and work in accordance with those values.
- A passion and substantial understanding of the developer tools, IT operations tools, and/or IT security markets.
- Experience with supporting both direct sales and channel sales teams.
- [Leadership at GitLab](https://about.gitlab.com/company/team/structure/#management-group)
- Travel up to 50%

### Additional Requirements for EMEA
- Past experience running marketing to develop DACH, BENELUX, Nordics, and/or UK & Ireland.
- Robust understanding of marketing to the automotive, banking, and tech sectors.

## Senior Field Marketing Manager
As Field Marketing Managers progress throughout their career at GitLab or we find the need to hire a Field Marketing Manager who has more years of relevant experience, there will be a need for a Senior Field Marketing Manager position with in the Field Marketing Organization. The Senior Field Marketing Manager will report into the Manager of Field Marketing for which region he/she reports into.

### Job Grade

The Senior Field Marketing Manager is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities
This role includes all of the responsibilities above, plus:
- Be a leader in working cross-functionally to drive the execution of demand generation and field marketing plans, aligning with other areas of marketing.
- Manage relevant agencies and 3rd parties in the execution of programs.
- Be willing to act as a Senior leader on the Field Marketing team, mentoring and guiding Field Marketing Coordinators and Managers.
- Be a leader in building and driving process within the Field Marketing organization.

### Requirements
This role includes all of the requirements above, plus:
- 10+ years of experience building and executing a regional strategy.
- Proven aptitude to build and drive a budget within the territory.
- Experience in enterprise solution demand creation and field marketing.
- Orientation to managing program details.
- Travel up to 50% .

## Staff Field Marketing Manager

### Job Grade

The Staff Field Marketing Manager is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Requirements
This role includes all of the requirements above, plus:
- Exemplary analytical skills and proven aptitude to use data to optimize program performance and inform future strategies.
- Plan and operate in a transparent manner for cross-organizational visibility and be a leader in sharing best practices with other regional field managers

### Responsibilities
This role includes all of the responsibilities above, plus:
- 15+ years of experience building and executing a regional strategy.
- Capacity to easily transition from high level strategic thinking to creative and detailed execution.
- Excellent communicator with proven aptitude to clearly convey ideas and data in written and verbal presentations to a variety of audiences.

## Manager, Field Marketing
As GitLab grows, it will be important to have regional leadership to scale the field marketing activities. The Manager, Field Marketing, is a leadership role responsible for managing field marketers in a specific region, including both commercial and public sector activities. The Manager, Field Marketing will report to the Director of Field Marketing.

### Job Grade

The Manager, Field Marketing is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities
- Manage a team of field marketers to develop and execute a regional demand generation strategy in line with company sales goals.
- Represent field marketing with field sales peers and management.
- Collaborate with senior marketing leaders, finance and sales leaders to create integrated plans and budgets.
- Be a player/coach, executing and managing marketing activities.
- Hire exceptional field marketers who align with GitLab values.

### Requirements
- The manager role includes all of the requirements above, plus:
- 10+ years of marketing experience
- Exemplary communication skills with senior marketing and sales leaders.
- Experience managing direct or cross-functional teams of 3-10 marketers.
- Experience managing a multi-million dollar marketing budget and working with senior finance and marketing leaders on annual budgeting processes.
- Demonstrated track record of driving results and staying within a budget.

#### AMER Specific Requirements 
- Past experience managing a team to develop the AMER region. 

#### APAC Specific Requirements 
- Past experience managing a team to develop the APAC region.

#### EMEA Specific Requirements 
- Past experience managing a team to develop the EMEA region.
- Robust understanding of marketing to verticals with a focus on automotive, banking, and tech sectors.

### Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).

- Qualified candidates will be invited to schedule a [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
- Next, candidates will be invited to schedule a series of 45 minute interviews with the Sr. Director of Revenue Marketing, Director of Field Marketing, regional sales leader, and a seller who he/she would support.
- Depending on location, candidates may meet in person with any of the above.
- Finally, our CEO may choose to conduct a final interview.
- Successful candidates will subsequently be made an offer via email. The total compensation for this role listed in https://about.gitlab.com/job-families/marketing/field-marketing-manager/ is 100% base salary.

## Senior Manager, Field Marketing
In addition to meeting all of the requirements of a Manager, Field Marketing, the Senior Manager will have the following:

### Job Grade

The Senior Manager, Field Marketing is a [grade 9](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities
* Establish, allocate, and track budget down to the sub-region
* Build out process to integrate channel partners into regional campaigns
*  Plan an integral role in building out and operationalizing the work Field Marketing does to support our growing channel business
* Play an integral role in building out Field Marketing’s work with cross-functional integrated campaigns

### Requirements
* 20+ years of experience
* Past experience running a team of 10+ direct reports

## Career Ladder

The next step for individual contributors is to move to the [Manager, Field Marketing](/job-families/marketing/#manager-field-marketing) role or laterally to the [Account Based Marketing Manager](/job-families/marketing/account-based-marketing-manager/) job family.


The next step for  managers of people is to move to the [Director of Field Marketing](/job-families/marketing/director-field-marketing) job family or laterally to the [Account Based Marketing Manager](/job-families/marketing/account-based-marketing-manager/) job family.

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).

- Qualified candidates will be invited to schedule a [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
- Next, candidates will be invited to schedule a series of 45 minute interviews with the Sr. Director of Revenue Marketing, Director of Field Marketing, regional sales leader, and a seller who he/she would support.
- Depending on location, candidates may meet in person with any of the above.
- Finally, our CEO may choose to conduct a final interview.
- Successful candidates will subsequently be made an offer via email. The total compensation for this role listed in https://about.gitlab.com/job-families/marketing/field-marketing-manager/ is 100% base salary.

Additional details about our process can be found on our [hiring page](/handbook/hiring/).
