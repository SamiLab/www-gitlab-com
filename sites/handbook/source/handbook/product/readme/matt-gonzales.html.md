---
layout: markdown_page
title: "Matt Gonzales' README"
---

## Matt's README

**Matt Gonzales, Senior Product Manager** 
Hey there! My name is Matt Gonzales and I'm obsessed with video games, tacos, learning, product management, and startups. In no particular order, these things compromise most thoughts in my day and are what drive me. Yes. Tacos literally drive me some days.

I also want to provide an honest assessment of myself, as best I can, to help others work with me. If you've never worked with me before this page will be especially helpful. If you have or currently work with me, this page will probably be less helpful but maybe there will be some eureka! moments. 

Please feel comfortable opening an MR to add to this page if you feel I've left something out. Like an egregious oversight of how often I wear sweatpants during colder days, of which there's probably 3 in Texas.

## Related pages

- [Content I've published](https://blog.kickbox.com/author/matt/)
- [Rare Twitter activity](https://twitter.com/omnomagonz)
- [My hobbyist coding project](https://gitlab.com/mattgonzales/board) (old; new one coming soon!)
- [React: The framework I'll eventually learn](https://reactjs.org/tutorial/tutorial.html)
- [My startup mentorship activity](https://www.healthwildcatters.com/mentors/)

## About me

I'm really really really ridiculously ~~good loo-~~ pasionate about startups and technology. The singular thing that drives me: improving the quality of life for **people** through the decisions I make as a product management professional. I want to share my excitement and passion with the people I work with and I do everything I can to be as inclusive as possible.

- I'm a San Antonio, TX native now living in Grand Prairie, TX. I like building relationships and bonding with friends over video games.
- I tend to overcommunicate to remove any confusion or doubt, which includes a slightly-more-than-healthy use of emojis.
- I like to communicate casually and whimsically[.](https://www.facebook.com/salttapps/videos/1724697331076268/) Text can be really tough to communicate tone and intent, so my intent is to let you know I don't mean any harm with the things I say.
- Trust is really important to me: both earning *your* trust but also being very quick to trust you. 
- I'm always looking to improve my personal organization skills, so I'm particularly receptive to suggestions on how to find a workflow that helps me be organized.
- My workday ends between 4 - 5pm CST most days and I'm _very_ strict about that. I've made mistakes in the past putting work on a pedestal and suffered personally and professionally because of it.

## How you can help me

- Provide as much context as possible because I'm usually context-switching and it really helps me get up to speed quickly.
- Try to work with me asynchronously before scheduling a call.
- Please use visual aids. I love them! A lack of visuals usually means I'm asking more questions and it makes me feel insecure, like I'm not understanding as well or as fast as I should be.
- Let me know if what I've said is helpful, vague, offensive, or confusing so that I can learn and do better.
- Have a bias for action and do things even if you feel it might be "stepping on my toes". I have short toes - literally and figuratively - and am most comfortable when the people around me feel empowered to take action.
- Give me candid, honest feedback about how we work together. I like to hear when I've done something good or positive, but I also need to know when I've done something bad or negative.

## My working style

- I'm an [INFJ-A](https://www.16personalities.com/profiles/5d75bb9df7f75) with the traits: assertive advocate, diplomat, and confident individualism. 
- I prefer to work asynchronously for work-related tasks and projects, but I love and prefer video calls for non-work: coffee chats, 1:1s, or Friday happy hours.
- I need visual aids and will always ask for them. If a visual aid is unavailable, I prefer our first collaboration focus on creating that artifact to help me understand the topic.
- Work-life balance is important to me and I'm usually completely unavailable after 5pm CST. I do allow email notifications for urgent matters.
- Sometimes I neglect the micro details because I favor the macro ones. I encourage people to tell me when I'm focused on the wrong detail.
- Collaboration is critical to my thought process. I prefer building solutions with others and struggle when I'm left to ideating by myself.
- I have a [low level of shame](https://about.gitlab.com/handbook/values/#low-level-of-shame) - an acquired skill at GitLab :joy: - which may conflict with others; please stop me and tell me if you feel my shame level is _too_ low.
- My preference is to always "Ship it!" and iterate and improve something later. I have a strong bias for action, but please stop me here as well if it becomes too much.

## What I assume about others

- [Positive intent](https://about.gitlab.com/handbook/values/#assume-positive-intent) though I struggle with this when strong or terse language is used without emojis or other diffusing mechanisms.
- We both want the same thing: mutual success; a successful product and company we helped build.
- We trust each other to do our best, bring our best selves to work, and support each other.
- You maintain a work-life balance that should be respected and work is not your sole passion.
- You will ask questions and challenge me so we can collaborate towards a better outcome.
- You are the expert of your domain and will be much more credible than I will be about those topics.

## What I want to earn

- Your respect: so that we can have a solid rapport and do amazing work together.
- Mentorship: on topics I don't have a strong command of. Current top 3 are re-learning javascript (especially React), learning Spanish, and how to hack my productivity with better organization specifically inside of GitLab.
- Trust: so that you feel comfortable questioning or challenging any decisions or statements I make so we can arrive at a better outcome together.
- Success: because we never truly achieve our goals without the help of others and we're only successful when we build each other up.

## Communicating with me

- I prefer async communication above all else. This was an adjustment for me at first, but I've learned I'm far more productive when I have time to process and respond on my own time.
- Synchronous communication is ok when we need to discuss complex topics quickly or walk through demos, prototypes, etc.
- Please always be honest with me in good and bad situations. I really appreciate direct communication.
- Please provide constructive criticism for me only in private.
- I really struggle with imposter syndrome and even small gestures of positive affirmation mean the world to me. I will always reciprocate or even initiate when I feel people do something worth praising, no matter how big or small.
- 1on1 calls are very important for building a strong relationship; I encourage you to schedule a coffee chat or 1on1 meeting with me if it's been a while since we've had quality face-time together.
- Always share your thoughts and opinions. Whether it's a current topic or one you think needs attention, I value your opinion because it's a unique perspective I don't have.

## Strengths/Weaknesses

**Strengths**
- Compliance: I have a strong grasp of legal and regulatory compliance, data security and privacy, and how various departments function in this context particularly.
- Technology: I have a broad understanding of many technology concepts and I'm always trying to learn more.
- Learning: I can generally understand new concepts very quickly, provided I can freely ask a lot of questions of a subject matter expert.
- Problem solving: Identifying pain points and problems has become a natural skill set for me even outside of my job.

**Weaknesses**
- Organization: I struggle with being organized, especially in GitLab issues and epics. I thrive with a whiteboard or pen and paper.
- Imposter syndrome: I have a difficult time coping with imposter syndrome and it's a perpetual challenge for me.
- Jumping ahead: I tend to pursue ideas before they've been fully thought-out because I'm excited or just prefer to learn by "doing".
