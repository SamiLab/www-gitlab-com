---
layout: markdown_page
title: "DevSecOps Message House"
---



| Positioning Statement: | *Bolster security and streamline the SDLC by embedding security into DevOps* |
|------------------------|-------------------------------------------------------------------------|
| **Short Description** | *GitLab automates robust application security scanning, embedded within CI, to help you align your security program to an iterative agile development process.* |
| **Long Description** | As developers push code faster and in smaller increments, you need application scanning that can keep up. GitLab automates robust application security scanning, embedded within CI, to help you align your security program to an iterative agile development process. Out-of-the-box scans include SAST, DAST, Container and Dependency Security scanning all without building pipelines yourself. |


| **Key-Values** | Empower developers to find and fix vulnerabilities before their code is merged with others | Scan every code change before it leaves the developer's hands, simplifying and even automating remediation | Embed security, don't just integrate it |
|--------------|------------------------------------------------------------------|----------|----------|
| **Promise** | Streamline security efforts -  provide clear accountability so developers know exactly what risk they introduced. | Use automation to find and fix security flaws within the developer's native workflow. | A single application for both the developer and security can avoid costly maintenance while providing a single source of truth for better collaboration. |
| **Pain points** | Help developers write secure code - without becoming security experts | Eliminate costly triaging and tracking of vulnerabilities that can be fixed at their source. | Stop managing complex tool chain plug-ins and fragile automation scripts. |
| **Why GitLab** | Security scan results are integrated natively into the developer's merge request pipeline  |Robust security scans embedded within GitLab CI, including SAST, DAST, Dependency, and Container scanning, along with License Compliance and Secrets Detection | The Security Dashboard provides insights security pros need, showing remaining vulnerabilities across projects and/or groups, along with actions taken, by whom and when.  |


| **Proof points** | *[Chorus](https://about.gitlab.com/customers/chorus/) uses GitLab for SAST...*  | [Glympse](https://about.gitlab.com/customers/glympse/) auditors love the transparency GitLab provides.
