---
layout: handbook-page-toc
title: "DM.7.03 - Data Retention and Disposal Policy"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# DM.7.03 - Data Retention and Disposal Policy

## Control Statement

A record retention policy and schedule define data retention and disposal practices to ensure data is properly stored and erased when no longer needed.

## Context

Securely disposing of both electronic and physical media adds a layer of protection from the data being disposed by unauthorized persons. There are several effective, publicly available tools and techniques to recover data from electronic and physical media, including hard drives and shredded paper. This control aims to reduce the risk of data being recovered by unauthorized persons and shows customers, GitLab team-members, and partners we take measures to protect their data even after it's done being used.

## Scope

This control applies to Red and Orange data as defined in the [Data Classification Policy](/handbook/engineering/security/data-classification-standard.html) 

## Ownership

* Control Owner: `IT Ops`
* Process owner(s):
    * IT Ops: `100%`

## Guidance

Certificates or logs of erasure should be maintained in accordance with the [Record Retention Policy](/handbook/legal/record-retention-policy/)

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Data Retention and Disposal Policy issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/-/issues/1696).

Examples of evidence an auditor might request to satisfy this control:

* [Record Retention Policy](/handbook/legal/record-retention-policy/)
* Record Retention Schedule
* Certificate(s) or log(s) of disposal
* Records indicating media is disposed of when appropriate

### Policy Reference

## Framework Mapping

* SOC2 CC
  * CC6.5
