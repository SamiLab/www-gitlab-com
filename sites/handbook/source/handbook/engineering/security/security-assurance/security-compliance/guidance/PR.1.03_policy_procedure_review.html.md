---
layout: handbook-page-toc
title: "PR.1.03 - Policy and Procedure Review"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# PR.1.03 - Policy and Procedure Review

## Control Statement
All employees, as part of the onboarding process, are required to review policies and procedures which are included in the Employee Handbook. The Employee Handbook includes the Code of Conduct and Information Security Policy. 

## Context
We need to demonstrate as part of new hire onboarding that policies and procedures are in place that define the dependencies between business processes, automated control activities, and technology general controls. New hires are required to review and acknowledge these policies and procedures in order to ensure awareness for GitLab's standards of conduct, information security policies and other policies outlined in the employee handbook.

## Scope
This applies to all GitLab, Inc. employees


## Ownership
* PeopleOps: `100%`  


## Guidance
In order to ensure policies and procedures are reviewed including the Employee Handbook, Information Security Policy and Code of Conduct, every new employee at GitLab must acknowledge their review during the onboarding process.

## Additional control information and project tracking
Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Policy and Procedure Review control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/-/issues/1706).

### Policy Reference
* [Onboarding at GitLab](/handbook/people-group/general-onboarding/)
* [Handbook](/handbook/)
* [Code of Conduct](/handbook/people-group/code-of-conduct/)
* [Information Security Policies](/handbook/engineering/security/#resources)

## Framework Mapping

* SOC
  * CC1.1
  * CC5.2
