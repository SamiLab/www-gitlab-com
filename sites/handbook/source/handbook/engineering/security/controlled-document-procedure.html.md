---
layout: handbook-page-toc
title: "Controlled Document Procedure"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Controlled Document Procedure

## Purpose 

GitLab deploys control activities through policies and standards that establish what is expected and procedures that put policies and standards into action. The purpose of this procedure is to ensure that there is consistency in developing and maintaining controlled documents at GitLab utilizing a hierarchal approach for managing legal and regulatory requirements.  

There are two types of documentation at GitLab:

1. Controlled Documents: Formal policies, standards and procedures. 
1. Uncontrolled Documents: Informal runbooks, certain handbook pages, guidelines, blog posts, templates, etc.

Everyone at GitLab is welcomed and encouraged to submit an MR to create or suggest changes to controlled documents at any time. 

## Scope

This procedure applies to all controlled documents developed in support of GitLab's statutory, regulatory and contractual requirements. Uncontrolled documents are dynamic in nature and not in scope of this procedure.

## Roles & Responsibilities:

| Role  | Responsibility | 
|-----------|-----------|
| Security Compliance Team | Responsible for implementing and maintaining Security Policies and oversight of supporting standards and procedures as part of ongoing continuous control monitoring | 
| Security Assurance Management | Responsible for approving changes to this procedure |
| Control Owners | Responsible for defining and implementing procedures to support Security policies and standards | 

## Procedure

### Definitions by Hierarchy

- Policy: A policy is a statement of intent and defines GitLab's goals, objectives and culture. Statutory, regulatory, or contractual obligations are commonly the root cause for a policy’s existence, as such policies are designed to be centrally managed at the organizational level (e.g. Security Compliance Team or Legal & Ethics Compliance Team). 
- Standard: Standards are mandatory actions or rules that give formal policies support and direction. Standards may take the form of technical diagrams. 
- Procedure: Procedures are detailed instructions to achieve a given policy and, if applicable, supporting standard. Procedures are decentralized and managed by process/control owners.

### Creation
At minimum, controlled documents should cover the following key topic areas:

- Purpose: Overview of why the controlled document is being implemented. 
- Scope: What does the controlled document apply to.
- Roles & Responsibilities: Who is responsible for doing what. This should refer to departments or roles instead of specific individuals. 
- Policy Statements, Standards or Procedure: The details.
- Exceptions: Define how exceptions to the controlled document will be tracked.
- References:  Procedure documents should map back to a governing policy or standard, and may relate to one or more procedures or other uncontrolled documentation. 

### Publishing
Creation of, or changes to, controlled documents must be approved by management, or a formally designated representative, of the owning department as defined in the [Code Owners](/handbook/business-ops/data-team/how-we-work/duties/#codeowner) file prior to publishing. 

Most controlled documents will be published to our publicly facing [handbook](https://about.gitlab.com/handbook), however if there is [non public data](/handbook/engineering/security/data-classification-standard.html) included in the documentation it should be published via an *internal facing only* mechanism, e.g. an internal GitLab project or internal only handbook page. Controlled documents should be accessible to all internal team members. 

### Review
Controlled documents are required to be reviewed and approved on a minimum of an annual basis and may be updated ad hoc as required by business operations. 

## Exceptions
Exceptions to controlled documents must be tracked and approved by the controlled document approver(s) via an auditable format. Exception process should be defined in each controlled document.  

Exceptions to this procedure will be tracked as per the [Information Security Policy Exception Management Process](/handbook/engineering/security/#information-security-policy-exception-management-process).

## References
- Parent Policy: [Information Security Policy](/handbook/engineering/security/)
- GCF Control: [SG.1.01 - Policy and Standard Review](/handbook/engineering/security/security-assurance/security-compliance/guidance/SG.1.01_policy_and_standard_review.html)
- [Data Classifiation Standard](/handbook/engineering/security/data-classification-standard.html)
- Current listing of controlled documents: https://gitlab.com/gitlab-com/gl-security/compliance/compliance/-/issues/1934
