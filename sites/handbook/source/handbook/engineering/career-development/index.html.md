---
layout: handbook-page-toc
title: "Engineering Career Development"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## The Three Components of Career Development

There are three important components of developing one's career:

### Structure

Team members who are (or want to be) on track for promotion should be engaged in
a career coaching conversation with their manager. Some basic information about
this process can be found in the [People Ops
handbook](/handbook/people-group/learning-and-development/career-development/#career-mapping-and-development).
Specific coaching plan templates are listed here to help start the conversation:

- [Senior
  Engineer](https://docs.google.com/document/d/11xZpY2RuTldp1g6bHRFYKwwlQjNjYaPquLPo5uD6hrg/edit#)
- (Others to come)

We want to build these documents around the career matrix for Engineering. Since this career
matrix is still being developed, these documents are currently based on the [job family requirements](/job-families/engineering/).

The first career matrix being developed is for [Individual Contributors in Engineering](career-matrix.html).

When a team member is deemed ready for promotion, their manager should follow
the [promotion process outlined by People
Ops](/handbook/people-group/promotions-transfers/).

Remember that the coaching process helps team members understand what they need
to do in order to prepare for a more senior position on the team. The promotion
process documents what an engineer has already done to deserve a more senior
position on the team. The two processes are related, but they are not
substitutes for each other.

### Individual Initiative

Most career opportunities involve stepping into a position of informal or formal leadership. As such, the initiative
of the individual is a necessary component, as well as a qualification. However, for the sake of inclusion we do ask that
managers periodically bring up the possibilities for advancement so that individuals know the avenues available to them
and receive encouragement to pursue them.  Managers and team members should strive to have a career development
conversation at least once a quarter.

### Opportunity

GitLab is a fast-growing startup. As such, there is no shortage of opportunity for advancement along either the
individual contributor or management tracks. We're fortunate that this is the easiest component. And internal
promotions are generally our first option.

Sometimes a position must become available on the management track before a promotion can occur. But at our
rate of growth in 2017 and 2018, it is usually only a matter of 6 months or less before something opens up for a prepared candidate.


## Individual Contribution vs. Management

Most important is the fork between purely technical work and managing teams. It's important that Engineering Managers self-select into that track and don't feel pressured. We believe that management is a craft like any other and requires dedication. We also believe that everyone deserves a manager that is passionate about their craft.

Once someone reaches a Senior-level role, and wants to progress, they will need to decide whether they want to remain purely technical or pursue managing technical teams. Their manager can provide opportunities to try tasks from both tracks if they wish. Staff-level roles and Engineering Manager roles are equivalent in terms of base compensation and prestige. Learn more about what it means to be a Staff-level engineer at GitLab in a [blog post with feedback from some of our Staff Engineers](https://about.gitlab.com/blog/2020/02/18/staff-level-engineering-at-gitlab/).

### Trying the Management Track

It's important that team members interested in the people management track have opportunities to try it out prior to committing themselves. Managers can provide multiple opportunities to Senior and Staff Engineers who want to consider moving into an available or upcoming manager role. Examples include hosting a Group Conversation, acting as the hiring manager for an intern position, or running a series of demo meetings for an important deliverable. These occasions for trying out management functions are useful and can provide good coaching opportunities for the individual looking to move to management. Some engineers may find through these experiences that they are better suited to remain on the technical path, and this gives them the ability to decide before investing a significant amount of time in the transition.

In order to facilitate the transition, we recommend any person moving from an Individual Contributor role to a Engineering Manager role work with their manager to create a focused transition plan. The goal is to provide a concentrated look into the responsibilities and challenges of people management with the understanding that, if the role is not for them, they can return to the IC track. A good example of how to build such a plan can be found in [this article](http://firstround.com/review/this-90-day-plan-turns-engineers-into-remarkable-managers/). Another good resource for when you are ready for the critical career path conversation is [this 30 mins video](https://www.youtube.com/watch?reload=9&v=hMz6QDURQOM&list=PLBzScQzZ83I8H8_0Qete6Bs5EcW3p0kZF&index=6).

### Temporary Management Positions
We create temporary management positions when there is organizational need, and we imprint these in our company org chart. These may be filled by someone who is transitioning into the role, experimenting with the role as they work on determining their career path or, someone who is just filling in while we hire and who is not interested in pursuing an Engineering Manager role long term. This difference should be made explicit with the individual and team members before the temporary role is created. See the [types of temporary roles](#types-of-temporary-roles) for more information on this distinction.

When someone fills a temporary role they are providing a service to the company, and perhaps getting a valuable career development opportunity for themself; so, poor performance against those duties would not result in termination. At worst, the person would return to their prior responsibilities. That does not mean, however, that an individual is immune to termination of their employment, for example if they commit a breach of their prior responsibilities or of the company's Code of Business Conduct & Ethics while in an temporary position.

Once you have been designated as filling a temporary management role, the current manager should update all reports to in BambooHR with a job information change request and create an access level request to grant interim manager access in BambooHR.

Those interested in these roles must:
- show managerial interest
- have experience in the individual contributor role for which they would be managing
- have been employed at GitLab for at least 6 months and thriving in our remote-only context
- exemplify [GitLab Values](/handbook/values)
- be subject to a talent review depending on the length of appointment

Process for selection:
- Upcoming Interim roles will be discussed over the Engineering and Development Staff meetings.
- Leadership can gather interest from their team members for the upcoming interim roles.
- Based on the above criteria the assessment will take place and most suitable team member will be selected for the interim role.

#### Types of Interim Roles
##### Acting Manager
An Acting Manager is someone who occupies a role temporarily and will move back to their original role after a set amount of time or other conditions. An Acting Manager may be experimenting with the role as a part of determining their career development path, or may be filling in for a vacant role while we hire.

##### Interim Manager
Interim Manager positions are for individuals who are pursuing the role long term: the requirement is that, before moving into the role full time, they will make at least one successful hire. The official promotion will not occur before 30 days after that person's hire, so that we can assess whether the hire was truly successful. If the new hire's success is indeterminate at the 30-day mark, then we will continue to review until a firm decision is made. If the new hire is not successful, that does not mean that the Interim Manager cannot eventually move into the full-time role. 

In some cases it may not be practical or headcount planning might not allow for the Interim Manager to make a new hire. In this case the Interim Manager and the interim manager's manager should agree on success criteria based on the requirements of the role. Examples of other success criteria are (multiselect): 
- Successfully pass an interview(s) with the Leader(s) of the department(s) around team management competencies/scorecard for Manager;
- Successfully pass team member relations case role play with the [People Business Partner](/handbook/people-group/#people-business-partner-alignment-to-division);
- Performing career development conversations with direct reports successfully;
- Perform a [Talent Assessment](/handbook/people-group/performance-assessments-and-succession-planning/#talent-assessment-options) with the manager's manager to determine readiness for the role; 
- Gather 360 feedback via Culture Amp for the manager. 

Once the Interim Manager's first new hire has been at GitLab for 30 days, or other agreed success criteria has been met, the Interim Manager can submit a [promotion document](/handbook/people-group/promotions-transfers/#for-managers-requesting-a-promotion-or-compensation-change) to their manager and determine the [compa group](/handbook/total-rewards/compensation/compensation-calculator/#determining) for the new role. The goal of this process is to have a determination made by the interim manager's manager about the promotion to a permanent manager role after the agreed success criteria has been satisfied. Generally we aim to have the interim period not to exceed 4 months before turning into a fulltime role.

## Roles

#### Engineering

```mermaid
  graph LR;
  eng:jbe(Junior Backend Engineer)-->eng:ibe(Intermediate Backend Engineer);
  eng:ibe(Intermediate Backend Engineer)-->eng:sbe(Senior Backend Engineer);

  eng:sbe(Senior Backend Engineer)-->eng:stbe(Staff Backend Engineer);
  eng:stbe(Staff Backend Engineer)-->eng:dbe(Distinguished Backend Engineer);
  eng:dbe(Distinguished Backend Engineer)-->eng:ef(Backend Engineering Fellow);

  eng:sbe(Senior Backend Engineer)-->eng:em(Backend Engineering Manager);
  eng:em(Backend Engineering Manager)-->eng:sme(Senior Manager, Engineering);

  eng:sme(Senior Manager, Engineering)-->eng:de(Director of Engineering);
  eng:de(Director of Engineering)-->eng:sde(Senior Director of Engineering);
  eng:sde(Senior Director of Engineering)-->eng:vpe(VP of Engineering);

  eng:jfe(Junior Frontend Engineer)-->eng:ife(Intermediate Frontend Engineer);
  eng:ife(Intermediate Frontend Engineer)-->eng:sfe(Senior Frontend Engineer);

  eng:sfe(Senior Frontend Engineer)-->eng:feem(Frontend Engineering Manager);
  eng:feem(Frontend Engineering Manager)-->eng:sme(Senior Manager, Engineering);

  eng:sfe(Senior Frontend Engineer)-->eng:stfe(Staff Frontend Engineer);
  eng:stfe(Staff Frontend Engineer)-->eng:dfe(Distinguished Frontend Engineer);
  eng:dfe(Distinguished Frontend Engineer)-->eng:feef(Frontend Engineering Fellow);

  click eng:jbe "/job-families/engineering/backend-engineer#junior-backend-engineer";
  click eng:ibe "/job-families/engineering/backend-engineer#intermediate-backend-engineer";
  click eng:sbe "/job-families/engineering/backend-engineer#senior-backend-engineer";
  click eng:stbe "/job-families/engineering/backend-engineer#staff-backend-engineer";
  click eng:dbe "/job-families/engineering/backend-engineer#distinguished-backend-engineer";
  click eng:ef "/job-families/engineering/backend-engineer#engineering-fellow";
  click eng:em "/job-families/engineering/backend-engineer#engineering-manager";
  click eng:sme "/job-families/engineering/engineering-management/#senior-manager-development";
  click eng:de "/job-families/engineering/backend-engineer#director-of-engineering";
  click eng:sde "/job-families/engineering/backend-engineer#senior-director-of-engineering";
  click eng:vpe "/job-families/engineering/backend-engineer#vp-of-engineering";
  click eng:jfe "/job-families/engineering/frontend-engineer#junior-frontend-engineer";
  click eng:ife "/job-families/engineering/frontend-engineer#intermediate-frontend-engineer";
  click eng:sfe "/job-families/engineering/frontend-engineer#senior-frontend-engineer";
  click eng:stfe "/job-families/engineering/frontend-engineer#staff-frontend-engineer";
  click eng:dfe "/job-families/engineering/frontend-engineer#distinguished-frontend-engineer";
  click eng:feef "/job-families/engineering/frontend-engineer#frontend-engineering-fellow";
  click eng:feem "/job-families/engineering/frontend-engineering-manager";
```

#### Security Engineering

```mermaid
  graph LR;
  sec:se(Security Engineer)-->sec:sse(Senior Security Engineer);
  sec:sse(Senior Security Engineer)-->sec:stse(Staff Security Engineer);
  sec:sse(Senior Security Engineer)-->sec:sem(Security Engineering Manager);
  sec:sem(Security Engineering Manager)-->sec:ds(Director of Security);

  click sec:se "/job-families/engineering/security-engineer/#intermediate-security-engineer";
  click sec:sse "/job-families/engineering/security-engineer/#senior-security-engineer";
  click sec:stse "/job-families/engineering/security-engineer/#staff-security-engineer";
  click sec:sem "/job-families/engineering/security-management#security-engineering-manager";
  click sec:ds "/job-families/engineering/security-management#director-of-security";
```

#### Quality Engineering

```mermaid
  graph LR;
  qual:jset(Junior Software Engineer in Test)-->qual:iset(Intermediate Software Engineer in Test);
  qual:iset(Intermediate Software Engineer in Test)-->qual:sset(Senior Software Engineer in Test);
  qual:sset(Senior Software Engineer in Test)-->qual:stset(Staff Software Engineer in Test);
  qual:sset(Senior Software Engineer in Test)-->qual:qem(Quality Engineering Manager);
  
  qual:ibeep(Intermediate Backend Engineer, Engineering Productivity)-->qual:sbeep(Senior Backend Engineer, Engineering Productivity);
  qual:sbeep(Senior Backend Engineer, Engineering Productivity)-->qual:stbeep(Staff Backend Engineer, Engineering Productivity);
  qual:sbeep(Senior Backend Engineer, Engineering Productivity)-->qual:bemep(Backend Engineering Manager, Engineering Productivity);
  
  qual:bemep(Backend Engineering Manager, Engineering Productivity)-->qual:dqe(Director of Quality Engineering);
  qual:qem(Quality Engineering Manager)-->qual:dqe(Director of Quality Engineering);
  
  click qual:jset "/job-families/engineering/software-engineer-in-test#junior-software-engineer-in-test";
  click qual:iset "/job-families/engineering/software-engineer-in-test#intermediate-software-engineer-in-test";
  click qual:sset "/job-families/engineering/software-engineer-in-test#senior-software-engineer-in-test";
  click qual:stset "/job-families/engineering/software-engineer-in-test#staff-software-engineer-in-test";
  click qual:ibeep "/job-families/engineering/backend-engineer/#engineering-productivity";
  click qual:sbeep "/job-families/engineering/backend-engineer/#engineering-productivity";
  click qual:stbeep "/job-families/engineering/backend-engineer/#engineering-productivity";
  click qual:bemep "/job-families/engineering/backend-engineer/#backend-manager-engineering";
  click qual:qem "/job-families/engineering/engineering-management-quality/#quality-engineering-manager";
  click qual:dqe "/job-families/engineering/engineering-management-quality/#director-of-quality-engineering";
```

#### Support Engineering

```mermaid
  graph LR;
  supe:se(Support Engineer)-->supe:sse(Senior Support Engineer);

  supe:sse(Senior Support Engineer)-->supe:stse(Staff Support Engineer);
  supe:sse(Senior Support Engineer)-->supe:sem(Support Engineering Manager);

  supe:sem(Support Engineering Manager)-->supe:ssem(Senior Support Engineering Manager);
  supe:ssem(Senior Support Engineering Manager)-->supe:ds(Director of Support);

  click supe:se "/job-families/engineering/support-engineer#support-engineer";
  click supe:sse "/job-families/engineering/support-engineer#senior-support-engineer";
  click supe:sem "/job-families/engineering/support-management#support-engineering-manager";
  click supe:stse "/job-families/engineering/support-engineer#staff-support-engineer";
  click supe:ssem "/job-families/engineering/support-management/index.html.md#senior-support-engineering-manager"
  click supe:ds "/job-families/engineering/support-management#director-of-support";
```

#### User Experience

```mermaid
  graph LR;
  eng:pd(Product Designer)-->eng:spd(Senior Product Designer);

  eng:spd(Senior Product Designer)-->eng:stpd(Staff Product Designer);

  eng:spd(Senior Product Designer)-->eng:pdm(Product Design Manager);
  eng:pdm(Product Design Manager)-->eng:spdm(Senior Product Design Manager);
  eng:spdm(Senior Product Design Manager)-->eng:dpd(Director of Product Design);
  eng:dpd(Director of Product Design)-->eng:vpux(VP of User Experience);

  eng:uxrc(UX Research Coordinator)-->eng:uxr(UX Researcher);
  eng:uxr(UX Researcher)-->eng:suxr(Senior UX Researcher);
  eng:suxr(Senior UX Researcher)-->eng:stuxr(Staff UX Researcher);

  eng:suxr(Senior UX Researcher)-->eng:muxr(Manager of UX Research);
  eng:muxr(Manager of UX Research)-->eng:smuxr(Senior Manager of UX Research);
  eng:smuxr(Senior Manager of UX Research)-->eng:duxr(Director of UX Research);
  eng:duxr(Director of UX Research)-->eng:vpux(VP of User Experience);

  eng:tw(Technical Writer)-->eng:stw(Senior Technical Writer);

  eng:stw(Senior Technical Writer)-->eng:sttw(Staff Technical Writer);

  eng:stw(Senior Technical Writer)-->eng:mtw(Technical Writing Manager);
  eng:mtw(Technical Writing Manager)-->eng:stwm(Senior Technical Writing Manager);
  eng:stwm(Senior Technical Writing Manager)-->eng:dtw(Director of Technical Writing);

  click eng:pd "/job-families/engineering/product-designer/";
  click eng:spd "/job-families/engineering/product-designer/";
  click eng:stpd "/job-families/engineering/product-designer/";
  click eng:pdm "/job-families/engineering/ux-management/";
  click eng:spdm "/job-families/engineering/ux-management/";
  click eng:dpd "/job-families/engineering/ux-management/";
  click eng:uxrc "/job-families/engineering/ux-researcher/";
  click eng:uxr "/job-families/engineering/ux-researcher/";
  click eng:suxr "/job-families/engineering/ux-researcher/";
  click eng:stuxr "/job-families/engineering/ux-researcher/";
  click eng:muxr "/job-families/engineering/ux-research-manager/";
  click eng:smuxr "/job-families/engineering/ux-research-manager/";
  click eng:duxr "/job-families/engineering/ux-research-manager/";
  click eng:tw "/job-families/engineering/technical-writer/";
  click eng:stw "/job-families/engineering/technical-writer/";
  click eng:sttw "/job-families/engineering/technical-writer/";
  click eng:mtw "/job-families/engineering/technical-writing-manager/";
  click eng:stwm "/job-families/engineering/technical-writing-manager/";
  click eng:dtw "/job-families/engineering/technical-writing-manager/";
  click eng:vpux "/job-families/engineering/ux-management/";
```

## Internships

A limited [internship pilot](/handbook/engineering/internships/) is launching in 2020. Depending on the success of the pilot we may choose to roll it out further.

## Apprenticeships

Sometimes, a member of the GitLab team might want to experience working in a new role without making a permanent change. If the department they want to learn more about can support this effort, then this offers a good opportunity for an apprenticeship.

Eligibility criteria:
* You are excelling in your role (no performance issues)
* You have been working full time with GitLab for at least 6 months

The apprentice agrees to:

* Continue to support their current role and report to their current manager
* Dedicate a pre-determined percentage of their time to the apprenticeship role
* Have clear timelines and deliverables throughout the apprenticeship
* Communicate their assignments and availability clearly to both departments

In return, the department with which they are apprenticing agrees to:

* Provide guidance and support to the apprentice
* Offer meaningful projects for the apprentice to work on
* Be flexible, so that the apprentice can continue to support their primary team and carry an appropriate workload

By default, apprenticeships last for 6 months. At the end of 6 months, both the apprentice and the department will determine whether it makes sense to extend the timeframe further.

If you wish to apprentice with an Engineering team, start by speaking with your manager.

**Important**: The Apprenticeship program should differ from any situations where there is actual temporary or permanent [realignment/redeployment/secondment](/handbook/people-group/promotions-transfers/#realignment-of-resources-impacting-multiple-team-members) as a result of changing business needs. If you are unsure whether your situation is aligned with the Apprenticeship program or falls into a resources realignment, please discuss with your aligned [People Business Partner](/handbook/people-group/#people-business-partner-alignment-to-division). 

## Junior Engineers

Junior Engineers require a high degree of mentorship to effectively set them up for success. Because GitLab Engineering is scaling so quickly at the moment (100% in 2018, 125% in 2019) we are not hiring at the junior level. A good guideline for what it takes to meet our intermediate criteria is 2 years of professional experience with rapid growth. We encourage you to apply, even if you have questions.

GitLab is committed to increasing diversity of all types, particularly in leadership, as it's one of our [core values](/handbook/values/). There is a misapprehension that the junior role is an effective tool for increasing our diversity. GitLab Engineering has made its greatest gains in diversity since putting a moratorium on junior hiring. This indicates that diversity and the junior level are orthogonal, at best.

Before this level is re-opened we need to assess the following attributes:

* Is a structured, time-bounded internship program a better alternative to the junior level
* Either stop scaling as a whole, or specific teams must meet a criteria (size? maturity? composition?) to mentor a junior
* Change the process so junior positions are advertised for, rather than leveled on-the-fly
* Consider changing the name of the level, since "junior" is not an effective recruiting title
* How do we effectively mentor juniors at a remote-only company?
* There is a confidential item in line with our [not public guidelines](/handbook/communication/#not-public)

## Senior Engineers

Note that we have a specific section for [Senior Engineer](/job-families/engineering/backend-engineer/#senior-backend-engineer) because it's an important step in the technical development for every engineer. But "Senior" can optionally be applied to any role here indicating superior performance. However, it's not required to pass through "senior" for roles other than Engineer.

Senior engineers typically receive fewer trivial comments on their merge requests. Attention to detail is very important to us. They also receive fewer _major_ comments because they understand the application architecture and select from proven patterns. We also expect senior engineers to come up with simpler solutions to complex problems. Managing complexity is key to their work. [Staff](/job-families/engineering/backend-engineer/#staff-backend-engineer) and [Distinguished](/job-families/engineering/backend-engineer/#distinguished-backend-engineer) positions extend the Senior Engineer role.


## Promotion

We strive to set the clearest possible expectations with regard to performance and [promotions](/handbook/people-group/promotions-transfers/). Nevertheless, some aspects are qualitative. Examples of attributes that are hard to quantify are communication skills, mentorship ability, accountability, and positive contributions to company culture and the sense of psychological safety on teams. For these attributes we primarily rely on the experience of our managers and the [360 feedback](/handbook/people-group/360-feedback/) process (especially peer reviews). It's our belief that while a manager provides feedback and opportunities for improvement or development, that it's actually the team that elevates individuals.

#### Transfer Options

The following table outlines some of the possible lateral transfer options at any level of the role, but don't feel limited by this table. Compa Ratio might differ per individual to determine leveling for each of the positions listed.

| Starting Role       | Lateral Options           |
|---------------------|---------------------------|
| Frontend Engineer   | Product Designer          |
| Product Designer    | Frontend Engineer         |
| Backend Engineer    | Production Engineer       |
| Production Engineer | Backend Engineer          |
| Backend Engineer    | Support Engineer          |
| Support Engineer    | Backend Engineer          |
| Support Engineer    | Solutions Architect       |
| Support Engineer    | Technical Account Manager |
| Support Engineer    | Implementation Specialist |
| Automation Engineer | Backend Engineer          |
| Backend Engineer    | Automation Engineer       |

Lateral transfers among backend teams are also an option. Those teams include Distribution, Create, Verify, Release, Geo, Monitoring, Gitaly, etc.

## Team members on loan to another team

A team member may be "on loan" to another team for various reasons. This is less formal and handled differently than [interim](#types-of-interim-roles) roles. When this occurs it is recommended to:

* Invite the team member to all Slack channels used by the team. (Don't worry about Slack channel overload. The team member can unsubscribe if they don't find it useful.)
* Invite the team member to any standup activities used by the team. To avoid task overload, the team member can choose to stop participating in their home team standup.
* The manager of the team should schedule weekly 1-1 meetings with the on-loan team member. The team member can choose to stop having weekly 1-1s with their home team manager.

Team members who were on loan to another team documented these best practices in a retrospective.
