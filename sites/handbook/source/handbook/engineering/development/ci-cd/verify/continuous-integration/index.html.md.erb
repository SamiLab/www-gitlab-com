---
layout: handbook-page-toc
title: "Verify:Continuous Integration Group"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Vision

For an understanding of what this team is going to be working on take a look at [the product
vision](/direction/verify/) and the [Category direction of Continuous Integration](https://about.gitlab.com/direction/verify/continuous_integration/)

## Mission

The Verify:Continuous Integration Group is focused on all the functionality with respect to
Continuous Integration.

This team maps to [Verify](/handbook/product/product-categories/#verify-stage) devops stage.

## Performance Indicator

We measure the value we contribute by using Performance Indicators (PI), which we define and use to track progress. The current [PI for the Continuous Integration group](/handbook/product/ops-section-performance-indicators/#verify-ci-verify-runner-count-of-pipelines-triggered-by-unique-users) is the `number of unique users who trigger ci_pipelines`. For more details, please check out the [Product Team Performance Indicators](/handbook/product/performance-indicators/#regular-performance-indicators). To view the latest Verify stage ci_pipeline data see our [Sisense Dashboard](https://app.periscopedata.com/app/gitlab/538594/Verify-Stage-Dashboard).

### Usage Funnel

Based on the AARRR framework (Acquisition, Activation, Retention, Revenue, Referral), this funnel represents the customer journey in using GitLab CI.  Each state in the funnel is defined with a metric to measure behavior. Product managers can focus on any of the various states in the funnel to prioritize features that drive a desired action.

```mermaid
classDiagram
  Acquistion --|> Activation
    Acquistion : Awareness: Are users aware of CI pipelines?
    Acquistion: Measurement (Total count of Pipelines page views)
  Activation --|> Retention
    Activation : Use: Are users creating CI configurations/pipelines?
    Activation: Measurement (Total count of projects with .gitlab-ci.yml files)
  Retention --|> Revenue
    Retention : Continued use: Are users triggering CI pipelines?
    Retention: Measurement (Total count of projects with a .gitlab-ci.yml file and CI pipeline triggered)
  Revenue --|> Referral
    Revenue : Monetary value: Are users paying for the CI features?
    Revenue: Measurement (Total count of projects using paid CI features)
  Referral --|> Acquistion
    Referral : Endorsement - Are user encouraging others to use GitLab CI?
    Referral: Measurement (Total count of Pipelines page views from social media)
```

### Core domain

- Pipeline configuration: YAML syntax, linter and configuration parser.
- Pipeline creation: process of building and persisting a pipeline including multi-project
  or child pipelines.
- Pipeline processing: processes responsible for transitions of pipelines, stages and jobs.
- Rails-Runner communication: jobs queuing, API endpoints and their underlying functionalities related
  to operations performed by and for Runners.
- Job artifacts: storage and management of artifacts is the gateway for many CI/CD features.

## Team Members

The following people are permanent members of the Verify:Continuous Integration group:

### Backend
<%= direct_team(manager_role: 'Backend Engineering Manager, Verify:Continuous Integration') %>

### Frontend
<%= direct_team(manager_role: 'Frontend Engineering Manager, Verify (Interim)', role_regexp: /Continuous Integration/) %>

## Stable Counterparts

The following members of other functional teams are our stable counterparts:

<%= stable_counterparts(role_regexp: /[,&] Verify(?!:)|Continuous Integration/, direct_manager_role: 'Backend Engineering Manager, Verify:Continuous Integration', other_manager_roles: ['Frontend Engineering Manager, Verify (Interim)']) %>

## Technologies

Like most GitLab backened teams we spend a lot of time working in Rails on the main [GitLab CE app](https://gitlab.com/gitlab-org/gitlab-ce), but we also do a lot of work in Go which is the language that [GitLab Runner](https://gitlab.com/gitlab-org/gitlab-runner) is written in. Familiarity with Docker and Kubernetes is also useful on our team.

## Useful Links

 * [Issue Tracker: `~group::continuous integration`](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acontinuous%20integration)
 * [Slack Channel: `#g_ci`](https://gitlab.slack.com/archives/g_ci)
 * [GitLab Unfiltered: Continuous Integration group](https://www.youtube.com/playlist?list=PL05JrBw4t0KpsVi6PG4PvDaVM8lKmB6lV)

### Getting Started

For those new to CI, these links may be helpful in learning more about the product and technology.

 * [CI/CD Development Documentation](https://docs.gitlab.com/ee/development/cicd/index.html)

#### Engineering Overview

 * [CI Backend Architectural Walkthrough - May 2020](https://www.youtube.com/watch?v=ew4BwohS5OY)
 * [Frontend CI product / codebase overview - June 2020](https://www.youtube.com/watch?v=7CUd7aAUiWo)

## How We Work

### Planning

#### Async Issue Refinement

Issues are refined and weighted prior to scheduling them to an upcoming milestone. Weighting also helps with capacity planning with respect to how issues are scheduled in future milestones. We are currently iterating on our async refinement and weighting process.

Issues may need to be broken down if they are too large for a milestone, or further research may be necessary, so subsequent issues or epics can be created.

#### Weighting Issues

We add a `Weight` to issues as a way to estimate the effort to complete an issue. We factor in complexity and any additional coordination needed to work on an issue.  We weight issues based on complexity, following the fibonacci sequence:

| Weight | Description  |
| --- | --- | --- |
| 1: Trivial | The problem is very well understood, no extra investigation is required, the exact solution is already known and just needs to be implemented, no surprises are expected, and no coordination with other teams or people is required.<br><br>Examples are documentation updates, simple regressions, and other bugs that have already been investigated and discussed and can be fixed with a few lines of code, or technical debt that we know exactly how to address, but just haven't found time for yet. |
| 2: Small | The problem is well understood and a solution is outlined, but a little bit of extra investigation will probably still be required to realize the solution. Few surprises are expected, if any, and no coordination with other teams or people is required.<br><br>Examples are simple features, like a new API endpoint to expose existing data or functionality, or regular bugs or performance issues where some investigation has already taken place. |
| 3: Medium | Features that are well understood and relatively straightforward. A solution will be outlined, and most edge cases will be considered, but some extra investigation will be required to realize the solution. Some surprises are expected, and coordination with other teams or people may be required.<br><br>Bugs that are relatively poorly understood and may not yet have a suggested solution. Significant investigation will definitely be required, but the expectation is that once the problem is found, a solution should be relatively straightforward.<br><br>Examples are regular features, potentially with a backend and frontend component, or most bugs or performance issues. |
| 5: Large | Features that are well understood, but known to be hard. A solution will be outlined, and major edge cases will be considered, but extra investigation will definitely be required to realize the solution. Many surprises are expected, and coordination with other teams or people is likely required.<br><br>Bugs that are very poorly understood, and will not have a suggested solution. Significant investigation will be required, and once the problem is found, a solution may not be straightforward.<br><br>Examples are large features with a backend and frontend component, or bugs or performance issues that have seen some initial investigation but have not yet been reproduced or otherwise "figured out". |

 We typically do not weigh an issue more than a 5, as we try to split up issues greater than 5.

We follow the same weighting system that the `Create` stage uses. see the [handbook page of `Create:Source Code` group as an example](/handbook/engineering/development/dev/create-source-code-be/#weights).


### Release Plans

To encourage more transparency and collaboration amongst the team and additionally align on the [Release Posts](/handbook/marketing/blog/release-posts/) we publish at the end of each milestone, we will be creating a separate issue to highlight a **Feature flag roll out plan** for each feature being released starting in 13.2, based on the [issue template for feature flag roll outs](https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/issue_templates/Feature%20Flag%20Roll%20Out.md). The engineer who implements the feature will be responsible for creating this separate issue to highlight the details of when and how the feature flag will be toggled, and subsquently link this issue to their feature issue.  The product manager will tag this issue as a blocker to their release post, so that everyone is aligned on the release plan of the feature.

### Workflow

Once the development phase begins, the team uses the [CI Build](https://gitlab.com/groups/gitlab-org/-/boards/1372896?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acontinuous%20integration&milestone_title=%23upcoming) board to communicate progress on issues.  **Note that the `Milestone` filter is required to use this board.**

Development moves through workflow states in the following order:

1. `workflow::ready for development`
1. `workflow::in dev`
1. `workflow::blocked` (as necessary)
1. `workflow::in review`
1. `workflow::verification`
1. `workflow::production`
1. `Closed`

#### "What do I work on next?"

Each member of the team can choose which issues to work on during a milestone by assigning the issue to themselves.  When the milestone is well underway and we find ourselves looking for work, we default to working **right to left** on our [CI Build](https://gitlab.com/groups/gitlab-org/-/boards/1372896?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acontinuous%20integration&milestone_title=%23upcoming) board, by pulling issues in the right-most column. If there is an issue that a team member can help with on the board, they should do so instead of starting new work. This includes conducting code review on issues that the team member may not be assigned to, if they feel that they can add value and help move the issue along to completion.

Specifically, this means our work is prioritized in the following order:
 * Any verification on code that is in `~workflow::verification` or `workflow::production`
 * Conducting code reviews on issues that are `workflow::in review`
 * Unblocking anyone in `workflow::blocked` or `workflow::in dev` if applicable
 * Then, lastly, picking from the top of the `workflow::ready for development` for development column

The goal of this process is to reduce the amount of work in progress (WIP) at any given time. Reducing WIP forces us to "Start less, finish more", and it also reduces cycle time. Engineers should keep in mind that the DRI for a merge request is **the author(s)**, to reflect the importance of teamwork without diluting the notion that having a [DRI is encouraged by our values](/handbook/people-group/directly-responsible-individuals/#dris-and-our-values).


#### Issue Health Status

For issues being implemented in the current milestone, we use the [Issue Health Status feature](https://docs.gitlab.com/ee/user/project/issues/#health-status) to designate the high level status of the issue. This issue health status is updated by the [directly responsible individual (DRI)](/handbook/people-group/directly-responsible-individuals/) as soon as they recognize the state of the issue has changed.

The following are definitions of the health status options:

- On Track - The issue has no current blockers, and is on schedule to be completed in the current milestone.
- Needs Attention - The issue is still likely to be completed in the current milestone, but there are setbacks or time constraints that could cause the issue to miss the deadline.
- At Risk - The issue has a high likelyhood of missing the current milestone deadline.


#### Category Labels

The Continuous Integration group supports the product marketing categories described below:

| Label                 | |  | | |
| ----------------------| -------| ----|------------| ---|
| `Category:Continuous Integration` | [Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AContinuous%20Integration) | [MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?label_name%5B%5D=Category%3AContinuous%20Integration) | [Direction](https://about.gitlab.com/direction/verify/continuous_integration/) | [Documentation](https://docs.gitlab.com/ee/ci/) |
| `Category:Jenkins Importer` | [Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AJenkins%20Importer) | [MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?label_name%5B%5D=Category%3AJenkins%20Importer) | [Direction](https://about.gitlab.com/direction/verify/jenkins_importer/) | |
| `Category:Merge Trains` | [Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AMerge%20Trains) | [MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?label_name%5B%5D=Category%3AMerge%20Trains) | [Direction](https://about.gitlab.com/direction/verify/merge_trains/) | [Documentation](https://docs.gitlab.com/ee/ci/merge_request_pipelines/pipelines_for_merged_results/merge_trains/) |

#### Continuous Integration Feature Labels

Continuous Integration covers a wide feature area, including the non-marketing category "Pipeline Authoring", so in order to categorize issues more specifically the following feature labels are applied. Which feature label maps to which category is indicated in the Group column.

| Label                 | |  | Description | Group |
| ----------------------| -------| ----|------------| -- |
| `ci::persistence` | [Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=ci::persistence) | [MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?label_name%5B%5D=ci::persistence) | Issues related to Persistence (workspaces, caching). Does not include artifacts, which is its own label. | Pipeline Authoring |
| `ci::rules` | [Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=ci::rules) | [MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?label_name%5B%5D=ci::rules) | Issues related to CI rules syntax. | Pipeline Authoring |
| `ci::integrations` | [Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=ci::integrations) | [MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?label_name%5B%5D=ci::integrations) | Issues related to supporting different CI targets directly (for example, Java or Mobile). | Pipeline Authoring |
| `ci::bridge-pipelines` | [Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=ci::bridge-pipelines) | [MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?label_name%5B%5D=ci::bridge-pipelines) | Issues related to how pipelines start and depend on each other. Includes triggering, cross-project pipelines, and child/parent pipelines. | Pipeline Authoring |
| `ci::dag` | [Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=ci::dag) | [MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?label_name%5B%5D=ci::dag) | Issues related to [Directed Acyclic Graphs](https://docs.gitlab.com/ee/ci/directed_acyclic_graph/). | Pipeline Authoring |
| `ci::authoring` | [Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=ci::authoring) | [MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?label_name%5B%5D=ci::authoring) | Issues related to authoring the `.gitlab-ci.yml` file and [CI YAML configuration](https://docs.gitlab.com/ee/ci/yaml/), at least when not covered more directly by another label such as `ci::rules`.| Pipeline Authoring |
| `ci::artifacts` | [Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=ci::artifacts) | [MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?label_name%5B%5D=ci::artifacts) | Issues related to [CI build artifacts](http://doc.gitlab.com/ce/ci/build_artifacts/README.html). Formerly `~artifacts` | Continuous Integration |
| `ci::variables` | [Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=ci::variables) | [MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?label_name%5B%5D=ci::variables) | Relates to functionality surrounding pre-defined and user-defined variables available in the Build environment. Formerly `~ci variables` | Continuous Integration |
| `ci::interactions` | [Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=ci::interactions) | [MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?label_name%5B%5D=ci::interactions) | Issues related to CI settings and interactions with running pipelines. | Continuous Integration |
| `ci::usage` | [Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=ci::usage) | [MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?label_name%5B%5D=ci::usage) | All issues and MRs related to how we count continuous integration minutes and calculate usage. Formely `~ci minutes` | Continuous Integration |
| `ci::processing` | [Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=ci::processing) | [MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?label_name%5B%5D=ci::processing) | Issues related to pipeline processing | Continuous Integration |
| `ci::statuses` | [Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=ci::statuses) | [MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?label_name%5B%5D=ci::statuses) | Issues related to pipeline statues | Continuous Integration |
| `ci::permissions` | [Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=ci::permissions) | [MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?label_name%5B%5D=ci::permissions) | Issues related to `CI_JOB_TOKEN` and CI authentication | Continuous Integration |
| `ci::api-interactions` | [Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=ci::api-interactions) | [MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?label_name%5B%5D=ci::api-interactions) | Issues related to API endpoints for CI features. | Continuous Integration |
| `ci::notifications` | [Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=ci::notifications) | [MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?label_name%5B%5D=ci::notifications) | Issues related to the notifications related to CI features. | Continuous Integration |
| `ci::graphs & analytics` | [Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=ci::graphs-%26-analytics) | [MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?label_name%5B%5D=ci::graphs-%26-analytics) | Issues related to pipeline graphs and CI analytics. | Continuous Integration |
| `ci::merge-request` | [Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=ci::merge-request) | [MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?label_name%5B%5D=ci::merge-request) | Issues related to CI functionality within the Merge Request. | Continuous Integration |

#### Other Notable Labels

| Label                 | |  | Description |
| ----------------------| -------| ----|------------|
| `CI/CD core platform` | [Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=CI%2FCD%20Core%20Platform) | [MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?label_name%5B%5D=CI%2FCD+Core+Platform) | Any issues and merge requests related to [CI/CD core domain](#core-domain), either as changes to be made or as observable side effects. |
| `onboarding` | [Issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acontinuous%20integration&label_name[]=onboarding) | | Issues that are helpful for someone onboarding to Verify:CI as a new team member. |
| [`Good for 1st time contributors`] | [Issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acontinuous%20integration&label_name[]=Good%20for%201st%20time%20contributors) | | Issues that are good for first time community contributors, and could similarly be labeled for `onboarding` |


### Collaboration with UX and Engineering

To create a high-quality product that is functional and useful – Engineering, PM and Product Designer need to work closely together, combine methodologies, and often connect throughout the product development.

Product Designers play a critical role in the product development of user-facing issues. They collaborate with the Engineering and the Product Manager to design the user experience for the features. Once the design solution is proposed, agreed and validated, the Engineering [DRI](https://about.gitlab.com/handbook/people-group/directly-responsible-individuals/) is assigned to implement that design and functionality during the milestone for which the issue is planned.

Product Designer, PM, and Engineering use `workflow::design` to discuss possible complexities, challenges, and uncover blockers around the proposed solution. To avoid blocking reviews later in the product development flow, the Product Designer, PM, and Engineering should work collaboratively throughout the feature design and development process to check-in often, so that the UX approval on merge requests is not required.

#### Inclusive Development

Our process of planning and development relies heavily on overcommunication rather than any approval gates or automated notification mechanisms. We adhere to the proactive mindset and responsibility of everyone involved to make sure every step in the process is as transparent as it can be.

For both planning and building this means direct, cross-functional, and other relevant stakeholders are included early into the process. This makes sure everyone is able to contribute to the best of their capabilities at the right time in the process. This can include, but is not limited to, GitLab objects, Slack, meetings, and daily standups.

Some practical examples of this are:
- When you open up or begin working on an epic, issue, or merge request consider if all stakeholders are aware of this or should be updated. If unsure, error on the side of updating rather than keeping silent.
- When making significant progress make sure this is seen by the relevant stakeholders even if you don't require immediate feedback by mentioning them rather than relying on automated email notifications. Make their involvement explicit.

Note: A good practice when only wanting to inform rather than requesting a direct action from the mentioned stakeholders is to put `FYI` directly following the @mention handle.

#### Breaking down issues for iteration

We suggest using the below steps to reach the best results in the shortest time:

* Once, through user research, we have identified a user need and have generated the big idea to meet that need, Product Designer drives the process of moving that idea into a solution prototype involving PM and Engineering Team early and often.
* We take that prototype through a user testing to validate it is solving the original problem.
* When the solution is validated with users - it is time to break the big solution down into smaller issues involving a whole team (PM, Engineers, Product Designer, QA and Technical Writer).
* Engineers, Product Designer, EM and PM are expected to work closely together to find the most technically feasible and smallest feature set to deliver value to early customers and provide feedback for future product development. Check out [iteration strategies](#iteration-strategies) for help.

We aim to design broadly for an epic or full feature at least one milestone ahead of time and then break the big solution into smaller issues to pick up in the next milestones.
Suppose working one milestone ahead to design the big solution is not possible. In that case, Engineering and Product Designer will define the first most technically feasible and smallest feature set ([MVC](https://about.gitlab.com/handbook/values/#minimal-viable-change-mvc)) to satisfy early customers that will be implemented in the same milestone.

#### Reviewing Merge Requests

UX should not be seen as a "gate" for reviewing Merge Requests. To avoid blocking reviews and speed up the response time, Product Designer and the Engineering DRI should work collaboratively throughout the feature development process and check-in often, so the UX approval at the stage of reviewing merge request is not required.

Tips to avoid blocking reviews:
- Product Designers should point Engineers to the needed patterns to ensure product consistency and speed up development time. (example: Pajamas components for Frontend engineers, API documentation for Back-end engineers)
- Whenever Engineering DRI submits a Merge Request that is causing changes to UI or user experience - Engineering DRI should tag the appropriate Product Designer for visibility. However, no UX approval on Merge Requests is expected at the review stage (except when the funtionality was not designed by the UX department).

#### Dealing with Community Merge Requests

TBD

#### Aligning on feature development

The Engineering DRI works with the Product Designer throughout the `workflow:in dev` phase to uncover possible problems with the solution early enough that exhibit unexpected behaviour to what was originally agreed upon. If there are changes to be added that weren't agreed upon in the initial issue - a followup issue should be made and the Engineering DRI should work with the Product Manager to schedule that issue in a following milestone. This allows us to focus on [cleanup over signoff](https://about.gitlab.com/handbook/values/#cleanup-over-sign-off), iterate quickly on issues with [a low level of shame](https://about.gitlab.com/handbook/values/#low-level-of-shame), and still make sure we accomplish what we've agreed upon. We should be careful not to hold off on completing these followup issues so that we don't build up a significant amount of UX debt issues.

If we find that solutions are consistently not matching the agreed upon design, we will hold a retrospective with the DRI, designer, and product manager to discuss where the gaps in communication are so that we can improve. It may be necessary to begin requiring a UX approval for merge requests on certain issues to help the Engineering DRI meet the requirements.

#### Avoiding crunch times between UX and Engineering

* Ideally, the Product Manager works ahead of time with a Product Designer to validate the problem and work on the solution. See [Validation track](/handbook/product-development-flow/#validation-track) for more details. This allows us to come up with the bigger idea ahead of time, and work further with Engineering to break it down into smaller iterations. Ideally, this should be completed before the implementation milestone starts.
* Product Designer, PM, and Engineering use the [Design phase](/handbook/product-development-flow/#validation-phase-3-design) in the Validation track to talk about complexities and discuss challenges and uncover blockers. Once we are all in agreement - we can have a put a direction on the board - see [Solution Validation phase](/handbook/product-development-flow/#validation-phase-4-solution-validation) for details.
* Engineers and Product Designers should stay in contact and frequently align throughout the [Build track](https://about.gitlab.com/handbook/product-development-flow/#build-track) to avoid unplanned changes.

### Technical debt

We track our technical debt using the following [CI Technical Debt board](https://gitlab.com/groups/gitlab-org/-/boards/1438885), where we track issues in the planning phase. This board has 2 main sections:

1. Issue readiness
    - In `workflow::planning breakdown` we find issues that are currently being groomed.
    - In `workflow::scheduling` we find issues that clearly defined and a weight has been assigned.

1. Impact breakdown. We use `severity::1`, `severity::2`, `severity::3` and `severity::4` labels to classify the impact of the specific tech debt item.
   We use the list below as a guideline to grade the impact.
    - `severity::1` Blocking or Critical
      - blocking debt that prevents any further changes in the area
      - we stumble on the same complex code every single time and it causes serious slow down on development
      - the problem exists in a core part of our domain that represents a dependency for a large number of features
      - the problem is wild spread throughout several domains
      - the problem is related to a number of reported `severity::1` bugs
    - `severity::2` High
      - blocks many changes in the area or makes them difficult to work around it
      - the problem exists in an area that changes frequently, causing it or workarounds to spread easily
      - the problem is related to an important feature (e.g multi-project pipelines)
      - the problem is very common in a specific domain and leaks into other domains too
      - the problem is related to a number of reported `severity::2` bugs
    - `severity::3` Medium
      - the problem exists in a feature that has a supporting role in our domain
      - the problem exists in an area that does not change frequently
      - the problem is very common in a specific domain but limited to its domain boundaries
      - workarounds slow down development
      - the problem is related to a number of reported `severity::3` bugs
    - `severity::4` Low
      - the problem is very isolated and has low or no impact to development
      - the area affected does not change frequently
      - the problem is related to a number of reported `severity::4` bugs

Note that a multiple factors can exist at once. In that case use your judgment to either bump the impact score or lower it. For example:
- the problem exists in a feature that has a supporting role in our domain but it's related to a number of `severity::2` bugs.
  Then choose `severity::2`.
- the problem is related to an important feature but the workaround is acceptable as the code around it does not
  change frequently. Then choose `severity::3`.

### Retrospectives

The CI team leverages a monthly async retrospective process as a way to celebrate success and look for ways to improve. The process for these retrospectives aligns with the automated retrospective process used by many teams at GitLab. The process is defined here: https://gitlab.com/gitlab-org/async-retrospectives#how-it-works.

A new retrospective issue is created on the 27th of each month, and remains open until the 26th of the following month. Team members are encouraged to add comments to that issue throughout the month as things arise as a way to capture topics as they happen. The current issue can be found in https://gitlab.com/gl-retrospectives/verify/-/issues.

On the 16th of each month a summary of the milestone will be added to the issue and the team will be notified to add any additional comments to the issue.

As comments are added to the issue, team members are encourage to upvote any comments they feel are important to callout, and to add any additional discussion points as comments to the original thread.

Around the 26th of the month, or after the discussions have wrapped up the backend engineering manager will summarize the retrospective and create issues for any follow up action items that need to be addressed. They will also redact any personal information, customer names, or any other notes the team would like to keep private. Once that has been completed the issue will be made non-confidential and closed.

### Team Communication

The CI team is globally distributed and separated by many timezones.  This presents some challenges in how we communicate since our work days only overlap by a couple hours.  We have decided as a team to embrace asynchronous communication because scheduling meetings is difficult to coordinate.  We meet as a team one day per week, on Wednesdays for a quick team meeting and 1-on-1s.

- We are pro-active about asking questions in advance with the understanding that the turnaround on receiving an answer is usually a full working day.
- Our engineering plan and issue boards need to be updated regularly to communicate what stage our work is in.
- Any meetings that we hold need a preset agenda and are recorded for folks who are unable to attend.
- Having a positive work/life balance, despite these challenges, must be a high priority and handled intentionally so no one is routinely starting early or staying late to attend meetings.


#### How to work with us

##### Slack

Daily standup updates are posted to [`#g_ci`](https://gitlab.slack.com/archives/CPCJ8CCCX). Feel free to ask us questions directly in this Slack channel and someone will likely get back to you within 24 hours (by the next business day).  We will use following emojis to respond to the posted question accordingly:
 * ![eyes](https://a.slack-edge.com/production-standard-emoji-assets/10.2/apple-large/1f440@2x.png){:height="20px" width="20px"} -- `:eyes:` to indicate that one of us has seen it
 * ![checkmark](https://a.slack-edge.com/production-standard-emoji-assets/10.2/apple-large/2705@2x.png){:height="20px" width="20px"} -- `:white_check_mark:` to indicate that the question has been answered

The verify stage has a separate Slack channel under [`#s_verify`](https://gitlab.slack.com/archives/C0SFP840G), which encompasses the other two teams of Verify: [Runner](../runner/) and [Testing](../testing/).

##### GitLab Issues and MRs

Most spontaneous team communiation happens in issues and MRs. Our issues have a group label of [`~"group::continuous integration"`](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=group%3A%3Acontinuous+integration&scope=all).  You can also tag a team member with `@mention` in the issue if you have someone specific to ask.

If you need to call the attention of the entire group, you can tag `@gitlab-com/ci-group`.

## Developer Onboarding

Welcome to Verify:CI! Whether you're transferring internally or joining GitLab as a new hire, you'll be assigned an [onboarding buddy](/handbook/general-onboarding/onboarding-buddies/). If you are an internal transfer, your manager will appoint you an onboarding buddy so you can have someone to work with as you're getting familiarized with CI, our codebase, our tech stack and the team.

Read over this page as a starting point, and feel free to set up regular sync or async conversations with your buddy. It's also recommended to schedule a few coffee chats to meet some members of our team. Your manager may optionally create an onboarding checklist for you to go through if it's helpful.

In the our list of issues, there are also ones with an `~onboarding` label that are smaller issues to onboard onto the product. If there aren't any in the current [CI Build](https://gitlab.com/groups/gitlab-org/-/boards/1372896?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acontinuous%20integration&label_name[]=onboarding&milestone_title=%23upcoming) board to work on, reach out to your EM/PM to see which issues you can work on as part of your onboarding period.
