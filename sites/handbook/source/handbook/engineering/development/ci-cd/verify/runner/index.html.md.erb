---
layout: handbook-page-toc
title: "Verify:Runner Group"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Vision

For an understanding of what this team is going to be working on take a look at [the product
vision](/direction/verify/runner/#top-vision-items).

## Mission

The Verify:Runner Group is focused on all the functionality with respect to
Runner.

This team maps to [Verify](/handbook/product/product-categories/#verify-stage) devops stage.

## Performance Indicator

We measure the value we contribute by using Performance Indicators (PI), which we define and use to track progress. As the GitLab Runner is the engine that enables GitLab CI, the PI for Verify:CI and Verify: Runner are the same. The [performance indicator](/handbook/product/ops-section-performance-indicators/#verify-ci-verify-runner-count-of-pipelines-triggered-by-unique-users) is the `number of unique users who trigger ci_pipelines`. We recognize that this metric on its own is not the best leading indicator for forecasting whether the core Runner product is positively impacting the long term growth of GitLab. As such, we will continue to iterate on refining the metrics.

## Usage Funnel

As defined on the growth page, AARRR stands for Acquisition, Activation, Retention, Revenue, and Referral. This framework represents the customer journey, and the various means a product manager may apply a North Star metric (performance indicator) to drive a desired behavior in the funnel. As discussed above, since Runner is the engine that drives GitLab CI, at this time, we will leverage the same usage funnel definitions for [Verify:CI](/handbook/engineering/development/ci-cd/verify/continuous-integration/#usage-funnel).

When we launch the macOS Runners as a product, we will add a section here that defines the AARRR metrics for that offer. The rationale for that is that we are planning for a separate landing page for the macOS Runners. As such, we can measure page views and click-throughs to call to actions.

Example AARRR metrics that we have in mind for that offer:
- Acquisition: conversion rate from page view to click-through to call to action.
- Activation:  conversion rate from call to action click-throughs to new user sign-ups for a GitLab plan.
- Retention: percentage of new users that continue to use GitLab after sign up.
- Revenue: macOS Cloud Runner minutes used per month.

## Team Members

The following people are permanent members of the Verify:Runner group:

<%= direct_team(manager_role: 'Backend Engineering Manager, Verify:Runner') %>

## Stable Counterparts

To find out who our stable countaerparts look at the [runner product
categtory](/handbook/product/product-categories/#runner-group)

## Projects we maintain

As a team we maintain several projects. The <https://gitlab.com/gitlab-com/runner-maintainers> group
is added to each project with maintainer permission. We also try to align tools and versions used across them.

### Product projects

- [GitLab Runner](https://gitlab.com/gitlab-org/gitlab-runner)
- [GitLab Runner's Helm Chart](https://gitlab.com/gitlab-org/charts/gitlab-runner)
- [Autoscaler - Custom Executor driver](https://gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler)
- [AWS Fargate - Custom Executor driver](https://gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/fargate)
- [GitLab Runner UBI offline build](https://gitlab.com/gitlab-org/ci-cd/gitlab-runner-ubi-images)
- [GCP Windows Shared Runners base VM image](https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/gcp/windows-containers)

### Helper projects

- [Runner release helper](https://gitlab.com/gitlab-org/ci-cd/runner-release-helper)
- [GitLab Changelog](https://gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog)
- [Release index generator](https://gitlab.com/gitlab-org/ci-cd/runner-tools/release-index-generator)
- [Common configuration for new GitLab Runner projects](https://gitlab.com/gitlab-org/ci-cd/runner-common-config)
- Linters
  - [Runner linters Docker images](https://gitlab.com/gitlab-org/ci-cd/runner-tools/runner-linters)
  - [goargs linter](https://gitlab.com/gitlab-org/language-tools/go/linters/goargs)

### Other projects

- [DinD image tests](https://gitlab.com/gitlab-org/ci-cd/tests/dind-image-tests)
- [Docker Machine fork](https://gitlab.com/gitlab-org/ci-cd/docker-machine/)

## Technologies

We spend a lot of time working in Go which is the language that [GitLab Runner](https://gitlab.com/gitlab-org/gitlab-runner) is written in. Familiarity with Docker and Kubernetes is also useful on our team.

## Becoming a maintainer for one of our projects

We follow the same process [outlined
here](/handbook/engineering/workflow/code-review/#how-to-become-a-maintainer).
Any engineeer inside of the organization is welcome to become a
maintainer of a project owned by the Runner team. The first step would
be to become a [trainee
maintainer](/handbook/engineering/workflow/code-review/#trainee-maintainer).

To start the maintainer training process,
please create an issue in the Runner project's issue tracker using the [Release Maintainer Trainee template](https://gitlab.com/gitlab-org/gitlab-runner/issues/new?issuable_template=trainee-backend-maintainer).

After the training is finished, the new maintainer will be added to the <https://gitlab.com/gitlab-com/runner-maintainers>
group, which will set proper permission across the projects.

## Common Links

 * [Issue Tracker](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Arunner)
 * [Slack Channel](https://gitlab.slack.com/archives/g_runner)
 * [Roadmap]()

## How we work

### Runner Group Specific Onboarding Needs

* `editor` access to the `group-verify` project in GCP
* Add as `developer` to the `gitlab-com/runner-group` group on GitLab.com
* Make sure entry in `team.yml` has the new member as a reviewer of `gitlab-org/gitlab-runner` and `gitlab-org/ci-cd/custom-executor-drivers/autoscaler`
* Add as `developer` to the `gitlab.com/gl-retrospectives/runner/` project and add to the team definition in `gitlab.com/gitlab-org/async-retrospectives`
* Add to Geekbot daily standup for `Runner Group Daily Standup` and `Runner Weekly Retro`
* Add to `Verify` 1password vault (requires creating an access request).

### Kanban

We try to use the Kanban process for planning/development.  Here is the
single source of truth about how we work. Any future changes should be
reflected here. If you feel like that process is not ideal in certain
aspects or doesn't achieve a goal, you are more than welcome to open a
merge request and suggest a change.

#### Goals

- Reduce churn of each milestone, having to move 1 issue to another
  milestone, checking if we have the capacity.
- Be more flexible with what makes it in a milestone.
- Make our process clearer to identify bottlenecks.
- Remove the feeling that we just planned this milestone but need to
  change certain things because we added a new issue.

#### Process

We use the following
[board](https://gitlab.com/groups/gitlab-org/-/boards/1605623?&label_name[]=group%3A%3Arunner)
for planning, milestone tracking.

- Open column: This is the teams' backlog, all issues that the team is
  responsible of.
- `~workflow::start`: This is the backlog for the product manager, they
  are responsible for this column. Issues should be vertically stacked,
  the top one has the highest priority. At this stage the issue is not
  very fleshed out, and we still need to understand the problem. When an
  issue is in this column it means that the product manager is aware of
  the issue and will start working on it soon. This column is limited to
  10.
- `~workflow::planning breakdown`: This is where the engineering team
  will work with the product manager to make sure we have a good
  proposal for the issue at hand. Each engineer should spend 1-2 hours each
  week to discuss the issue async with the community and product manager
  to figure out a proposal. Particular attention should be given to thinking
  about how the team can build the feature in an iterative way - what's the
  smallest change we could make to get some version of the feature into the
  hands of a customer in a single release?

  Sometimes it's helpful to attach a milestone to the issue that is in this
  column, so that an engineer can focus time doing PoC and research spikes for
  that specific release.. It's perfectly OK for an issue to be closed from this
  column if we decided to split the issue into multiple ones. When both the
  product manager and engineering team are happy with the proposal they should
  move the issue to the next column, `~workflow::ready for development`. The
  people assigned to this issue are most likely engineers and are responsible
  for getting this issue to the next stage.
- `~workflow::ready for development`: At this stage, we should have a
  good idea of what the issue requires, and how much work it is. When an
  engineer is out of tasks to work on and has no merge requests to
  review or any issues that are `~workflow::In dev` they should pick the
  one on top, assign it to them and move it to the next column. In this
  column, each issue should have a milestone attached to it to indicate
  to the customer in which release it will be done in.
- `~workflow::In dev`: Here is where the engineer starts working on the
  issue. If the issue is not clear on what needs to be achieved, they
  should discuss it with the team to see if it needs to go back to
  previous stages. This column is limited to 3 issues per engineer in
  the team. If they have more issues assigned to them we need to
  reevaluate the workload of the engineer because there is most likely a
  lot of context switching which is not effective.
- `~workflow::In review`: When the issue has either 1 merge request or
  multiple merge requests ready for review it should be moved to the
  `~workflow::In review` column. There is a limit of 20 issues that
  should be in review. If we are at limit engineers should not pick up
  new work but see if they can help out in the review process.
- `~workflow::blocked`: There are multiple reasons why an issue can be
  blocked. If the issue has a milestone attached to it, the issue
  blocking it should have the same milestone or 1 earlier, and have a
  higher priority. We should also mark the issue as blocked using the
  [related issue
  feature](https://docs.gitlab.com/ee/user/project/issues/related_issues.html#adding-a-related-issue).
- Closed column: Congratulations, the issue process has finished, the
  bug was fixed or the feature was added. Check if you can help with
  `~workflow::planning breakdown` or `~workflow::In review`. If not pick
  a new issue from `~workflow::ready for development`.

Tips:
- For each column the priority is stack-ranked, meaning that the one on
  the top is the most important one.
- To keep the stack intact, moving 1 issue from 1 column to another
  might mess the priority. Open the issue and use the
  [`/label`](https://docs.gitlab.com/ee/user/project/quick_actions.html)
  quick action to keep the same priority stack.

#### Kickoff

As discussed above each issue inside of `~workflow::ready for
development` should have a milestone attached to it. If stack-ranked
properly, the issues on top should be in the current milestone, and then
the upcoming milestone. The product manager will take the top 5 issues
from the `~workflow::ready for development` column that is intended
for the upcoming milestone and use these issues as [kickoff
issues](/handbook/product/product-processes/#kickoff-meetings).

#### Weekly Refinement

The team has a weekly meeting where we spend 10-15 minutes
to review and discuss issues on the board. Especially if the product manager needs input on
items in `~workflow::start`. There shouldn't be uncertainties regarding the priorities
for the week ahead at the end of the meeting.

#### Technical Debt / Backstage work

In general, technical debt, backstage work, or other classifications of development work that don't directly contribute to a users experience with the runner are handled the same way as features or bugs and covered by the above Kanban style process. The one exception is that for each engineer on the team, they can only have 1 technical debt issue in flight at a time. This means that if they start working on a technical debt type issue they cannot start another one until the first one is merged. In the event that an engineer has more than one technical debt item in flight, they should choose which one to keep working on and move the others to the "in development" or "ready for review" columns depending on their status. The intent of this limitation is to constrain the number of technical debt issues that are in review at any given time to help ensure we always have most of our capacity available to review and iterate on features or bugs.

## Regression Error Budget Process

Each quarter we have an error budget of how many regression the release
can cause.

Following the point system, each quarter we have we have 100 points.
Each type of regression has a
[priority/severity](../../../../quality/issue-triage/#priority), which
every priority has a certain point associated to it, the higher the
priority the more points:

- `~priority::1`: 70
- `~priority::2`: 30
- `~priority::3`: 15
- `~priority::4`: 5

Every beginning of the quarter the error budget is set to 100 again.

### Goals

- Provide an incentive to balance reliability with other features.
- Provide confidence to customers that we are don't release regressions
  for each release.
- Make sure that the team is accountable.
- Make sure the team is acting to make the product more resilient.
- Give the team permission to focus on reliability when data indicates
  that reliability is more important than other product features.

### Non-Goals

- This process is not intended to serve as a punishment.
- This process is not intended to serve as a punishment for adding
  regressions.
- This process is not intended to slow us down from shipping features,
  but actually speed it up in the long-run.

### Exceeding Error Budget

Each regression should have a retrospective item with corrective actions
to prevent any similar regression from happening again. These issues
should be labeled with `~"corrective action"`.

While Google's original paper advocates for stopping all feature work
and focusing the team only on reliability improvements for the quarter,
we have taken a more minimal first step: exceeding our error budget will
only be a callout for our Product Manager to immediately prioritise
corrective actions identified during the retrospective. Other feature
work by team members not assigned to those corrective actions can
proceed normally.

### History

Below is the history of each quarter, and should be filled with the
following template:

```
- Regressions:
    - Issue
        - Retrospective Issue
        - Corrective Action Issues
- Final Error budget: 30
```

### Background

This was inspired by the [Google Error
budget](https://landing.google.com/sre/workbook/chapters/error-budget-policy/).
It's used to balance reliability and new features.

## How to work with us

### On issues

Issues worked on by the Runner group a group label of `~group::runner`. Issues that contribute to the verify stage of the devops toolchain have the `~devops::verify` label.

### Get our attention

GitLab.com: `@gitlab-com/runner-group`
Slack: [`#g_runner`](https://gitlab.slack.com/archives/CBQ76ND6W)

### Code review

Our code review process follows the [general process](https://docs.gitlab.com/ee/development/code_review.html)
where you choose a reviewer (usually not a maintainer) and then send it over to a maintainer for the final review.

Current maintainers are members of the [`runner-maintainers`](https://gitlab.com/groups/gitlab-com/runner-maintainers/-/group_members?with_inherited_permissions=exclude) group.
Current reviewers are members of the [`runner-group`](https://gitlab.com/groups/gitlab-com/runner-group/-/group_members?with_inherited_permissions=exclude) group.

### Feature Freeze

The gitlab-runner codebase, which is the primary codebase that the runner group works in, follows a 7th-of-the-month feature freeze. This is documented with the codebase [here](https://gitlab.com/gitlab-org/gitlab-runner/-/blob/master/PROCESS.md#on-the-7th) and is important to be aware of as it drastically differs from most of the rest of the gitlab release timing.

## Team Resources

### Testing on IBM Z/OS

To facilitate testing the `s390x` architecture artifacts,
a Z/OS VM is available to GitLab team members.

### Logging in

1. In [1Password](https://about.gitlab.com/handbook/security/#1password-guide),
   under the `Verify` vault, download the `zOS login - gitlabkey02.pem` file.
1. From the `zOS login` entry in the same vault, take note of the `user` and `address` fields.
1. SSH into the Z/OS VM:

    ```shell
    ssh -i "zOS login - gitlabkey02.pem" <user>@<address>
    ```

   Note: You'll be requested the password to unlock the .pem file. Enter the password attached
   to the `zOS login - gitlabkey02.pem` entry.

### Testing helper image

Assuming you want to test a `prebuilt-s390x.tar.xz` image produced by a CI/CD pipeline,
and already have the .pem file from the [previous point](#logging-in),
the steps would be the following:

1. Copy the `prebuilt-s390x.tar.xz` file to the Z/OS VM:

    ```shell script
    scp -i "zOS login - gitlabkey02.pem" prebuilt-s390x.tar.xz <user>@<address>:/home/ubuntu/
    ```

   Note: You'll be requested the password to unlock the .pem file. Enter the password attached
   to the `zOS login - gitlabkey02.pem` entry.

1. SSH into the VM:

    ```shell
    ssh -i "zOS login - gitlabkey02.pem" <user>@<address>
    ```

1. Import the image and run it:

    ```shell
    sudo docker import ./prebuilt-s390x.tar.xz gitlab/gitlab-runner-helper:s390x-dev
    sudo docker run -it gitlab/gitlab-runner-helper:s390x-dev bash
    gitlab-runner-helper help
    ```
