---
layout: handbook-page-toc
title: "Release:Progressive Delivery Group"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Progressive Delivery Group

The Progressive Delivery Group works on the parts of GitLab within the
[Release Stage](/handbook/product/product-categories/#release-stage), a component of our greater [CI/CD product](/direction/ops/).

## Vision

For an understanding of what this team is going to be working on take a look at [the product
vision](/direction/ops/).

## Mission

The Release:Progressive Delivery is focused on all the functionality with respect to
Continuous Delivery and Release Automation.

This team maps to [Release](/handbook/product/product-categories/#release-stage) devops stage.

## Product Categories

We are responsible for [maturation](/direction/maturity/) of several [continuous delivery product categories](/direction/ops/#progressive-delivery).

## Jobs to be Done

You can view and contribute to our current list of JTBD and job statements [here](/handbook/engineering/development/ci-cd/release/progressive-delivery/jtbd/).

## OKR's

OKRs stand for Objectives and Key Results and are our quarterly objectives. We work to achieve the [company OKRs](https://about.gitlab.com/company/okrs/) by contributing towards stage OKRs. Achieving these goals will advance us towards product maturity in the categories linked above.

<%= partial("handbook/engineering/ux/stage-group-ux-strategy/release/progressive-delivery/_progressive_delivery_okrs") %>

## KPI's

Secondary to the completeness of OKRs, we will continuously monitor the following Key Performance Indicators:

* [Say/Do Ratio](https://app.periscopedata.com/app/gitlab/658030/WIP:-Say-Do-Ratios)
* The timely resultion of Security Patches and Bug Fixes.

## Team Members

The following people are permanent members of the Release Team:

<%= direct_team(manager_role: 'Backend Engineering Manager, Release:Progressive Delivery') %>
<%= direct_team(manager_role: 'Frontend Engineering Manager, Release (CD)', role_regexp: /Progressive/) %>

## Stable Counterparts

The following members of other functional teams are our stable counterparts:

<%= stable_counterparts(role_regexp: /[,&] Release/, direct_manager_role: 'Backend Engineering Manager, Release:Progressive Delivery', other_manager_roles: ['Frontend Engineering Manager, Release (CD)']) %>

## Technologies

Like most GitLab backend teams, we spend a lot of time working in Rails on the main [GitLab CE app](https://gitlab.com/gitlab-org/gitlab-ce), but we also do a lot of work in Go which is used heavily in [GitLab Pages](https://about.gitlab.com/stages-devops-lifecycle/pages/). Familiarity with Docker and Kubernetes is also useful on our team.

## Common Links

 * [Release Issue Tracker](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Arelease)
 * [Release Slack Channel](https://gitlab.slack.com/archives/s_release)
 * [Progressive Delivery Issue Tracker](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Arelease&label_name[]=group%3A%3Aprogressive%20delivery)
 * [Progressive Delivery Slack Channel](https://gitlab.slack.com/archives/g_progressive-delivery)
 * [Ops Roadmap](/direction/ops/)

## Planning and Process

<%= partial("handbook/engineering/ux/stage-group-ux-strategy/release/progressive-delivery/_progressive_delivery_planning") %>

## How to work with us

### On Issues

Issues that contribute to the progressive delivery group of the release stage of the devops toolchain have the `~"devops::release"` and `~"group::Progressive Delivery"` labels.

### Self-assignment of Issues

As described below, we use a successive set of discrete labels and issue boards built from those labels in a mostly linear flow that is similar to Kanban. In a kanban-like flow, issues/cards are _pulled not pushed_. Individuals on the team should self-assign to issues in the `Ready for Development` columns. On occasion, the Engineering Manager will assign a designer, developer, or other contributor as required.

### In Slack

The team's primary Slack channel is `#g_progressive-delivery`, and the public managers channel is `#release-stage-managers`. We also support a number of feature channels for discussons or questions about a specific [feature area](/handbook/product/product-categories/#release-stage). We no longer support issue specific channels, as they are easy to lose track of and fragment the discussion. Supported channels are:

* Continuous delivery: `f_continuous_delivery`
* Feature flags: `f_feature_flags`
* Review Apps: `f_review_apps`
* Deploy Keys: `f_deploy_keys`

### Team Workflow and Issue Boards

#### Planning

Ahead of the near-term planning phase there is an estimation phase. See the engineering evaluation and estimation section below.

We use a [Milestone Planning Board](https://gitlab.com/groups/gitlab-org/-/boards/1454481?milestone_title=13.1&&label_name[]=devops%3A%3Arelease&label_name[]=group%3A%3Aprogressive%20delivery) customized for each milestone to organize and plan upcoming features. This board details the various stages an issue can exist in as it is being readied for development. It's not necessary that an issue go through all of these stages, and it is allowed for issues to move back and forward through the workflow as they are iterated on. It highlights `Release::P1`, `Release::P2`, and `bugs` above other priority issues. This is use in concert with the async release planning issue (see below).

Issues that are being planned should include the `cicd::planning` issue label so as to distinguish them from those issues which will be worked on during this milestone. We reserve the `cicd::active` label as the counterpart to `cicd::planning` for active work.

Once we have determined the cutline for a given milestone based on our capacity, spillover from the previous milestone, bugs, security fixes, tech debt, and UX debt, we can move those issues into `workflow::scheduling`.

#### Scheduling

The `~cicd::planning` and `~workflow::scheduling` labels and a customized `%milestone` assignement filter are used to feed the scheduling board where the EM and PM connect on scheduling upcoming issues.

We use the [Scheduling Board](https://gitlab.com/groups/gitlab-org/-/boards/1489554?label_name[]=group%3A%3Aprogressive%20delivery&label_name[]=workflow%3A%3Ascheduling) as a checkpoint to ensure that we have well-formed issues and have estimated our capacity correctly prior to labelling these issues ~"workflow::ready for development".

Both `~workflow::ready for development` and `cicd::active` labels **are** required before they're scheduled in an iteration.

#### Building

We use the [Build board](https://gitlab.com/groups/gitlab-org/-/boards/1489558?label_name[]=cicd%3A%3Aactive&label_name[]=group%3A%3Aprogressive%20delivery) to track features as they make their way from idea to production. _This should is the only place that developers should find new work_. This board does not have a concept of a Milestone, it represents the development lifecycle and is populated with issues that have been previously evaluated. On occasion, unscheduled issues like high priority bugs, community contributions, the like will appear on this board. Here, issues move more or less linearly from Ready for Development thru to Verification.

On occasion they have to return to an earlier state like `workflow:ready for development` as priorities shift, `workflow::refinement` if the issue was not truly ready, or to `workflow::blocked` if we are unable to proceed. As a point of clarification, `workflow::scheduling` contains issues that are ready to go and that we're waiting to act until we have capacity or waiting planned for specific milestone while `workflow::refinement` is about the proposal needing polishing.

Issues if over a weight of 1, will have multiple MRs. Each of these associated MRs will likely be in a different stage of development. It's important to update the issues on the build board with the latest, most relevant state. Example, if an issue has three MRs with one in review, another in development, and one ready for development, then the we should place that issue is in ~"workflow::In dev" as this is where the most activity for this issue is happening - it's the most relevant signal to the rest of the team. Because we have a one-to-many relationship between our issues and MRs, activity on the board will appear slow. Async Issue Updates and weekly sync meetings both outlined below help with any ambiguity that this system introduces.

#### Workflow Stages and Labels

Below is a description of each stage by label, its meaning, and any requirements for that stage.

* `workflow::start`
  * The entry point for workflow scoped labels. From here, Issues will either move through the validation workflow, go directly to planning and scheduling, or in certain cases, go directly to the development steps.
  * includes `cicd::planning` label
* `workflow::problem validation`
  * This stage aims to produce a [clear and shared understanding of the customer problem](/handbook/product-development-flow/#validation-phase-2-problem-validation).
  * includes `cicd::planning` label
* `workflow::solution validation`
  * This output of this stage is a clear prototype of the solution to be created.
* `workflow::design`
  * Issues in the UX Design phase
  * includes `cicd::planning` label
* `workflow::solution validation`
  * This output of this stage is a [clear prototype of the solution to be created](/handbook/product-development-flow/#validation-phase-3-solution-validation).
  * includes `cicd::planning` label
* `workflow::planning breakdown`
  * this step includes the estimation the Engineering Evaluation & Estimation work as outlined below.
  * includes `cicd::planning` label
* `workflow::scheduling`
  * This is the last checkpoint to ensuer that issues are ready for development.
  * includes `cicd::planning` label
* `workflow::ready for development`
  * This stage indicates the issue is ready for engineering to begin their work.
  * Before entering this workflow state, issues must meet the Definition of Ready (see below)
  * If you are looking for new work to pick up, unassigned issues should be tackled in order of priority label.
  * includes `cicd::active` label
* `workflow::in dev`
  * This stage indicates that the issue is actively being worked on by one or more developers.
  * Please see that the issue meets the Definition of Done (see below) before proceeding.
  * includes `cicd::active` label
* `workflow::in review`
  * This stage indicates that the issue is undergoing code review by the development team and/or undergoing design review by the UX team.
  * includes `cicd::active` label
* `workflow::verification`
  * This stage indicates that everything has been merged and the issue is waiting for verification after a deploy.
  * includes `cicd::active` label

#### Definition of Ready

Before the team will accept an issue into a milestone (as marked by the the labels `~workflow::"ready for development"`` + `~cicd::active`) for work it must meet the following criteria:

* Issues labeled with `~feature` include a well stated "why" and the customer problem to solve
* For issues labeled with `~bug` include, steps to reproduce
* Designs are in the design tab if designs are needed
* A weight applied for either frontend, backend, or both as appropriate.
* Labeled with either `frontend`, `backend` or both to indicate which areas will need focus.

#### Label Definitions

There are a number of labels applied at any time to both issues and merge requests, but there is a priority, first being the highest:

* `planning priority` - This label is rarely used, but we consider it to be a promise to a customer. Issues with this label will supersede all other work even those planned issues. The use of this label is usually held in reserve for issues of the utmost importance.
* `priority::1` / `severity::1` - From the perspective of bugs, a `priority::1` / `severity::1` has the [highest priority](/handbook/engineering/quality/issue-triage/#priority) and a short SLO. Issues with this label will supersede all other work even planned issues.
* `Deliverable` - Issues with this label are agreed between team EM and the PM to have the highest overall normal priority and could be delivered in the given milestone. Deliverable issues not closed in a given milestone are auto forwarded via milestone cleanup workflow. Deliverable issues are often paired with `Release::P1` issues.
* `Release::P1` - Similar to and often paired with `Deliverable`. This is the highest priority from the perspective of the sub-department. Assuming there are no `planning priority` and `priority::1/severity::1` issues, we start with these issues first, and we focus on these over lower priority issues as much as possible.
* `Community contribution` - The main label for the Contributor Program. We should support the contributor community by triaging and coaching issues in our area with this label. We prioritize this effort between Deliverables and lower priority issues.
* `Release::P2` - This is the second highest priority from the perspective of the sub-department.
* `Release::P3` - This is the third highest priority from the perspective of the sub-department.
* `Unscheduled` - Items with this label are being worked on in this release but have not been previously scheduled by the EM and PM. Work orignating with this label is usually event driven - another team requires immediate help, a regression affecting users, or severe technical debt that is causing inefficiency in the team.
* `Stuff that should Just Work` - This part of our [incremental improvement engineering process](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html#incremental-improvements) where should allow for capacity to make improvements to the GitLab codebase or documentation leaving our work environment a better place than when we found it.

#### Definition of Done

GitLab has a documented a clear [Definition of Done](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html#definition-of-done) for contributions to GitLab. We will follow that standard as our own definition of done.

#### Work In Progress Limits

We have established a Work in Progress limit (WIP) of 9 given that we currently have 6 engineers on the team (1.5 x number of engineers). This has been applied to the `workflow::in dev` list on the build board. When we've hit our maximum, should instead help move WIP issues along. The EMs will reevaluate this WIP limit with each milestone and make adjustments accordingly.

#### Capacity Planning

Historically, our average MRs per month (not milestone) has been about 27 MRs. (see this [periscope chart](https://app.periscopedata.com/app/gitlab/556687/CI-CD-Section-Metrics?widget=8427818&udv=0))

With the knowledge that [things take longer than you think](https://erikbern.com/2019/04/15/why-software-projects-take-longer-than-you-think-a-statistical-model.html), we will devote about 30% of our estimated capacity for a given milestone to `Release::P1` / `Deliverable` issues. The rest of the milestone capacity is taken with assisting your peers, followed by peer reviews, maintainer reviews, community contributions, lower priority items, bugs, issue estimations, and fixing stuff that should just work. The EM and PM will evaluate this capacity allocation for Deliverable issues during milestone planning and this may adjust overtime.

### Engineering Evaluation & Estimation

In order to make sure iterations are efficient, it is important to ensure issues are clearly defined and broken down before the start of an iteration.
In order to accomplish this, engineering evaluation will take place for issues that require it (small changes and bug fixes typically won’t require this analysis).
With each milestone we collect a series of upcoming issues to be reviewed and discussed asynchronously into a meta Needs Weight Issue ([example issue](https://gitlab.com/gitlab-org/gitlab/-/issues/212773)). These issues should have the label `workflow::planning breakdown`. We use this label not only for well-defined user stories, but also functional, performance, documentation, and security issues.
See the [Build phase](/handbook/product-development-flow/#build-phase-1-plan) of the Product Development Workflow.

Assigned engineers will work with Product, UX and other engineers to determine the implementation plan for a proposed feature. As the assigned engineer provides a propsed breakdown, they should also affix a weight to the issue. See below.

Once an implementation plan has been finalized, the following steps should be taken:

* The issue description should be updated to include the details of the implementation plan along with a checklist to show the planned breakdown of merge requests.
* The issue should be moved forward in the workflow to `workflow::ready for development` by the Engineering Manager only after consulting with the PM during scheduling. Capacity, cut line, an issue that does not meet the definition of ready are some sceanarios that could prevent this label from being applied during planning.
* The weight of the issue should be updated to reflect the number of merge requests estimated as part of the implementation plan. For smaller changes and bugs, the weight should be 1.  Note, that we are separating frontend weight from backend weight where possible using a `~frontend-weight::[1-5]` label.
* Those updates to the issue will signal that the issue has been properly scoped and is ready to be worked on in an upcoming milestone.

Issues with a weight **greater than 4** should be reconsidered by the team. This is a signal that the issue is too large, too complicated, and that we should iterate on the issue proposal. Please consider proposing a new scope of the issue such that it can be split into future iterations.

#### Merge Request Count As A Measure Of Issue Weight

As a byproduct of the engineering evaluation process, a rough estimate of the number of merge requests required to develop a feature will be produced. This measure can be used as a way to determine issue weights. These weights can be useful during the planning process to get a rough gauge of how many features can be worked on in a milestone, and to understand how much work the team can do in a milestone. This metric also aligns with the [throughput metric](/handbook/engineering/management/throughput/) currently measured by engineering teams.

#### Avoid Interdependent Merge Requests in a Single Milestone

As a general rule, we should avoid adding interdependent Merge Requests into a single milestone except under special circumstances like a `~priority::1/~severity::1` bug.
Given our dependence on availability of reviewers and ultimately maintainers, we are not always able to guarantee a perfect coordinated release of all MRs required for a given feature sometimes 1 or more MRs is blocked by review, merge conflicts, or other entanglements.

We should take advantage of Feature Flags and other tactics if we need to neutralize behavior as needed for feature rollouts.


## Tips: When you're having hard time to estimate MR count, consider PoC step.

If you have no idea where to begin the development due to lack of knowledge on the context or
you're less confident on the current technical proposal,
it might be better to work on Proof-of-Concept MR (PoC or Prototype) at first.
This gives you more insights on the current implementation details and potential required changes.

This step removes a lot of "if"s from your assumptions. When you're making a
technical proposal, often you don't have enough time to investigate the actual
implementation details. In such case, you're assuming like "Probably, this feature works
like ..." or "Probably, this feature and that feature are connected like ...", which
leaves some probability in your development plan, and if it turned out a wrong assumption, you
may need to rethink the whole approach, even though you're at the middle of development cycle.
PoC step derisks such a turnaround by cross-checking the technical proposal with
domain/performance/security experts and UX/PM.

Here is how to work on PoC step:

- Create a new MR with a title started from "PoC:". Explain the technical proposal
  in the MR description, that you think it's the best.
- Try to implement the feature in the merge request. Frontend and bakcend engineers
  can work on the same branch.
- If it takes too long time to make the feature actually work on your local development machine,
  your technical proposal might need to be reconsidered. You can seek the other viable approaches
  by hearing from domain experts or the other developers.
- If you see technical difficulties that seem impossible to be solved in the same milestone,
  you should raise a concern that there is a blocker thus we might
  not be able to finish the feature by the due date.
- If the change size is too large (e.g. you had to modify over 1000-1500 lines without tests),
  you should raise a concern that the issue is too large to ship within one milestone,
  and you can make a suggestion that what can be done in the release and what can be done
  in the next milestone (i.e. issue split).
- In the PoC review, you should ask domain/performance/security experts or the other engineers who are familar with the context
  to review the technical approach and actual code. This is to ensure that there are no big red flags on the proposed solution.
- In the PoC review, you should ask PM and UX to review the feature's behavior.
  This is to ensure that the feature is correctly implemented and orthogonal to our problem to solve.
- You don't need to split MRs. It can be done later.
- You don't need to write neither tests nor documentation. It can be done later.
- You don't need to fix failed pipelines. You can fix the broken tests later. (Although, it might have some useful information)
- You don't need to write well-organized code. Refactoring can be done later.
- The PoC MR is not for merge, however, you can copy/paste the changes into the actual MRs, and poish later.
- Definition of done of PoC step is collecting approvals from PM, UX, domain/performance/security experts.
- Once it's done, you can estimate the number of MRs. For example, if the change size is 1000,
  you can split it to 5 MRs with 200 LoC. As you add tests in the actual development,
  the finial change size per MR would be doubled.
- Once it's done, you can close the PoC MR.

Technically, if you've done a PoC step, there is no need to ask additional reviews
in actual MRs as long as you don't change the feature behavior. You can simply
focus on general engineering review or documentation review, only.
For example, improving code quality, refactoring code, writing tests, writing documents, etc.

Here are the examples of PoC step. [Example 1](https://gitlab.com/gitlab-org/gitlab/merge_requests/16276),
[Example 2](https://gitlab.com/gitlab-org/gitlab/merge_requests/18115)

#### Issue template for feature development

This is an issue template for feature development. It includes some
important engineering tasks to be done in general.

<details>
<summary>Click me to collapse/fold the Issue template for feature development.</summary>
<pre>
```
<!-- The issue begins with "Problme to solve", "Intended users", "Proposal", etc.
     Read more https://gitlab.com/gitlab-org/gitlab/blob/master/.gitlab/issue_templates/Feature%20proposal.md -->

## Technical proposal

<!-- In this section, describe technical proposal for the problem to solve.
     It should give enough contexts to be able to be reviewed by domain/performance/security experts. -->

## Feature Flag

This feature is implemented behind `feature-flag-name` feature flag and disabled by default.
Once we've confirmed the feature is deemed stable, we remove the feature flag in order to publish the feature as GA.
<!-- Read more [Feature flags in development of GitLab](https://docs.gitlab.com/ee/development/feature_flags/) -->

## Planned MRs

### Backend

- [ ] [MR-1 title](MR link if it already exists)
- [ ] [MR-2 title](MR link if it already exists)

### Frontend

- [ ] [MR-1 title](MR link if it already exists)
- [ ] [MR-2 title](MR link if it already exists)

### General

- [ ] [Write a feature spec to test frontend and backend change altogether](MR link if it already exists)
- [ ] [Remove the feature flag and update documentation](MR link if it already exists) # i.e. publish the feature
```
</pre>
</details>

#### Issue template for feature evaluation (dogfooding, beta-tester program)

This is an issue template for feature evaluation. It includes some
important engineering tasks to be done in general.

When you enable a feature via feature flag and expect a significant impact on
user workflow or production load, you should create an issue with the following
template to communicate better with the affected users or SRE.

<details>
<summary>Click me to collapse/fold the Issue template for feature evaluation</summary>
<pre>
```
## Summary

<!-- The reason of feature evaluation. -->

## Feature details

<!-- Describes the feature details and expected performance or usability impact -->

## Timeline

<!-- This section describes the timeline of the evaluation.
     Example:
     - 2019-10-12 01:00 UTC The second evaluation: We will enable the feature on yyy project for X days.
     - 2019-10-11 01:00 UTC We've disabled the feature on xxx project because ...
     - 2019-10-10 01:00 UTC The first evaluation: We've enabled the feature on yyy project for X days.
-->

## How to enable the feature

<!-- Describes how to enable the feature that anyone can execute -->

## How to disable the feature

<!-- Describes how to disable the feature that anyone can execute.
     Consider, in an emergency case, someone else might need to disable the feature instead of you. -->

## Beginning of evaluation

- [ ] Announce in Slack/CompanyCall (T-minus 1 day)
- [ ] Enable the feature

## End of evaluation

- [ ] Announce in Slack/CompanyCall
- [ ] Disable the feature

## Feedback/Metrics

<!-- This section should be filled after the end of evaluation.
     You can collect metrics from user feedback or looking at Grafana, Sentry, Kibana, etc -->

### The second evaluation

<!-- The result of the second evaluation. e.g. We didn't observe any problmes. -->

### The first evaluation

<!-- The result of the first evaluation. e.g. We've found a crucial problem thus we need to fix YYY issue before the second evaluation. -->
```
</pre>
</details>


### Code Review

Code reviews follow the standard process of using the [reviewer roulette](https://docs.gitlab.com/ee/development/code_review.html#reviewer-roulette) to choose a reviewer and a maintainer. The roulette is optional, so if a merge request contains changes that someone outside our group may not fully understand in depth, it is encouraged that a member of the Release team be chosen for the preliminary review to focus on correctly solving the problem. The intent is to leave this choice to the discretion of the engineer but raise the idea that fellow Release team members will sometimes be best able to understand the implications of the features we are implementing. The maintainer review will then be more focused on quality and code standards.

This tactic also creates an environment to ask for early review on a WIP merge request where the solution might be better refined through collaboration and also allows us to share knowledge across the team.


#### Code Review Considerations

As part of the [12.9 Release Post retrospective](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/6698#note_334000537), we're taking part in an experiment whereby we should try to avoid review roulette and the maintainer bottleneck. In this pilot, try to assign reviewers / maintainers only from the [CI/CD Sub-department](/handbook/engineering/development/ci-cd/#who-we-are) and not by reviewer roulette.

Results: We will evaluate at the end of 13.1 if this was helpful using the metric `mean time to merge`. If this improves, we'll consider keeping this change for another milestone for further evaluation. If the impact on mean time to merge is detrimental, then we may consider dropping this process and reverting back to the previous roulette process.

### Async Status Updates

Since we are a [remote](/company/culture/all-remote/) company, we utilize a Slack plugin called [Geekbot](https://geekbot.io/) to coordinate various status updates. There are currently 3 status updates configured in Geekbot, one is weekly and two are daily:

#### Weekly Status Update

The **Weekly Status Update** is configured to run at noon on Fridays, and contains three questions:

1. ***What progress was made on your deliverables this week?*** (MRs and demos are good for this)

    The goal with this question is to show off the work you did, even if it's only part of a feature. This could be a whole feature, a small part of a larger feature, an API to be used later, or even just a copy change.

2. ***What do you plan to work on next week?*** (think about what you'll be able to merge by the end of the week)

    Think about what the next most important thing is to work on, and think about what part of that you can accomplish in one week. If your priorities aren't clear, talk to your manager.

3. ***Who will you need help from to accomplish your plan for next week?*** (tag specific individuals so they know ahead of time)

    This helps to ensure that the others on the team know you'll need their help and will surface any issues earlier.

#### Async Daily Standups

1. The **Daily Standup** is configured to run each morning Monday through Thursday and posts to `#g_progressive-delivery` Slack channel. It has just one question:

> ***Is there anything you could use a hand with today, or any blockers?***

This check-in is optional and can be skipped if you don't need any help, don't have any blockers or have not made any progress since the last check-in.

2. Our updates on the progress and status of a given feature will be added to the issue as a comment.
A status update comment should include the percentage complete of the work, the confidence of the person that their estimate is correct, and, optionally, any notes on what was done. Indications of other key steps like if review has started should also be included.
In the case where multiple developers are active on this issue, it could be good to include whether this is a front-end or back-end update if there are multiple people working on it. Finally, for each MR associated, please include an entry for each.

One of our colleagues, Nick Kipling, has created a tool called [TalTal](https://taltal.netlify.app/) to help make this process easier. _The use of the TalTal tool is totally optional_.

Below are some templates that you might find also useful.

**Please consider the following suggestions with regard to async issue updates**:
- If you work on an MR or an issue on a given day, add an async update. If you miss some, it won't be a big deal. For an in development issue where it's been many days since the last update, that shall be a cue for finding out what is happening (or giving an update explaining why things have stalled).
- Don't be strict on format, some issues just aren't as complicated as others. See also various templates below.
- Use these updates as a place to collect some technical notes and thoughts or "think out loud" as you work through an issue. This can create an opening for other engineers to engage and collaborate if they have ideas.

Examples templates:

```
## Async Update

#### Summary

<!-- general updates, probably what you would have otherwise noted in standup -->

#### MR Statuses

<!-- List of each MR with a quick status and/or update, this information is somewhat duplicated and could potentially be left off since the
MRs are listed at the top of the issue with labels identifying the workflow state, but I like that anyone reading
an update doesn't have to go searching for the MRs -->
* !99995 - in maintainer review, in doc review
* !99998 - in dev, 50% complete, 80% confident
```


```
Complete: 80%
Confidence: 90%
Notes: expecting to go into review tomorrow
```

```
It takes something more than intelligence to act intelligently.

Concern: ~frontend
Issue status: 20% complete, 75% confident
```

```
MR statuses:
!11111 - 80% complete, 99% confident - docs update - need to add one more section
!21212 - 10% complete, 70% confident - api update - database migrations created, working on creating the rest of the functionality next
```

#### Daily Social

The optional **Daily Social** is configured to run each morning and posts to #g_cicd_social. It has just one question:

1. ***What was something awesome, fun, or interesting that you did yesterday outside of work?***

    This check-in is optional and can be skipped if you don't have anything to share.

### Sync Meetings

Each week, we make time to sync with eachother on Tuesdays to share and discuss what's happening in the current week. Meetings are recorded and available for viewing later as needed.

In an effort to allow for everyone to participate in our weekly sync meetings despite our timezone challenges, we alternate our Tuesday sync meetings from **9am US Eastern Time on Week A** to **4:30pm US Eastern Time on Week B**.

### Async Retrospectives

We use [this project](https://gitlab.com/gl-retrospectives/release-stage/progressive-delivery/-/issues/) to organize issues directly related to and affecting our group including Retrospective Issues.

Retrospective Issues are created and delivered by the GitLab Bot and from configuration located in [async-retrospectives](https://gitlab.com/gitlab-org/async-retrospectives/-/blob/master/teams.yml).

#### Adding to the Wider Engineering Retrospective

Contributing to the wider engineering retrospective can be helpful for other groups and the engineering organization as a whole. To include items in the wider retrospective, we vote on items from our own retrospectives, positive or negative, with some affirmation style emoji like +1, 👍, 💯 on or before the 1st of each month. After voting, the EM will add it to [the agenda](https://docs.google.com/document/d/1nEkM_7Dj4bT21GJy0Ut3By76FZqCfLBmFQNVThmW2TY/edit) for that meeting.

See also:

- [Engineering Team Retrospectives](/handbook/engineering/management/team-retrospectives/)

####

### Async Iteration Retrospective Issues

We discuss iteration retrospectives using an asynchronous process similar to other planning processes where a well defined issue is available for the group to comment on our iteration successes, failures, and areas for improvement. Findings from that retrospective should inform our process refinements and should be captured here.

#### Previous Iteration retrospectives

* [Iteration Retrospective Issue - April 2020](https://gitlab.com/gl-retrospectives/release-stage/progressive-delivery/-/issues/3)
