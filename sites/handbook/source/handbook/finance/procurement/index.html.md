---
layout: handbook-page-toc
title: The Procurement Team
---

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc}

- TOC
{:toc}


## <i class="far fa-paper-plane" id="biz-tech-icons"></i> Contacting Procurement

#### How do I contact procurement?
1. If you have a general sense for your business needs that you need to purchase, open a [vendor contract request](/handbook/finance/procurement/vendor-contract-process) issue as identified under [Requesting Procurement Services](/handbook/finance/procurement/procurement-services/). You do NOT need a contract to open a contract request issue.
1. If you have a general question or are looking for direction, use the #procurement slack channel.

#### Why should I contact procurement? 
Team members can purchase goods and services on behalf of the company in accordance with the [Signature Authorization Matrix](/handbook/finance/authorization-matrix/) and guide to [Spending Company Money](/handbook/spending-company-money/). However, all purchases made on behalf of GitLab that are not a personal expense, must first be reviewed by procurement, then signed off by a member of the executive team. 

Finance vendor contract request issues are required for all third party purchases being made on behalf of GitLab that aren't considered a [personal reimbursable expense](/handbook/spending-company-money/#expense-policy). This ensures the organization can appropriately plan for spend and assess supplier risk. Exceptions to this are:

1.  Last minute un-planned onsite event needs such as food and rental transactions needed for event.
2.  One-time field marketing and event purchases less than $10K such as booth rentals, AV equipment, catering, etc.
     - In this instance, the vendor can invoice GitLab in Tipalti, and AP will route approvals based on the matrix

#### When should I contact procurement?
1. Contact procurement BEFORE you have a contract. 
1. **Create an issue 60-90 days before you need the service/product** to allow for best negotiations and terms.
1. The sooner procurement is engaged, the better.

## <i class="fas fa-stream" id="biz-tech-icons"></i> Procurement process flow
<div class="flex-row" markdown="0" style="height:100px;">
  <a href="/handbook/finance/procurement/prior-to-contacting-procurement/" class="btn btn-purple" style="width:170px;margin:5px;display:flex;align-items:center;height:100%;">1. Prior to contacting procurement</a>  
  <a href="/handbook/finance/procurement/procurement-services/" class="btn btn-purple" style="width:170px;margin:5px;display:flex;align-items:center;height:100%;">2. Requesting Procurement Services</a>
  <a href="/handbook/finance/procurement/vendor-contract-process" class="btn btn-purple" style="width:170px;margin:5px;display:flex;align-items:center;height:100%;">2. Vendor Contract Process</a>
  <a href="/handbook/finance/procurement/vendor-terms-and-conditions/" class="btn btn-purple" style="width:170px;margin:5px;display:flex;align-items:center;height:100%;">3. Vendor Terms and Conditions</a>
</div>

## <i class="fas fa-bullseye" id="biz-tech-icons"></i> Procurement Main Objectives

Procurement operates with three main goals
1.  Cost Savings
2.  Centralize Spending
3.  Compliance

## <i class="far fa-clock" id="biz-tech-icons"></i> Procurement Performance Indicators
Process of measurement in progress.

**Response times to initial requests for review <= 2 business days**
- Monthly average response time to new procurement purchase request issues within 2 business days. Issues with missing or incomplete information will be rejected after two consecutive attempts to follow up with the issue creator.

**Administer, maintain, and manage the procurement purchase request issue tracker (daily, ongoing) <= 2 busines days**
* Triage and assign issues in the procurement [issue board](https://gitlab.com/gitlab-com/Finance-Division/procurement-team/procurement/-/boards/1844091) within 2 business days of receipt.

**Ensure all contracts have the correct approvals in place before signed = 100%**
* Verify the correct approvers have approved all purchase requests according to the [Authorization Matrix](/handbook/finance/authorization-matrix/) before the contract is signed. 

**Ensure all fully executed vendor contracts are posted to ContractWorks = 100%**
* Upload all fully executed agreement to ContractWorks for accurate recording. Legal team will assign terms and fields.

**'Turn-Around' times <= 3 business days**
* Monthly average response times within purchase request issues for proposal updates, commercial constructs, procurement questions, etc.


## <i class="far fa-question-circle" id="biz-tech-icons"></i> Frequently asked questions


#### What if the Vendor I am working with doesn't require a contract?
1. GitLab requires a Vendor Contract even if your vendor doesn't.
1. The Vendor Contract approval process is designed to protect both you and the GitLab and the vendor. 
1. In this event legal can and will provide contract terms to govern the transaction based on the level of risk.
1. Open the appropriate vendor contract request to initiate the process

## <i class="far fa-flag" id="biz-tech-icons"></i> Important Considerations

#### Vendor Code of Ethics
All vendors that GitLab does business with, must legally comply with the [Supplier Code of Ethics](/handbook/people-group/people-policy-directory/#partner-code-of-ethics). When having discussions with your vendor regarding the contract, make them aware of this requirement.

#### Modern Slavery and Human Trafficking Compliance Program

GitLab condemns exploitation of humans through the illegal and degrading practices of human trafficking, slavery, servitude, forced labor, forced marriage, the sale/exploitation of chilren and adults and debt bondage (“Modern Slavery”).  To combat such illegal activities, GitLab has implemented this Modern Slavery and Human Trafficking Compliance Program.  

##### *Risk Areas and Markets*

While Modern Slavery can occur in any country and in any market, some regions and sectors present higher likelihood of vioaltions. Geographies with higher incidents of slavery are India, China, Pakistan, Bangladesh, Uzbekistan, Russia, Nigeria, Indonesia and Egypt. Consumer sectors such as food, tobacco and clothing are high risk sectors; but Modern Slavery can occur in many markets.*  
 (*According to Source: Statista, Walk Free Foundation, https://www.statista.com/chart/4937/modern-slavery-is-a-brutal-reality-worldwide/)  

##### *Actions to Address Modern Slavery Risks*

All vendors, providers and entities providing services or products to GitLab (“Vendors”) are expected to comply with [GitLab’s Partner Code of Ethics](/handbook/people-group/people-policy-directory/#partner-code-of-ethics) which specifically addresses Modern Slavery laws.  Compliance with the Partner Code of Ethics will be included in Vendor contracts and/or purchase orders going forward.  Existing Vendor contracts will be updated with the appropriate language upon renewal.

Those entities who are of higher risk or whom GitLab suspects may be in violation of Modern Slavery laws, may be required to complete an audit.  Audits may be presented in the form of a questionnaire or may be an onsite visit.  Any known or suspected violations will be raised to Legal and/or Compliance.  Failure to comply with Modern Slavery laws will result in a termination of the relationship and GitLab retains all rights in law and equity.

Vendors understand and agree that violations of Modern Slavery laws may require mandatory reporting to governing authorities. GitLab has discretion if and how to best consult with Vendors for purposes of Modern Slavery reporting. GitLab is senstive to and will take into consideration, the relationship and the risk profile of Vendor to ensure that Modern Slavery risks have been appropriately identified, assessed and addressed and that the Vendor is aware of what actions it needs to take.

##### *Assessment of Effectiveness*

GitLab will review its Modern Slavery and Human Trafficking Compliance Program on an annual basis or sooner if it is determined there is increased exposure or concerns with overall compliance.   The Program may be amended from time to time by GitLab, to ensure compliance with the most current Modern Slavery laws and regulations.

##### *Compliance Program Approval*

GitLab’s Executive Team reviewed and approves this Modern Slavery and Human Trafficking Compliance Program.

## <i class="fas fa-book" id="biz-tech-icons"></i> Related Docs and Templates

#### Documentation

* [Uploading Third Party Contracts to ContractWorks](/handbook/legal/vendor-contract-filing-process/)
* [Compliance](/handbook/legal/global-compliance/) - general information about compliance issues relevant to the company
* [Company Information](https://gitlab.com/gitlab-com/Finance-Division/finance/-/wikis/company-information) - general information about each legal entity of the company
* [Trademark](/handbook/marketing/growth-marketing/brand-and-digital-design/brand-guidelines/#trademark) - information regarding the usage of GitLab's trademark
* [Authorization Matrix](/handbook/finance/authorization-matrix/) - the authority matrix for spending and binding the company and the process for signing legal documents

##### Contract Templates

- [Mutual Non-Disclosure Agreement (NDA)](https://drive.google.com/file/d/1kQfvcnJ_G-ljZKmBnAFbphl-yFfF7W5U/view?usp=sharing)
- [Logo Authorization Template](https://drive.google.com/file/d/1Vtq3UHc8lMfIbVFJ3Mc-PZZjb6_CKAvm/view?usp=sharing)


