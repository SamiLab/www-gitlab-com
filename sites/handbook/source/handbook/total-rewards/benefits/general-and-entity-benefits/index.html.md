---
layout: handbook-page-toc
title: General & Entity Specific Benefits
description: A list of the General & Entity Specific Benefits that GitLab offers.
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

For the avoidance of doubt, the benefits listed below in the General Benefits section are available to contractors and team members, unless otherwise stated. Other benefits are listed by countries that GitLab has established an entity or co-employer and therefore are applicable to team members in those countries only via our entity specific benefits. GitLab has also made provisions for Parental Leave which may apply to team members and contractors but this may vary depending on local country laws. If you are unsure please reach out to the Total Rewards team.

## General and Entity Benefits
- [GitLab BV (Netherlands)](/handbook/total-rewards/benefits/general-and-entity-benefits/bv-benefits-netherlands)
- [GitLab BV (Belgium)](/handbook/total-rewards/benefits/general-and-entity-benefits/bv-benefits-belgium)
- [Global Upside (India)](/handbook/total-rewards/benefits/general-and-entity-benefits/global-upside-benefits-india)
- [GitLab Inc (US)](/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us)
- [GitLab Inc (China)](/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-china)
- [GitLab LTD (UK)](/handbook/total-rewards/benefits/general-and-entity-benefits/ltd-benefits-uk)
- [GitLab GmbH (Germany)](/handbook/total-rewards/benefits/general-and-entity-benefits/gmbh-benefits-germany)
- [GitLab PTY (Australia & New Zealand)](/handbook/total-rewards/benefits/general-and-entity-benefits/pty-benefits-australia)
- [GitLab Canada Corp](/handbook/total-rewards/benefits/general-and-entity-benefits/canada-corp-benefits)
- [Remote (South Africa and Mexico)](/handbook/total-rewards/benefits/general-and-entity-benefits/remote-com)
- [Safeguard (Ireland, Hungary, Spain, Italy, France, Brazil, South Africa and Switzerland)](/handbook/total-rewards/benefits/general-and-entity-benefits/safeguard/)

### Benefits Available to Contractors

Contractors of GitLab BV are eligible for the [general benefits](/handbook/total-rewards/benefits/#general-benefits), but are not eligible for entity specific benefits as they have an additional 17% added to the [compensation calculator](/handbook/total-rewards/compensation/compensation-calculator). A contractor may bear the costs of their own health insurance, social security taxes, payroll administration, tax details and so forth, leading to a 17% higher compensation for the contractor within the calculator.

Our contractor agreements and employment contracts are all on the [Contracts](/handbook/contracts/) page.

## General Benefits

### Spending Company Money

GitLab will [pay for the items you need to get your job done](/handbook/spending-company-money).

### Stock Options

[Stock options](/handbook/stock-options/) are offered to most GitLab team members. We strongly believe in team member ownership in our Company. We are in business to create value for our shareholders and we want our team members to benefit from that shared success.

### Life Insurance

In the unfortunate event that a GitLab team member passes away, GitLab will provide a [$20,000](/handbook/total-rewards/compensation/#exchange-rates) lump sum to anyone of their choosing. This can be a spouse, partner, family member, friend, or charity.
* For US based team members of GitLab Inc., this benefit is replaced by the [Basic Life Insurance](/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us/#basic-life-insurance-and-add).
* For all other GitLab team members, the following conditions apply:
  * The team member must be either an employee or direct contractor.
  * The team member must have indicated in writing to whom the money should be transferred. To do this you must complete the [Expression of wishes](https://docs.google.com/document/d/1bBX6Mn5JhYuQpCXgM4mkx1BbTit59l0hD2WQiY7Or9E/edit?usp=sharing) form. To do this, first copy the template to your Google Drive (File -> Make a copy), enter your information, sign electronically. To sign the document, use a free document signing program like [smallpdf](https://smallpdf.com/sign-pdf) or [HelloSign](https://app.hellosign.com/); or you can print it, sign and digitize. Sign, save as a pdf and upload to your Employee Uploads folder in BambooHR.
  * For part-time GitLab team members, the lump sum is calculated pro-rata, so for example for a team member that works for GitLab 50% of the time, the lump sum would be [$10,000](/handbook/total-rewards/compensation/#exchange-rates).

### Paid Time Off

GitLab has a "no ask, must tell" time off policy [time off policy](/handbook/paid-time-off) per 25 consecutive calendar days off.

### Tuition Reimbursement

GitLab supports team members who wish to continue their education and growth within their professional career. GitLab team-members are eligible for a reimbursement of up to [20,000 USD](/handbook/total-rewards/compensation/#exchange-rates) per calendar year (January 1st - December 31st) depending on tenure, performance, company need for the learned skill, and available budget. A course is considered to be included in the calendar year in which the course is paid/reimbursed (which should also be the same calendar year in which the course ends). 

Follow the [Expensify process](/handbook/finance/expenses/#work-related-online-courses-and-professional-development-certifications) if the course is under [$500 USD](/handbook/total-rewards/compensation/#exchange-rates).

If the course is over [$500 USD](/handbook/total-rewards/compensation/#exchange-rates) or if you have exceeded your allotted [$500 USD](/handbook/finance/expenses/#work-related-online-courses-and-professional-development-certifications) limit for courses and professional development certifications, follow the steps below. 


#### Tuition Reimbursement FAQ

**Can I participate in this program?**
* If you are a full-time GitLab team member and have been employed for more than three months, you are eligible to participate in this program.

**What courses are eligible for reimbursement?**
* Courses must be a requirement of a degree or certification program and delivered through a credentialed college or university or effective online education such as [Udacity](https://www.udacity.com/).
* There is no limit to the number of years a team member can participate in the program. 
* Courses eligible for reimbursement include classes for credit resulting in a grade (not pass/fail), courses providing continuing education credits, and/or courses taken as part of a certification program. You must earn a passing grade equivalent to a “B” or obtain a successful completion certification to submit for reimbursement.
* A completed [Tuition Reimbursement Agreement](https://docs.google.com/document/d/1-WvPzoUMpcaUUOm3Gl5eDKQDw_-TtI4J0LP5ifMa8Vo/edit) (per the process below) along with a final grade report or satisfactory certificate of completion are required to receive reimbursement.
* Budget estimations for tuition reimbursement are based on utilization rates from past years, adjusted for the size of the company. The budget is managed on a company-wide scale, not division-specific. For example, approving tuition reimbursement for a team member in Engineering does not "take away" budget from other engineers. Eligibility is managed on an individual basis. Eligibility for this benefit is part of each GitLab team member's Total Rewards package. 
* The course must be relevant to your role's goals and development:

Examples of requests that may be approved:

* A Backend Engineer who is not on a performance improvement plan seeking a bachelor's degree in Computer Science.
* A People Operations Generalist seeking a master's in Human Resources with a concentration in International Employee Relations.
* A Security Analyst seeking to take courses to gain a Cybersecurity certificate through an accredited college or university.
* A Product Marketing Manager seeking to take courses to become a Certified Brand Manager from The Association of International Product Marketing and Management.
* A Technical Account Manager seeking to obtain a Cloud Certification such as CompTIA Cloud, AWS Certified Solutions Architect, MicroSoft MCSA/MCSE, Cisco CCNA/CCNP.

Examples of requests that may be denied:

* A Marketing Manager seeking a master's in Human Resources, but has no intention of applying to a role in people ops.
* A Frontend Engineer seeking a master's in computer science who is on a performance improvement plan or having discussions around underperformance.
* If the tuition reimbursement budget has been reached, then your request may be denied, even if you meet eligibility requirements.

**Do I need to get approval to participate in this program?**
* Yes, seek verbal approval from your manager before you submit your tuition reimbursement form.
* Approval will also be obtained from the e-group member in charge of your organization via Hello Sign in accordance with the [signature authorization policy](/handbook/finance/authorization-matrix/)

**I'm planning on taking a degree program, do I need approval for each class or for each semester/term?**
* A [Tuition Reimbursement Agreement](https://docs.google.com/document/d/1-WvPzoUMpcaUUOm3Gl5eDKQDw_-TtI4J0LP5ifMa8Vo/edit) will be needed for each semester, however, the relevance of the classes taken for each semester will be subject to the approval of the manager and e-group leader.

**When should I apply for Tuition Reimbursement?**
* The [Tuition Reimbursement Agreement](https://docs.google.com/document/d/1-WvPzoUMpcaUUOm3Gl5eDKQDw_-TtI4J0LP5ifMa8Vo/edit) (as noted in the process below), must be submitted to Total Rewards at least 30 days before your course start date.

**What is covered in the tuition reimbursement program?**
* The program will cover only the tuition and enrollment related fees. Additional fees related to parking, books, supplies, technology, or administrative charges are not covered as part of the program. Tuition will be validated by receipt showing proof of payment. 

**When will I receive my tuition reimbursement?**
* Your tuition reimbursement will be paid in the following pay cycle after you have completed the course and sent your official grade report or successful certification of completion to Total Rewards.
* If the program does not allow for tuition deferment and this would cause financial strain, you can request to receive 50% of the reimbursement up front and the other 50% upon successful completion of the course. 

**What happens if I leave GitLab?**
* If you voluntarily terminate employment with GitLab after completion of the course and prior to completing twelve consecutive months of active employment after completion of the course, you will refund the entire amount of the educational expenses provided to you.


#### Tuition Reimbursement Process

To receive tuition reimbursement, GitLab team members should follow the following process:

1. Discuss your interest in professional development with your manager. If the manager agrees that the degree or certification program is aligned with the business and growth opportunities within GitLab, fill out the **first** page of the [Tuition Reimbursement Application Form](https://docs.google.com/document/d/1-WvPzoUMpcaUUOm3Gl5eDKQDw_-TtI4J0LP5ifMa8Vo/edit).
1. If you are participating in a degree program, a Tuition Reimbursement Application Form will be needed for each semester or term. If this is the case, list down the course codes and names of the courses you are taking under the "Course" section of the form. 
1. Once you have filled out the first page of the form, share or forward it to total-rewards@ domain.
1. The Total Rewards will stage the second page for signatures in Hello Sign.
1. The Total Rewards Analyst will file the application and signed agreement in BambooHR.
1. The Total Rewards Analyst will also log the tuition reimbursement in the "Tuition Reimbursement Log" found on the Google Drive.
1. Once the course is completed, an official grade report or successful certification of completion must be submitted to Total Rewards.
1. After grades are verified, Total Rewards will ensure the reimbursement is processed through the applicable payroll by the second pay cycle after submission.

#### Tax Implications for Tuition Reimbursement by Country

In some countries, tuition reimbursement may be considered as taxable income and can be (partially) exempted from personal income taxes or subject to employer withholding taxes. 

For example, in the United States 2020 tax year if GitLab [pays over $5,250 for educational benefits for you during the year, you must generally pay tax on the amount over $5,250](https://www.irs.gov/newsroom/tax-benefits-for-education-information-center).

Please contact [Payroll](mailto:payroll@gitlab.com) or [People Ops](mailto:peopleops@gitlab.com) when you have questions.

#### Tuition Reimbursement for English Language Courses

As GitLab expands globally, we want to support our team members where English may not be their first language. Team members are able to use the Tuition Reimbursement policy in order to take classes or courses that develop and strengthen their English language skills. Managers are encouraged to recommend this benefit to team members whose engagement as a part of GitLab and/or performance may be improved by further developing their English language skills.

Any full-time GitLab team member is eligible to participate in this program. Courses offered in-person or online by a credential college or university or an English language school are eligible for reimbursement.

To receive this reimbursement, GitLab team members should complete the following process:

1. The team member should first discuss and receive approval from their manager.
1. The team member should forward their manager's approval along with information on the English course including cost to the Total Rewards Analyst at total-rewards@domain.
1. The Total Rewards Analyst will notify payroll to process the reimbursement by the second pay cycle after submission.

Note: The above process is not required if the course is being offered to you at no cost.

Examples of English Language Courses:
 * [Coursera](https://www.coursera.org/browse/language-learning/learning-english) offers a wide variety of online English courses in partnership with various reputable colleges and universities. Their courses range from improving your English language skills to more specialized courses that focus on English for STEM or career development
 * English Language Courses offered in-person or online by a local college or university or English language courses offered online by a non-local college or university.
 * Please add any recommendations to this list.

### GitLab Contribute
Every nine months or so GitLab team members gather at an exciting new location to [stay connected](/blog/2016/12/05/how-we-stay-connected-as-a-remote-company/), at what we like to call [GitLab Contribute](/company/culture/contribute). It is important to spend time face to face to get to know your team and, if possible, meet everyone who has also bought into the company vision. There are fun activities planned by our GitLab Contribute Experts, work time, and presentations from different Departments to make this an experience that you are unlikely to forget! Attendance is optional, but encouraged. For more information and compilations of our past events check out our [previous Contributes (formerly called GitLab Summit)](/company/culture/contribute/previous).

### Business Travel Accident Policy

[This policy](https://drive.google.com/a/gitlab.com/file/d/0B4eFM43gu7VPVl9rYW4tXzIyeUlMR0hidWIzNk1sZjJyLUhB/view?usp=sharing) provides coverage for team members who travel domestic and internationally for business purposes. This policy will provide Emergency Medical and Life Insurance coverage should an emergency happen while you are traveling. In accompaniment, there is coverage for security evacuations, as well a travel assistance line which helps with pre-trip planning and finding contracted facilities worldwide.
   * Coverage:
      - Accidental Death [enhanced coverage]: 5 times Annual Salary up to USD 500,000.
      - Out of Country Emergency Medical: Coverage up to $250,000 per occurrence. If there is an injury or sickness while outside of his or her own country that requires treatment by a physician.
      - Security Evacuation with Natural Disaster: If an occurrence takes place outside of his or her home country and Security Evacuation is required, you will be transported to the nearest place of safety.
      - Personal Deviation: Coverage above is extended if personal travel is added on to a business trip. Coverage will be provided for 25% of length of the business trip.
      - Trip Duration: Coverage provided for trips less than 180 days.
      - Baggage & Personal Effects Benefit: $500 lost bag coverage up to 5 bags.
   * For any assistance with claims, please reference the [claims guide (internal only)](https://drive.google.com/file/d/1vmLjhebsf81N8oSxqlCihYg5q1WT8Efw/view?usp=sharing).
   * This policy will not work in conjunction with another personal accident policy as the Business Travel Accident Policy will be viewed as primary and will pay first.
   * For more detailed information on this benefit, please reference the [policy document](https://drive.google.com/file/d/1ktx_mhlEYyQoLrQJ7DhIcibQhrlnB-lb/view?usp=sharing).
   * If you need a confirmation of coverage letter, please reference the [visa letter generation document (internal only)](https://drive.google.com/file/d/1oesZnp-fVWWCakVejB7nTV39lntMFnSd/view?usp=sharing).
   * For any additional questions, please contact the Total Rewards Analyst.

### Immigration

GitLab offers benefits in relation to [obtaining visas and work permits](/handbook/people-group/visas/) for eligible team members.

### Employee Assistance Program

GitLab offers an Employee Assistance Program to all team members via [Modern Health](/handbook/total-rewards/benefits/modern-health).

### Incentives

The following incentives are available for GitLab team members:
   - [Discretionary Bonuses](/handbook/incentives/#discretionary-bonuses)
   - [Referral Bonuses](/handbook/incentives/#referral-bonuses)
   - [Visiting Grant](/handbook/incentives/#visiting-grant)

### All-Remote

GitLab is an [all-remote](/blog/2018/10/18/the-case-for-all-remote-companies/) company; you are welcome to [read our stories](/company/culture/all-remote/stories/) about how working remotely has changed our lives for the better.

You can find more details on the [All Remote](/company/culture/all-remote/) page of our handbook.

_If you are already a GitLab employee and would like to share your story, simply add a `remote_story:` element to your entry in `team.yml` and it will appear
on that page._

### Part-time contracts

As part of our Diversity, Inclusion & Belonging  value, we support [Family and friends first](/handbook/values/#family-and-friends-first-work-second) approach. This is one of the many reasons we offer part-time contracts in some teams.

We are growing fast and unfortunately, not all teams are able to hire part-time team members yet. There are certain positions where we can only hire full-time team members.

Candidates can ask for part-time contract during the interview process. Even when a team they are interviewing for can't accept part-time team members, there might be other teams looking for the same expertise that might do so. If you are a current team member and would like to switch to a part-time contract, talk to your manager first.

### Parental Leave

GitLab offers anyone (regardless of gender) who has been at GitLab for six months and completed a [probationary period](/handbook/contracts/#probation-period) (if applicable) up to **16 weeks of 100% paid time off** during the first year of parenthood. This includes anyone who becomes a parent through childbirth or adoption. We encourage parents to take the time they need. GitLab team-members will be encouraged to decide for themselves the appropriate amount of time to take and how to take it.

For many reasons, a team member may require more time off for parental leave. Many GitLab members are from countries that have longer standard parental leaves, occasionally births have complications, and sometimes 16 weeks just isn't enough. Any GitLab team member can request additional unpaid parental leave, up to 4 weeks. We are happy to address anyone with additional leave requests on a one-on-one basis. All of the parental leave should be taken in the first year.

If you have been at GitLab for a six months and completed your probationary period your parental leave is fully paid. If you've been at GitLab for less than a six months it depends on your jurisdiction. If applicable, commissions are paid while on parental leave based on the prior six months of performance with a cap at 100% of plan. For example, if in the six months prior to starting parental leave you attained 85% of plan, you will be compensated at the same rate while on leave. On the day you return from leave and going forward, your commissions will be based on current performance only.

You are entitled to and need to comply with your local regulations. They override our policy.

To [alleviate the stress](/handbook/paid-time-off/#returning-to-work-after-parental-leave) associated with returning to work after parental leave, GitLab supports team members coming back at [50% capacity](/handbook/paid-time-off/#returning-to-work-at-50-capacity). Parents at GitLab who are reentering work following parental leave are encouraged to reach out to team members who self-designate as a [Parental Leave Reentry Buddy](/handbook/total-rewards/benefits/parental-leave-toolkit/).

Managers of soon to be parents should check out this [Parental Leave Manager Tool Kit](/handbook/total-rewards/benefits/parental-leave-toolkit/#manager-tool-kit) for best practices in supporting your team members as they prepare for and return from Parental Leave.

If you're interested in learning about how other GitLab team members approach parenthood, take a look at [the parenting resources wiki page](https://gitlab.com/gitlab-com/gitlab-team-member-resources/wikis/parenting) and [#intheparenthood](https://gitlab.slack.com/messages/CHADS8G12/) on Slack.

**Parental Leave FAQs**

I'm planning to take Parental Leave, what should I do before my leave starts?
* Firstly, celebrate as this is an exciting time! Congratulations to you and your family! Some teams require more time to put a plan of action in place so we recommend communicating your plan to your manager at least 3 months before your leave starts. In the meantime, familiarize yourself with the steps below and specific leave requirements in your country (if any). You must initiate your Parental Leave at least 30 days before the leave starts.

How do I initiate my Parental Leave?
* Submit your time off by selecting the Parental Leave category in PTO Ninja at least 30 days before your leave starts. Your manager and the Total Rewards team will get notified after you submit your leave. Please note, even though we have a "no ask, must tell" Parental Leave Policy, some countries require extra paperwork or notifications to a PEO so it's important that the Total Rewards team is aware of your leave at least 30 days before your leave starts.

Will I get a confirmation that Total Rewards received my Parental Leave request?
* Yes, Total Rewards will send you an e-mail within 48 hours confirming that they've been notified of your Parental Leave dates. 

Can I speak to someone about my Parental Leave?
* You are more than welcome to send an e-mail to Total Rewards if you have any questions about taking Parental Leave. You may also send us an e-mail to schedule a 1:1 call with Total Rewards if you need any support regarding Parental Leave.

When does my 16 weeks of Parental Leave start?
* This period starts on the first day that you take off. This day can be in advance of the day that the baby arrives.

Do I have to take my Parental Leave in one continuous period?
* No, we encourage you to plan and arrange your Parental Leave in a way that suits you and your family's needs. You may split your Parental Leave dates as you see fit, so long as it is within the 12 months of the birth or adoption event.

When does my Parental Leave coverage begin if I don't meet the initial requirements for paid leave? 
* GitLab payroll coverage begins once you meet the requirements. Until then, you will not receive pay.  If for example, you are someone who qualifies after 6 months at Gitlab and then goes on leave at the start of your 120th day at GitLab, you would not receive payment from GitLab for the first 60 days. You would receive payment from GitLab for up to 60 additional days taken within a year from the birth event.

Can I change the dates of my Parental Leave?
* Absolutely, you can edit your Parental Leave dates via PTO Ninja. Total Rewards will receive a notification everytime you edit your Parental Leave dates. Make sure your leave is under the Parental Leave category, otherwise Total Rewards won't get a notification.
* Please note, if you are planning to change or extend your Parental Leave by using a different type of leave such as PTO, unpaid leave or any local statutory leave, please send an e-mail to Total Rewards.

Is the 16-week balance per birth or adoption event?
* Yes, you may take up to 16 weeks of Parental Leave per birth or adoption event.

I haven't been at GitLab for 6 months, can I go on Paid Time Off to spend time with my family?
* Of course, please refer to our [Paid Time Off policy](/handbook/paid-time-off/#paid-time-off).

What support is available after I return from Parental Leave?
* This [handbook page](/handbook/paid-time-off/#returning-to-work-after-parental-leave) has resources for team members returning to work after parental leave.
* You may [return to work at 50% capacity](/handbook/paid-time-off/#returning-to-work-at-50-capacity). Please note, if you wish to use your Parental Leave on a part-time basis, you may arrange this via PTO Ninja (for instance, taking 2 days of Parental Leave out of 5 weeks for a certain period of time). If you have used up your Parental Leave balance of 16 weeks and you wish to return to work on a part-time capacity, discuss your options with your manager first.

Is there anything else that I need to know to initiate my Parental Leave?
* Some countries require extra paperwork or have specific leave requirements, which are subject to change as legal requirements change. Please take a look at your country's leave policy:

* [GitLab Inc. United States Leave Policy](/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us/#gitlab-inc-united-states-leave-policy)
    * [GitLab Inc. China Leave Policy](/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-china/#gitlab-inc-china-leave-policy)
    * [GitLab B.V. Netherlands Leave Policy](/handbook/total-rewards/benefits/general-and-entity-benefits/bv-benefits-netherlands/#gitlab-bv-netherlands-leave-policy)
    * [GitLab B.V. Belgium Leave Policy](/handbook/total-rewards/benefits/general-and-entity-benefits/bv-benefits-belgium/#gitlab-bv-belgium-leave-policy)
    * [Global Upside India Leave Policy](/handbook/total-rewards/benefits/general-and-entity-benefits/global-upside-benefits-india)
    * [GitLab LTD United Kingdom Leave Policy](/handbook/total-rewards/benefits/general-and-entity-benefits/ltd-benefits-uk/#gitlab-ltd-united-kingdom-leave-policy)
    * [GitLab GmbH Germany Leave Policy](/handbook/total-rewards/benefits/general-and-entity-benefits/gmbh-benefits-germany/#gitlab-gmbh-germany-leave-policy)
    * [GitLab PTY Australia Leave Policy](/handbook/total-rewards/benefits/general-and-entity-benefits/pty-benefits-australia/#gitlab-pty-australia-parental-leave-administrative-details)

**Process for Total Rewards Analysts**
  * PTO Ninja will notify Total Rewards of any upcoming parental leave.
  * Log and monitor upcoming parental leave in the "Parental Leave Log" Google sheet on the drive.
  * Notify the team member that the parental leave request was received by sending the confirmation e-mail. 
  * Notify payroll - add the start date and end date on the payroll changes file, under the appropriate pay cycle date.
  * Update the BambooHR Employment Status of the team member to "Parental Leave" with the start date of the leave.
  * Add another entry with the date the team member will return from leave with the status "End of Parental Leave". This will generate a notification to the Total Rewards team 1 day before the team member returns from leave.
  * Confirm the team member's return by sending the return to work e-mail. 
  * If the dates in PTO Ninja remain the same, update the team member's BambooHR status to "Active" upon the team member's return.
  * If the dates in PTO Ninja change, update payroll and the BambooHR status accordingly.
