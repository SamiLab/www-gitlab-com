---
layout: handbook-page-toc
title: "Remote.com"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

----

The following benefits are provided by [Remote](https://www.remote.com/) and apply to team members who are contracted through Remote. If there are any questions, these should be directed to the Total Rewards team at GitLab who will then contact the appropriate individual at Remote.

## South Africa
* Medical Insurance allowance (300 USD per month for team members / 500 USD per month for team members plus dependents) Proof of cover must be shown to the total rewards team before the allowance will be paid via payroll. 
    * TODO: Total Rewards to outline with Payroll how this will be administered 
* Provident fund match - 5% employee & 5% employer
    * TODO: Work with Remote to outline administration of the benefit. 
* Life Cover of 4 x Annual Salary 
    * TODO: Work with Remote to outline administration of the benefit. 
* Income protection of 75% of salary (scheme rules apply, Remote will provide documentation upon sign up)
* Severe Illness of 2 x Annual Salary 
    * TODO: Work with Remote to outline administration of the benefit. 
* Funeral Benefit of R30,000 paid to a family member.
    * TODO: Total Rewards to outline details on how the above mentioned benefits will be administered

## Mexico

### Social Security

All workers employed in Mexico must be registered with and contribute to the following institutions that deal with different social security insurance benefits:
* Mexican Institute of Social Security (IMSS):
    * Provides medical services, child day care, accident and sickness compensation, pregnancy benefits and disability pensions
* National Workers’ Housing Fund Institute (INFONAVIT):
    * Provides subsidized housing to employees as well as loans, and the Retirement Savings Program (SAR). SAR provides employees with retirement benefits when they reach 65 years of age.

Employees in Mexico are covered by the Social Security Law, under IMSS who is responsible for administering social security insurance benefits and the collection of contributions.
* Both the employer and employee are required to contribute to social security, although the employer has the responsibility of withholding the employee’s contribution.
* These contributions fund retirement pensions, health and maternity insurance, occupational risk, day-care, disability and life insurance, and unemployment/old age insurance.

### Medical Benefits

* Medical Insurance allowance of 5000 USD per annum (Details to be added by Total Rewards)

### Life Insurance

* Life insurance cover provided via Remote (Details to be added by Total Rewards)

### Christmas Bonus (Aguinaldo)

* GitLab offers 30 working days pay (which includes total earnings + taxable allowances + commissions)
* Paid by December 20th (so the employee can use it for the holiday)
* Employees with less than one year of service will recieve a pro-rated christmas bonus. 

### Vacation Bonus (Prima)

The vacation premium is an additional cash benefit given to employees for use on their vacation. It is calculated as a minimum of 25% of daily salary multiplied by the number of days of vacation. Employees who have provided one year of service must be afforded a minimum of 6 paid vacation days in their first year of employment. Two working days will be added to that vacation time every following year through the fourth year. After five years, employers are required to add two days of vacation time every five years.
* Here is a chart on how vacations days increase:

| Year(s)  | Days |
|----------|------|
| 1        | 6    |
| 2        | 8    |
| 3        | 10   |
| 4        | 12   |
| 5 to 9   | 14   |
| 10 to 14 | 16   |
| 14 to 19 | 18   |

**Calculation:**
* To calculate the vacation bonus based on 25% and a salary of $50,000 USD per year:
* $50,000 USD divided by 365 days = $136.99 USD daily pay rate
* $136.99 USD daily pay rate multiplied by 25% = $34.25 USD
* $34.25 USD multiplied by 6 vacation days (first year) = $205.50 USD vacation bonus
* It can be paid right away when the vacation is taken, or as a lump sum upon completing one year of service (or employment ends). It is the employer’s choice which method to use.
* Unused leave must also be paid when employment ends (in other words, the employee does not ‘forfeit’ unused vacation time)

### Remote Mexico Leave Policy

#### Statutory Maternity Leave

The statutory entitlement for maternity leave is 12 weeks (6 weeks before the child is born and 6 weeks after birth). The employee can negotiate the leave period before the birth of the child in an agreement with the employer in order to partially enjoy them after the birth of the child. During maternity leave, women are entitled to the benefits that they would normally receive.

* In addition to the following rules apply: 
    * Pregnant women may not work under the hazardous conditions; 
    * Perform industrial tasks during the night, extraordinary hours or during sanitary contingencies; 
    * Their salary, benefits, and rights should not be affected.
* When an employee return from maternity leave, the employee is entitled to her employment, provided that no more than 1 year has passed since the date of delivery.
* Maternity leave does not affect the longevity of service. During maternity leave, the Mexican Social Security Institute will pay the working mother 100% of her daily salary as a social security contribution.
* If the maternity leave period is extended, she is entitled to 50% of the daily salary of social security contribution for a period of up to 60 days.

#### Statutory Paternity Leave

A male team member is entitled to 5 working days as paternity leave.

### Sick Leave

Team members unable to work because of a nonwork-related injury or illness and who have made payments into the social security system for the four weeks before the condition developed are eligible for paid sick leave through the Social Security Institute. The benefit, which is 60% of an employee’s regular wage, is paid from the fourth day of the illness for up to 52 weeks and maybe extended for another 52 weeks. 
 