---
layout: handbook-page-toc
title: "Promotions and Transfers"
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

At GitLab, we encourage team members to take control of their own career advancement. We have no minimum time in role requirements for promotions or transfers at GitLab. For all title changes, the team member should first speak with their manager to discuss a personal [professional development plan](/handbook/people-group/learning-and-development/career-development/). If you feel your title is not aligned with your skill level, come prepared with an explanation of how you believe you meet the proposed level and can satisfy the business need.

As a manager, please follow the following processes, discuss any proposed changes with either the People Business Partner or Chief People Officer, and do not make promises to the team member before the approvals are completed.

We recorded a training on this subject:

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/TNPLiYePJZ8" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## Definitions

- Promotions occur when a team member increases in level within the same job family. For example, a Backend Engineer is promoted to a Senior Backend Engineer. Similarly, a Senior Backend Engineer would receive a promotion by moving to a Staff Backend Engineer or Manager, Engineering.
- Transfers occur when someone changes job families. A Backend Engineer would transfer to a Site Reliability Engineer.
- Change in specialty has no impact to job family. Therefore, this is not a promotion or a transfer.

## Career Mobility Issue

A [Career Mobility Issue](https://gitlab.com/gitlab-com/people-group/employment-templates/-/blob/master/.gitlab/issue_templates/career_mobility.md) is created when the one of the following criteria is met;

- Migration from Individual Contributor to Manager
- Migration from Manager to Individual Contributor
- Migration of Team

## Career Mobility Issue Creation Process

The Total Rewards Team will notify the People Experience Team of a pending migration of a team member via @ mention in the Promotion/Transfer Tracker. The [People Experience Associate](https://about.gitlab.com/job-families/people-ops/people-experience-associate/) currently in the assignment rotation will assign the migration to an Associate in the People Exp/Ops Tracker.

The [Career Mobility Issue](https://gitlab.com/gitlab-com/people-group/employment-templates/-/blob/master/.gitlab/issue_templates/career_mobility.md) will then be **created by the People Experience Associate** assigned by using the [automated Slack command](/handbook/people-group/engineering/employment-issues/#career-mobility-issues) one day prior to the effective date.

Important things to ensure:

1. Add a due date of two weeks from the migration effective date.
1. Check to see that the previous Manager and new Manager is listed correctly in the issue.
1. Complete all applicable tasks under the People Experience list.

## Important Tasks once Career Mobility has been finalised

1. This needs action from both the current and new managers to set the migrating team member up for success in their new role. This may include:

- Creating the correct Access Requests for systems needed and for systems no longer needed.
- Create any training issue that may be required.
- Reminding the team member to update their title on the team page.
- If we are in the middle of company wide 360 reviews, it is encouraged that the current manager and new manager arrange a successful handover of the feedback, whether sync or async.

## Compliance

The People Experience Associate completes a bi-weekly audit of all transition issues that are still open and checks that all tasks have been completed by all members applicable. In the event that tasks are still outstanding, the People Experience Associate will ping the relevant team members within the transition issue to call for tasks to be completed.

Once all tasks have been completed and the issue is still open, the People Experience Associate will close the transition issue accordingly. The transitioning team member can also close the issue once satisfied all the tasks are complete.

All migration tasks by the applicable team members needs to be completed within 2 weeks of the migration start date.

## BambooHR or Greenhouse Process

### Greenhouse 
* Promotions or Applications to Manager level roles: All managers will need to apply for the open position in [Greenhouse](/handbook/hiring/interviewing/#internal-applicants). They will go through the interview and approval process in Greenhouse to ensure anyone who would want to apply has the opportunity.
  * Example: Senior Backend Engineer applies to become a Manager, Engineering
* Interim Roles: Interim roles at all levels should go through the Greenhouse interview process (regardless of the number of applicants for the role).
  * Example: Senior Backend Engineer applies to become a Manager, Engineering (interim)  
* Lateral Transfers to a different job family: apply and approval through the Greenhouse [hiring process](/handbook/hiring/).
  * Example: Backend Engineer applies to a Site Reliability Engineering role.

#### Greenhouse Process Requirements

For any transfer being submitted through Greenhouse [hiring process](/handbook/hiring/), the following is required:

- The positions must be open for a minimum of 24 hours
- There must be a minimum of 2 interviews completed before moving to an offer for a candidate

### BambooHR
* Promotions for Individual Contributors in the same job family: reviewed and approved through the [BambooHR Approval Process](/handbook/people-group/promotions-transfers/#bamboohr-promotion-approval-process)
  * Example: Backend Engineer is promoted to Senior Backend Engineer.
* Change in Territory, Segment, or Specialty:
  * Reviewed and approved through the [BambooHR Approval Process](/handbook/people-group/promotions-transfers/#bamboohr-promotion-approval-process) for any change in territory, segment, or specialty when there is no change to the job family or level, there is no compensation or stock adjustment and no backfill request.
  * Apply and approval through the Greenhouse [hiring process](/handbook/hiring/) for any change in territory, segment, or specialty that requires a change in job family or level, change in compensation or stock adjustment or requires a backfill request.
* **Promotions to Director and above**: approval through the [BambooHR Approval Process](/handbook/people-group/promotions-transfers/#bamboohr-promotion-approval-process) to ensure the CFO/CEO have insight and approval into leadership promotions.
    * The [e-group](https://about.gitlab.com/company/team/structure/#e-group) will review any promotion at Director level (or above) in their weekly meeting 3 months before the promotion is intended to take place and feedback may be provided to the individual and their manager to work on. This is to ensure consistent leadership qualities across the company. Exceptions to this timeline can be made at the e-group's discretion. We do not yet have a similar method to ensure consistency in external hires for Director (and above) roles but we hope to add it soon.
    * All Director level (or above) promotions will be reviewed by the [e-group](/company/team/structure/#e-group) on a quarterly basis at the e-group offsite. Proposed promotions will be assessed against the criteria outlined in our [Team Structure](/company/team/structure/), as well as the business need for the promotion, each of the [Values](/handbook/values/), [our competencies](/handbook/competencies/), and considerations.
    * The only exception to this process is when filling an open Director and above role with an existing GitLab team member for a role posted publicly that an internal team member applies for. If external candidates have been considered and interviewed, and the internal candidate earns the role through a standard hiring process (screening, full interview process) then the recruiter may make an offer to the candidate as soon as the offer is approved. There should be no difference in the timing or process of making and accepting an offer for open roles between internal and external candidates.
    * Please [use this template](https://docs.google.com/document/d/1VcQK1Uci2VSnbX0BP8QIYKHcsJi0Fvwl9u79S8W1mpM/edit#heading=h.o7rctrz9uxl7) for Director or above promotions, and share the final version with your People Business Partner so the promotion is added to the tracker for the e-group to review.


## Interim and Acting Roles

### Interim (Engineering-Specific)

As part of the career development structure within the Engineering division, interim and acting role opportunities occasionally arise. For more information on how interim and acting roles fit into Engineering career development, please reference the [Engineering career development handbook page](/handbook/engineering/career-development/). For information on the interim and acting processes, please continue reading below. 

#### Beginning Interim Period

As highlighted in the [BambooHR or Greenhouse Process](https://about.gitlab.com/handbook/people-group/promotions-transfers/#bamboohr-or-greenhouse-process) section, all interim roles (regardless of the number of applicants) should go through the Greenhouse application and interview process. The interview process steps will be determined by the hiring manager and next level leader. This will contain several steps of the standard [GitLab hiring process](https://about.gitlab.com/handbook/hiring/interviewing/#typical-hiring-timeline). The process for team members interested in applying for an interim role is as follows:

1. *Team Member*: Apply for the interim position in Greenhouse.

_Once a team member successfully completes the interview process and is selected for the interim period, the following steps should be taken to ensure the team member is set up for success in their interim role._

2. *Recruiter*: Issue a Letter of Adjustment to finalize the beginning of the interim period. The Letter should include: interim job title, start date, and end date (if known). Letters of adjustment are important as this is the process by which Total Rewards is notified of change from Greenhouse. 

3. *Total Rewards*: Update the team member's job title in BambooHR to reflect that they have started an interim role (I.E. `Senior Manager, Engineering (interim)`). This update serves as the SSOT for tracking interim start and end dates, in addition to providing transparency pertaining to who is currently executing in an interim role. Job code and job grade will remain the same, as interim periods have no impact on compensation.

4. *Current Manager*: A [Job Information Change Request](https://about.gitlab.com/handbook/people-group/promotions-transfers/#job-information-change-in-bamboohr) should be submitted to:
  * Move interim direct reports so the team member's interim direct reports are reporting to them in BambooHR. The change needs to be initiated by the current manager. The philosophy here is that if a team member has successfully gone through the interview process and has demonstrated they are ready/able for an interim period in a manager role, they have the required level of EQ and discretion to have direct reports in BambooHR. It is, of course, expected that should the interim period not end in promotion, the team member continue to treat confidential information confidentially. 


#### Ending Interim Period

When the interim period comes to a close, one of two outcomes can occur:

1. The team member successfully completes the interim period and moves into the interim role permanently. 
* As a general guideline, the interim period should last no _less_ than 30 days, and no _more_ than 4 months
* Because the team member _already_ went through the Greenhouse interview process, there is *no need to submit a promotion document to make the move official*. 
* At the end of the interim period the team member should have a 1:1 with the hiring manager dedicated to providing feedback (strengths during the interim period and improvement areas) to ensure alignment moving forward. 
* Following this session, the hiring manager may [submit a formal promotion request](/handbook/people-group/promotions-transfers/#submit-a-promotion-request-in-bamboohr) via BambooHR to make the change official. 

1. The team member does not complete the interim period successful or decides that the manager track is not something they want to pursue, and moves back to their role prior to the interim period. 
* A feedback session between the team member and hiring manager should take place, so it is clear to the team member why the interim period was not successful.
* The manager should submit a [job information change request](https://about.gitlab.com/handbook/people-group/promotions-transfers/#job-information-change-in-bamboohr) in BambooHR to revert the team member's job title. 

* Not successsfully completing the interim period _does not_ mean the team member can not move into a similar role in the future


_Irrespective of the outcome, when the interim period ends, the manager should review the [Criteria For Eligibility](/handbook/total-rewards/compensation/#criteria-for-eligibility) for the [Interim Bonus](/handbook/total-rewards/compensation/#calculation-of-interim-bonus) and [submit an interim bonus request](/handbook/incentives/#process-for-recommending-a-team-member-for-a-bonus-in-bamboohr) for the team member. 

### Acting

A person "acting" in the role is someone who occupies a role temporarily and will move back to their original role after a set amount of time or other conditions. "Acting" in a role may be experimenting with the role as a part of determining an individual's career development path, or may be filling in for a vacant role while we hire someone to fill the role permanently. While interim is only applicable to the Engineering division, acting is used across GitLab. 

*Interviews are not required role Acting roles as they generally do not end in promotion, nor are direct reports in BambooHR generally moved to Acting managers.* The process for selecting someone for an acting position is:

  * Upcoming Interim roles will be discussed over Staff meetings.
  * Leadership can gather interest from their team members for the upcoming acting roles.
  * The hiring manager will determine the most suitable team member for the acting role. 


_When the acting period ends, the manager should review the [Criteria For Eligibility](/handbook/total-rewards/compensation/#criteria-for-eligibility) for the [Interim Bonus](/handbook/total-rewards/compensation/#calculation-of-interim-bonus) and [submit an interim bonus request](/handbook/incentives/#process-for-recommending-a-team-member-for-a-bonus-in-bamboohr) for the team member._

## For Managers: Requesting a Promotion or Compensation Change

To promote or change compensation for one of your direct reports, do the following steps:

1. [Verify whether this promotion should be approved in BambooHR or Greenhouse](/handbook/people-group/promotions-transfers/#bamboohr-or-greenhouse-process). If BambooHR, continue. If Greenhouse, please reach out to the recruiter on the role.
1. [Create a Promotion or Compensation Change Document](/handbook/people-group/promotions-transfers/#creating-a-promotion-or-compensation-change-document)
1. [Complete a Compa Group Review](/handbook/people-group/promotions-transfers/#complete-a-compa-group-review)
1. [Submit a request in BambooHR](/handbook/people-group/promotions-transfers/#submit-a-promotion-request-in-bamboohr)

**Things to consider before you start the process:**

- There will be situations when an employee is ready to move to the next level through a promotion, however, due to the nature of the business, that particular role or next level may not be available for business reasons. For example, the employee is ready for a Manager or Director role, however, the business does not have the need, budget or scope for an additional manager/director at that time. The position may or may not become available in the future but it is not a guarantee.
- If the vacancy is being advertised via the [jobs page](https://about.gitlab.com/jobs/) the individual must submit an application for the role in order to be compliant with global anti-discrimination laws. Similarly, if there is no vacancy posting, one must be created and shared on the `#new-vacancies` slack channel so that everyone has the opportunity to apply and be considered.

### Creating a Promotion or Compensation Change Document

Create a google doc that outlines the criteria of the role. Make this document viewable and editable by all. If the criteria for promotion is not adequately described on the relevant vacancy description page, work on that first. Do not add compensation values to the google doc - only reasoning. Compensation values go into BambooHR only. Make the google doc accessible to anyone at GitLab who has the link. We have no minimum time for promotions at GitLab.

Tips for creating a promotion document:
* Promote based on performance, not based on potential
* Add evidence - try to remove feelings and use evidence: links to MRs, issues, or project work that clearly demonstrates the skills, knowledge, and abilities of the person.
* Feedback from other team members is _optional_ to include in promotion documents per the [promotion template](https://docs.google.com/document/d/1SavyZeQRXY4fOzv0Y7xMv4TiuWQIf51YtMptr_ZWFQs/edit#). 

**Important Notes**:
* Do not solicit feedback for the purpose of the promotion document as this could be a violation of personal privacy. All feedback included should be feedback provided prior. 
* All feedback included should be anonymized (unless it had already been made public - for example: a discretionary bonus, #thanks Slack channel, etc.)

* [Values](https://about.gitlab.com/handbook/values/) alignment - values are also very important when considering promotions. This is especially the case for anyone who is moving to Senior. As one moves into management, the importance compounds. Values thrive at a company only when we take them into consideration for hiring and promotions.
* For People Management roles - the team member should be prepared for management. This includes, at a minimum, the ability and willingness to provide honest and direct performance feedback and coaching, interviewing candidates with an ability to be decisive, ability to have difficult conversations, the ability to set direction, inspire, and motivate the team.
  * Note: People manager applications must be applied for through [Greenhouse](/handbook/hiring/interviewing/#internal-applicants).

For compensation changes Only:

- Outline the reasons for the compensation increase, also tying the rationale to our values and the vacancy description.

##### General Promotion Document Template

The general promotion document serves as a baseline to outline what should be included in all promotion documents, irrespective of division, department, or role. Additional sections may be included depending on role-specific requirements (for example, in Engineering, it is common to have a "technical abilities" section.)

* [General Promotion Document: Template](https://docs.google.com/document/d/1SavyZeQRXY4fOzv0Y7xMv4TiuWQIf51YtMptr_ZWFQs/edit#)

While this varies, most promotion documents should be roughly 4-6 pages in length (though it is possible to have a solid document with less!). Quality is more important than quantity for promotion documents; 20 pages of documentation should not be necessary if the team member is truly ready for a promotion. 

### Complete a Compa Group Review

At GitLab, we ensure that promotions are impactful from the compensation perspective, stay within range of the compa ratio, and are equitable to peers in the same role. To complete a Compa Group Review, read the breakdown of [Compa Groups](/handbook/total-rewards/compensation/compensation-calculator/#compa-group) and, if it helps, use the Compa Group Worksheet.

Things to note when completing a compa group review:

- When a team member is promoted from one level to the next in the same job family, it is typical to see compa groups reset. For example, if you are an expert in the role as a Frontend Engineer, you would typically be learning in the role as a Senior Frontend Engineer.
- Benchmarking data relays that typically [4-8% of your population will be promoted](https://www.shrm.org/ResourcesAndTools/business-solutions/Documents/Human-Capital-Report-All-Industries-All-FTEs.pdf) annually with an average of a [9.3% increase to base salary](https://www.worldatwork.org/docs/research-and-surveys/survey-brief-promotional-guidelines-2016.pdf).
- Any promotions with the following conditions will require additional justification to the Total Rewards team and executive approver:
    1. An increase of more than 10%
    1. A comp group higher than learning in the role (or growing in the role if the team member served as an interim).
    1. The promotion exceeds the compa ratio (would be paid over the top end of the compensation range).

### Submit a Promotion request in BambooHR

- Login to BambooHR.
- Select the team member you would like to adjust.
- On the top right-hand corner, click “Request a Change”.
- Select which type of change you are requesting.
    - Select “Compensation” if there is a salary change only
    - Select “Promotion” if there is a title change and a salary change involved.
    - Select “Job Information” if there is only a change in title
- Enter in all applicable fields in the form, and then submit.
    - Note: Salary information is entered per payroll. For example, if the team member is in the United States, the annual salary is divided by 24 to get the pay rate to be entered. If the employee is in the Netherlands, please divide by 12.96. The divisor can be found in the "Pay Frequency" field above "Comment for the Approver(s)" in the request form. For any questions on how to fill out the form, please reach out to People Ops Analysts.
- In the comments section please include:
    - The promotion document
    - Your Compa Group Review
    - Any proposed change to variable compensation (if applicable)
- Managers should not communicate any promotion or salary adjustment until the request has gone through the entire approval process and you receive an adjustment letter from Total Rewards Analysts.

### BambooHR Promotion Approval Process

This section describes the approval chain after a manager submits a promotion or compensation change request in BambooHR.

1. The indirect manager will review the promotion in BambooHR.
1. The People Business Partner will then review the business case for promotion and compa group then they will ping the Compensation & Benefits Manager to add the compensation notes (Review the [For People Business Partners](#for-people-business-partners-approving-promotions--compensation-changes) section below for details).
1. The Compensation & Benefits Manager will ensure the proposal adheres to the Global Compensation Calculator, with a comment outlining old compensation, new compensation, increase percentage, additional [stock options](/handbook/stock-options/#stock-option-grant-levels), compa group, job code, and a link to the salary calculator. The People Business Partner will then approve the request if applicable.
1. The E-Group leader will review the entire proposal in BambooHR for approval.
1. The CFO or CEO will review the request. If there are any questions, the CFO or CEO will add a comment outlining the specific concerns and the Total Rewards team will ensure follow-up within a week to escalate to the comp group or deny the request.
1. If the request is approved, the Total Rewards team will process the change on BambooHR and stage the Letter of Adjustment in HelloSign.
1. HelloSign will prompt the manager to discuss the promotion with the team member and announce on the Slack **#team-member-updates** channel where the manager describes how the individual met the promotion criteria and includes a link to the merge request where the individual's title is updated on the team page.
1. When announcing or discussing a promotion on Slack **#team-member-updates** channel, please include a link the promotion Google Doc to increase visibility for the reasons behind the promotion.
1. For change of departments or change of roles (individual contributor to managers), People Experience Associates will create a Career Mobility Issue.

### For People Business Partners: Approving Promotions & Compensation Changes

1. The People Business Partner (PBP) will receive an email titled `Promotion Request: [Team Member Name]`.
1. Confirm the [promotion document](/handbook/people-group/promotions-transfers/#creating-a-promotion-or-compensation-change-document) is included in the request.
1. Review the content of the promotion document.
    - Job family alignment
    - Values alignment
    - While not a requirement, some promotion documents also include: references from other team members and improvement areas
    - Check the sharing settings, as this should be readable by everyone at GitLab with a link
1. Review the Compa Group submitted for knowledge, skills, and ability alignment. The [Compa Group Worksheet](/handbook/total-rewards/compensation/compensation-calculator/#determining) is optional, but often in Engineering used as a tool for the manager to determine the compa group. Additionally check:
    - Bonuses received by the team member
    - Current Compa Group (I.E. Typically before moving to promotion the team member should be `thriving` in their current role)
1. Ping the manager requesting the promotion via Slack to confirm that the department leader and E-Group leader are aware and supportive of the promotion.
1. Align with the `Manager, Total Rewards` in the private #promo_compensation Slack channel to ensure Compensation notes are added in BambooHR.
1. Cross-check Compensation adjustment in accordance with the [compensation calculator](/handbook/total-rewards/compensation/compensation-calculator/calculator/).
1. We want for promotions and compensation changes in Engineering that they are approved by all leaders up to the EVPE. As BambooHR has a limit in the amount of approvers use the Division specific slack channel to have approval of all leaders.
1. If an approver is skipped in BambooHR: Share the promotion information in the private Division-specific promotion Slack channel with an overview of:
    - Team member name
    - Promotion request (current job title and new job title)
    - Compensation notes (current compensation, proposed compensation, increase %, additional stock options, compa group, compensation calculator)
1. `@mention` the skipped department leader for approval in the Division-specific promotion Slack channel. Example: Promotion for Backend Engineer to Senior Backend Engineer. The BambooHR process includes the direct manager, department Director and EVPE. The missing department leader here is the VP of Development. In this case `@mention` the VP of Development for approval.
1. PBP approves the promotion request in BambooHR.

### For Total Rewards: Processing Promotions & Compensation Changes

1. If the request is approved through BambooHR, the Total Rewards Analyst/Coordinator will create the Letter of Adjustment whereas if the request is through Greenhouse the Total Rewards Analyst/Coordinator will be notified via the Total Rewards team email inbox that the letter has been signed. If this is the case, only data systems will need to be updated.
1. If the request comes through BambooHR, approve the request, then update the entries in BambooHR to ensure that there are the proper dates, amounts and job information. Also, ensure to add stock options to the benefits tab if applicable. If the team member is moving to a Manager position, update their access level in BambooHR.
1. Notify Payroll of the changes. This can be done in the following google docs: United States: "Payroll Changes", Everyone else: "Monthly payroll changes for non-US international team members". Payroll does not need to be notified for Contractors.
1. Update the compensation calculator backend spreadsheet.
1. If the team member is in Sales or transferred to Sales, update the changes tab on the "Final Sales OTE FY 2020" google doc.
1. Make a copy of the Letter of Adjustment template and enter all applicable information based on the BambooHR request. The effective date is as follows:
    - For sales personnel with a variable change, the effective date is always the 1st of the month regardless of their entity.
    - For US team members, the effective date should be either the 1st or the 16th. If the [payroll cut off date](/handbook/finance/payroll/#payroll-cut-off-date) has passed for the current pay period, the effective date should be made for the start of the next pay period. The GitLab Inc and Federal Payroll calendar should be referenced when determining the effective date.
        - For example, if the change is being processed on June 22, since this date is before the payroll cut off date of June 23, the effective date should be June 16.
        - If the change instead is being processed on June 25, the effective date should be July 1 since this is after the payroll cut off date.
    - For Canada team members, the effective should be the start of the pay period closest to, but not after the [payroll cut off date](/handbook/finance/payroll/#payroll-cut-off-date) depending on when the change is processed. The GitLab Canada Corp Payroll calendar should be referenced when determing the effective date.
        - For example, if the change is being processed on June 15, since the payroll cut off date of June 6 has passed, this would go to the next pay period with cut off date of June 20. The corresponding start of the pay period for the June 20 cut off date is June 21 so June 21 should be the effective date.
    - For all other changes, the effective date should be the first of the current month if processed on or before the 8th of the month and the first of the next month if processed after the 8th of the month.
        - For example, if a GitLab Ltd team member has a change being processed on June 7, this would be effective June 1.
        - If the change was instead being processed on June 15, this would be effective July 1.
1. Stage the letter in HelloSign and add the following team members to sign:
    - Add checkbox for the Total Rewards Analyst to audit
    - Add checkbox for the Manager to communicate the change to the team member and announce at the company calculator
    - Add signature field for the Compensation and Benefits Manager
    - Add signature field for the team member
    - Add sign date field for the team member
    - **Note:** Make sure that **a)** "Assign signer order” option has been selected while preparing the doc, and **b)** In **Settings >> Profile >> Send an email to all parties in an ordered signature request when the request has started** option is unchecked.
1. Once signed by all parties, save the letter to the “Contracts & Changes” folder in BambooHR.
1. If some amount of onboarding in the new role or offboarding from the old role is required (for example a change in access levels to infrastructure systems; switch in groups and email aliases, etc.), notify People Experience Associates in the internal Promotions/Transfers spreadsheet tracker (using the `people-exp@gitlab.com` alias) and the People Experience Associates will create an associated [Career Mobility Issue](https://gitlab.com/gitlab-com/people-group/employment-templates/-/blob/master/.gitlab/issue_templates/career_mobility.md) with the [Slack Command](/handbook/people-group/engineering/#internal-transition-issue-creation) list and track associated tasks for the previous and new manager. Tag or mention the aligned [People Business Partner](/handbook/people-group/#people-business-partner-alignment-to-division) in the Career Mobility Issue.

1. The previous manager will be prompted to create an [Access Removal Request Issue](https://gitlab.com/gitlab-com/team-member-epics/access-requests/blob/master/.gitlab/issue_templates/Access%20Removal%20Request.md) and the new manager will create an [Access Request Issue](https://gitlab.com/gitlab-com/team-member-epics/access-requests) to ensure the correct access is given for the new role and deprovisioned for the previous role, if need be.

## Demotions

To demote one of your direct reports, a manager should follow the following steps:

- The manager should discuss any performance issues or possible demotions with the People Business Partner in their scheduled meetings with a corresponding google doc.
- To initiate the process, the manager must obtain agreement from two levels of management.
- Proposed changes to a current vacancy description or a new vacancy description should be delivered with request for approval by the second level manager and the People Ops Manager.
- Demotions should also include a review of [compensation](/handbook/total-rewards/compensation/) and [stock options](/handbook/stock-options/#stock-option-grant-levels) in the google doc. Managers should consult with Total Rewards team on these topics; and of course always adhere to the Global Compensation Calculator.
- Once agreement is reached on the demotion and changes (if any) in compensation, the total rewards team will act as the point of escalation to have any demotion reviewed and approved by the Compensation Group once the relevant google doc is complete.
- Once approved, the manager informs the individual. Please cc total-rewards@ gitlab.com once the individual has been informed, to processes the changes in the relevant administrative systems, and stage a [Letter of Adjustment](/handbook/people-group/contracts-and-international-expansion/#letter-of-adjustment).
- Changes in title are announced on the `#team-member-updates` Slack channel.
- The manager will initiate any necessary onboarding or offboarding.

## Job Information Change in BambooHR

Job information changes are anything that requires an update to the team member's profile in BambooHR that is not compensation related. The current manager is the person who needs to submit all job information change requests (including requests to change team member's manager).

**Process for the current manager:**

1. Login to Bamboo HR and select the direct report
1. In the Request Change dropdown, select Job information
    - Under "Effective Date" input the date the transfer to the new manager is effective.
    - Under "Department" input the new (if applicable) Department
    - Under "Reports To" search and select the name of the Hiring Manager
    - Under "Comment for Approvers" paste any additional relevant information.
    - Update the `Job Title Specialty` field (if applicable)

### For Total Rewards: Processing Job Information Change Requests

1. Audit the team member's department, division and cost center against the new manager's.
1. For US team members, the entity must also match with the manager's. For any changes in entity, update the payroll file.
1. Update the Expensify Job Information Changes sheet.
1. In case of `Job Title Specialty` change requests. Notify People Experience Associates in the internal Transition Tracker spreadsheet tracker (using the `people-exp@gitlab.com` alias) and the People Experience Associates will create an associated [Career Mobility Issue](https://gitlab.com/gitlab-com/people-group/employment-templates/-/blob/master/.gitlab/issue_templates/career_mobility.md) with the [Slack Command](/handbook/people-group/engineering/#internal-transition-issue-creation) list and track associated tasks for the previous and new manager. Tag or mention the aligned [People Business Partner](/handbook/people-group/#people-business-partner-alignment-to-division) in the Career Mobility Issue.

## Department Transfers

If you are interested in a vacancy, regardless of level, outside your department or general career progression, you can apply for a transfer through [Greenhouse](https://boards.greenhouse.io/gitlab) or the internal job board, link found on the #new-vacancies Slack channel.
You will never be denied an opportunity because of your value in your current role. We have no minimum time in role requirements for transfers at GitLab.

### For Internal Applicants - Different Job Family

- If you are interested in a transfer, simply submit an application for the new position. If you are not sure the new role is a good fit, schedule time with the hiring manager to learn more information about the role and the skills needed. If after that conversation you are interested in pursuing the internal opportunity, it is recommended that you inform your current manager of your intent to interview for a new role. While you do not need your their permission to apply to the new role, we encourage you to be transparent with them. Most will appreciate that transparency since it's generally better than learning about your move from someone reaching out to them as a reference check. You can also use this as an opportunity to discuss the feedback that would be given to the potential new manager were they to seek it regarding your performance from your current and/or past managers. We understand that the desire to transfer may be related to various factors. If the factor is a desire NOT to work with your current manager, this can be a difficult conversation to have and shouldn't prevent you from pursuing a new role at GitLab.
- Transfers must go through the application process for the new position by applying on the [jobs page](https://gitlab.greenhouse.io/internal_job_board). The team member will go through the entire interview process outlined on the vacancy description, excluding behavioral or "values alignment" interviews. If you have any questions about the role or the process, please reach out to your Department or Division's [People Business Partner](/handbook/people-group/#people-business-partner-alignment-to-division). In all cases, the applicable People Business Partner should be informed via email, before a transfer is confirmed.
- In the case of transfers, it is expected and required that the gaining manager will check with internal references at GitLab, including previous and current managers.
- Before the offer is made the recruiter will confirm with the team member and the gaining manager that they have indeed reached out to the current manager. They will discuss the new internal opportunity and that an offer will be made to the team member.
- Recruiting team will ensure that, if applicable, the position has been posted for at least three business days before an offer is made.
- [Compensation](/handbook/total-rewards/compensation/) and [stock options](/handbook/stock-options/#stock-option-grant-levels) may be reviewed during the hiring process to reflect the new level and position.
- If after interviews, the manager and the GitLab team-member want to proceed with the transfer, internal references should be checked. While a manager cannot block a transfer, there is often good feedback that can help inform the decision. It is advised that the GitLab team-member talk to their manager to explain their preference for the new team and to understand the feedback that will be given to the new manager. It should also be noted, that performance requirements are not always equal across roles, so if a GitLab team-member struggles in one role, those weakness may not be as pronounced in the new role, and vice versa. However, if there are systemic performance problems unrelated to the specific role or team, a transfer is not the right solution.
- If the GitLab team-member is chosen for the new role, the managers should agree on an reasonable and speedy transfer plan. 2 weeks is a usually a reasonable period, but good judgment should be used on completing the transfer in a way that is the best interest of the company, and the impacted people and projects.
- The Recruiter and Hiring Manager will review the offer details with the internal candidate and a [letter of adjustment](/handbook/contracts/#letter-of-adjustment) will be sent out following the hiring process
- If the team member is transferred, the new manager will announce in the `#team-member-updates` Slack Channel and begin any additional onboarding or offboarding necessary. Before the new manager makes the transfer announcement they must confirm with the team members current manager that the current team has been informed about the team members new position and transfer.
- Team members changing functional roles should complete onboarding for the new function. For example, a Backend Engineer who transferring to become or work on Frontend work should do Frontend Engineer onboarding.

### For Internal Applicants - Same Job Family, Different Department or Specialty

If the team member is staying in the current benchmark for the Job Family, but changing their Specialty or Department (ex: moving from Plan to Secure or moving from Development to Infrastructure), the above steps will be followed. Solely the recruitment procedure might be shortened if the requirements for the role are the same. At a minimum we would ask for the hiring manager to have an interview with the team member.

If selected for the role, a [letter of adjustment](/handbook/contracts/#letter-of-adjustment) will be sent outlining the changes to department and specialty for the Total Rewards team to process in BambooHR. If the current manager needs to backfill the role they should reach out to the Finance Partner.

## Department Transfers Manager Initiated

If you are a manager wishing to recruit someone, the process is the same as a team member initiated transfer. We encourage the hiring manager to be transparent with the team member's current manager. This will allow the current manager the maximal amount of time to plan for the transfer and speed up the overall process.

### Internal Department Transfers

If you are interested in another position within your department and the manager is also your manager you must do the following;

- Present your proposition to your manager with a google doc.
- If the vacancy is advertised on the [jobs page](/jobs/), to be considered, you must submit an application. If there is no vacancy posting, one must be created and shared in the #new-vacancies channel so that everyone has the opportunity to apply and be considered.
- The manager will asses the function requirements; each level should be defined in the vacancy description.
- If approved, your manager will need to obtain approval from their manager, through the chain of command to the CEO.
- [Compensation](/handbook/total-rewards/compensation/) and [stock options](/handbook/stock-options/#stock-option-grant-levels) will be reevaluated to ensure it adheres to the compensation calculator. Don't send the proposal to the CEO until this part is included.
- If the team member is transferred, the manager will announce in the `#team-member-update` Slack channel and begin any additional onboarding or offboarding necessary.

#### When promotion is a consideration - Within Same Job Family

If a team member sees a vacancy posted that is the next level up within their [job family](/handbook/hiring/job-families/#job-families) (for example an Intermediate Frontend Engineer sees a vacancy for an Senior Frontend Engineer), the team member should have a conversation with their manager about exploring that opportunity. Once that discussion is completed the team member should follow the internal department transfers guidance above.

It is the manager’s responsibility to be honest with the team member about their performance as it relates their promotion readiness. If the manager agrees that the team member is ready, then they will be promoted to the next level. If they do not think the team member is ready for the promotion, they should walk through the compa group and their career development document, as well as work on a promotion plan with the team member. The manager should be clear that the team member is not ready for the promotion at this time and what they need to work on. If the team member would still like to submit an application for the role after the conversation with their manager, they can apply and go through the same interview process as external candidates. The recruiter will confirm with the manager that the promotion readiness conversation has taken place before the internal interview process starts.

#### For Internal Applicants - Different Job Family

If the role is in a completely different job family (within their own division or in a completely different division, for example, if a Product Designer is interested in a Product Manager role), the team member must submit an application via the posting on GitLab’s [internal job board](https://app2.greenhouse.io/internal_job_board) on Greenhouse.

After the team member applies, the recruiter will reach out to the team member to connect regarding compensation for the role. In some cases, the compensation may be lower than the current one. Once the team member understands and agrees with the compensation for the new role, they can continue the interview process.

Internal and external candidates will have the same process with the same amount of interviews and when possible the same interviewers, with the exception of the full screening call (which will be instead a short conversation to discuss compensation, as mentioned above). However, if the internal applicant will be staying in the same division and the executive level interview is a part of the process, the executive may choose to skip their interview. All interview feedback and notes will be captured in the internal team member’s Greenhouse profile, which will be automatically hidden from the team member. After interviews are completed, internal “reference checks” will be completed with the applicant’s current manager by the new hiring manager.

It is recommended that team members inform their manager of their desire to move internally and their career aspirations. Your manager should not hear about your new opportunity from the new hiring manager; it should come from you prior to the new hiring manager checking in for references with the current manager.

If you are unsure of the role, set up a coffee chat with the hiring manager to introduce yourself. Express your interest in the role and your desire to learn more about the vacancy requirements and skills needed. If after that conversation you do not feel that you are qualified or comfortable making the move, ask the hiring manager to provide guidance on what you can do to develop yourself so you will be ready for the next opportunity. It may also be possible to set up an [internship for learning](/handbook/people-group/promotions-transfers/#internship-for-learning) situation with the hiring manager.

#### Announcing Internal Promotions/Transfers

While the [Internal Transition Issue](/handbook/people-group/promotions-transfers/#internal-transition-issue) aims to kick off the logistics of switching roles, the guidelines below are meant to guide the communication of internal promotions and transitions to ensure consistency and alignment from all parties involved.

1. Prior to any company-wide announcement, the team member should be given the opportunity to share the news with their immediate team members.
1. The `new manager` should post the announcement in the `#team-member-updates` Slack channel. This should ideally happen (timezome permitting) on the <b>same day that the candidate signs their [Letter of Adjustment](/handbook/contracts/#letter-of-adjustment)</b>.
1. For cases where announcing on the same day the Letter of Adjustment is signed is not possible, the announcement should no more than 24 hours after the candidate has signed.
1. Following this initial announcement, the `current manager` can proceed with making this announcement in other relevant team-specific channels.

_NOTE: Though the Total Rewards and People Experience team may have visibility into promotions or transfers due to the administration of updating information as part of their roles, they should not communicate with the team member about their promotion/tranfer until an announcement has been made._--

#### Realignment of Resources impacting multiple team members

Company priorities can change and occasionally some or all members of a team may be asked to transfer to high priority vacancies within other teams.

In cases where multiple individuals are asked to transfer to high priority roles:

1. Legal counsel and the People Business Partner for the group should be notified to align on the process and impact of the resources realignment.
1. Select a DRI to coordinate the overall realingment process. This DRI should work closely with their People Business Partner to ensure we comply with all local labor laws.
1. Communicate the reassignment decision to effected team members. Emphasize this is not about poor performance, but rather a way to shift high value individuals to the highest priorities.
1. Organize one or more Team Pitch Office Hours meeting where individuals considering transfers can learn about teams that are hiring. Hiring managers should attend the office hours to talk about what is interesting about their teams. [Team Pitch Office Hours video](https://www.youtube.com/watch?v=-MgiUA7sAHU&feature=youtu.be)
1. Encourage individuals considering transfers to meet with hiring managers to get more information about the roles they are interested in.
1. Ask for and record the role each individual's transfer preference. Also ask for their second choice and third choice.
1. After individuals gave their preference, the skills/requirements of the roles will be matched to the skills of the individuals. For example: level, compa group, product/technical skills, potentially soft skills.
1. DRI work with People Business Partner to ensure all legal requirements are met as these vary between countries.
1. Ask hiring manager to approve transfer. If they don't approve look at the individual's first or second choice.
1. Follow the normal process for recording and announcing each transfer.

#### For People Success & Recruiting Team

Vacancies will be posted internally using the Greenhouse internal job board for at least 3 business days. If a role is not posted internally there must be a business case documentd in the HRIS file of the team member who received the new role. See [Department Transfers](/handbook/people-group/promotions-transfers/#department-transfers) for additional details.

More details can be found on the [Intneral Applications](/handbook/hiring/interviewing/#internal-applicants) page and the [Letter of Adjustment (LOA)](/handbook/people-group/contracts-and-international-expansion/#letter-of-adjustment) page.

### Leveling Up Your Skills

There are a number of different ways to enhance or add to your skill-set at GitLab, for example, if you do not feel like you meet the requirements for an inter-department transfer, discuss with your manager about allocating time to achieve this. This can be at any level. If you're learning to program, but aren't sure how to make the leap to becoming a developer, you could contribute to an open-source project like GitLab in your own time.

As detailed in our [Learning & Development Handbook](/handbook/people-group/learning-and-development/), team members will be supported in developing their skills and increasing their knowledge even if promotion is not their end goal.

### Internship for Learning

If your manager has coverage, you can spend a percentage of your time working (through an 'internship') with another team.

This could be for any reason: maybe you want to broaden your skills or maybe you've done that work before and find it interesting.

If your team has someone working part-time on it, it's on the manager of that team to ensure that the person has the support and training they need, so they don't get stuck. Maybe that's a buddy system, or maybe it's just encouragement to use existing Slack channels - whatever works for the individuals involved.

#### How does this work?

**What percentage of time should be allocated?** Well, 10% time and 20% time are reasonably common. As long as you and your manager have the capacity the decision is theirs and yours.

**What about the team losing a person for X% of the time? How are they supposed to get work done?**
Each manager needs to manage the capacity of their team appropriately. If all of the team are 'at work' (no one is on PTO, or parental leave, or off sick), and the team still can't afford X% of one person's time - that team might be over capacity.

**Can I join a team where I have no experience or skills in that area?**
That's up to the managers involved. It may be that the first step is to spend some time without producing anything in particular - in which case, it's possible that the [tuition reimbursement policy](/handbook/total-rewards/benefits/general-and-entity-benefits/#tuition-reimbursement) may be a better fit (or it might not.)

**How long does an internship of this nature last?**
This will vary from team to team, but typically 6 weeks to 3 months depending on the goals for your internship.

**This sounds great but how do I initiate this process?**
First step is to discuss this with your manager at your next 1:1. Come prepared with your proposal highlighting what skills you want to learn/enhance and the amount of time you think you will need. Remember, this should be of benefit to you and GitLab. You and your manager will need to collaborate on how you both can make this happen which may also involve discussing it further with the manager of the team you may be looking to transfer to. All discussions will be done transparently with you. Be mindful though that the business needs may mean a move can't happen immediately.

**How do I find a mentor?**
On the [team page](/company/team), you can see who is willing to be a mentor by looking at the associated [expertise](/company/team/structure/#expert) on their entry.

**Does completing an internship guarantee me a role on the team?**
Completing an internship through this program does not guarantee an internal transfer. For example, there may not be enough
allocated headcount in the time-frame in which you complete your internship.

If at the end of your internship, you are interested in transferring teams please follow the guidelines in [Internal Department Transfers](#internal-department-transfers).

#### Starting Your New Internship

Please create a new issue in the [Training project](https://gitlab.com/gitlab-com/people-ops/Training/issues/new), choose the `internship_for_learning` template, and fill out the placeholders.

No internship for learning should be approved without both managers having a conversation and agreeing upong the percentage of time the team member will spending on the internship. Also, the team members manager may has discretion not to approve the internship for learning if there are team member performance issues.

Once you've agreed upon the internship goals, both managers should inform their respective groups' People Business Partner.

#### Recommendations

We recommend that, at any given time, each [team](/company/team/structure/#team-and-team-members) is handling only one intern. This is to allow for an efficient and focused mentorship without impacting the capacity of the team. You can, of course, adjust this depending on the size of the team but please consider the impact of mentoring when scheduling internships.
