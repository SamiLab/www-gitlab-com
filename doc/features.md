# Update the features

All features are listed in a single yaml file
([`/data/features.yml`](/data/features.yml)) under the `features` section.
It is the single source of truth for the following pages:

- <https://about.gitlab.com/features/>
- <https://about.gitlab.com/products/>
- <https://about.gitlab.com/gitlab-com/>
- <https://about.gitlab.com/devops-tools/>
- <https://about.gitlab.com/roi/>

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Update the features](#update-the-features)
  - [Features attributes](#features-attributes)
    - [Badges](#badges)
  - [Update the features page (under `/features`)](#update-the-features-page-under-features)
  - [Update the pricing page (under `/pricing`)](#update-the-pricing-page-under-pricing)
  - [Create or update the comparison pages (under `/devops-tools`)](#create-or-update-the-comparison-pages-under-devops-tools)
  - [Create or update the stage pages (under `/stages`)](#create-or-update-the-stage-pages-under-stages)
  - [Update the Return on Investment calculator page (under `/roi`)](#update-the-return-on-investment-calculator-page-under-roi)
  - [Not yet implemented features](#not-yet-implemented-features)
  - [Why we use YAML for the stages, features, and pricing pages](#why-we-use-yaml-for-the-stages-features-and-pricing-pages)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

---

## Features attributes

These instructions are deprecated. View up-to-date instructions for [adding features to webpages](https://about.gitlab.com/handbook/marketing/website/#adding-features-to-webpages) on the website handbook page.

### Badges

These instructions are deprecated. View up-to-date instructions for [adding features to webpages](https://about.gitlab.com/handbook/marketing/website/#adding-features-to-webpages) on the website handbook page.

## Update the features page (under `/features`)

These instructions are deprecated. View up-to-date instructions for [adding features to webpages](https://about.gitlab.com/handbook/marketing/website/#adding-features-to-webpages) on the website handbook page.

## Update the pricing page (under `/pricing`)

These instructions are deprecated. View up-to-date instructions for [adding features to webpages](https://about.gitlab.com/handbook/marketing/website/#adding-features-to-webpages) on the website handbook page.

## Create or update the comparison pages (under `/devops-tools`)

These instructions are deprecated. View up-to-date instructions on [creating comparison pages](https://about.gitlab.com/handbook/marketing/website/#creating-a-devops-tools-comparison-page) on website handbook page.

## Create or update the stage pages (under `/stages`)

WARNING outdated info. New instructions need to be added to the website handbook.

Every feature should have a stage. The list of the existing stages can be
found at [`data/stages.yml`](/data/stages.yml).

To create a new stage:

1. Edit [`data/stages.yml`](/data/stages.yml) and add the new stage.
   You can copy the format of an existing one.

    >**Note:**
    There are two categories of stages: 1) "Phases of the software development
    lifecycle" and 2) "Quality attributes of GitLab". The features index page
    only lists the "Phases of the software development lifecycle". They are
    taken from [`data/stages.yml`](/data/stages.yml), and any stage with
    `marketing` set to false is ignored so if that changes, make sure to also
    update the code in `source/features/index.html.haml`
    (`data.stages.stages.select{|stageKey,stage| stage.marketing}.each`).

1. Create `source/stages/<stage-name>/index.html.haml`. You can copy the
   format of an existing stage to get started.

To update an existing stage, just edit [`data/stages.yml`](/data/stages.yml).

## Update the Return on Investment calculator page (under `/roi`)

The [`/roi`](https://about.gitlab.com/roi/) page grabs its content
automatically from [`/data/features.yml`](/data/features.yml).

Consult the [features attributes table](#features-attributes) for a complete
list of possible attributes.

## Not yet implemented features

Features that are not yet implemented and not yet scheduled,
but are interesting to GitLab, should be documented in `features.yml`.
Include at least the following attributes:

```
- title
- description
- link_description
- link (Link to the issue)
- stage: missing
```

The `stage: missing` attribute and value will display the feature in the
Missing section of <https://about.gitlab.com/features/>.

## Why we use YAML for the stages, features, and pricing pages

After attempting to maintain the features, pricing, and
stages pages independently of one another, we decided to use YAML. This didn't work well because the
pages often had overlapping content, and the site wasn't following
[DRY principles](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself). Without
being DRY, we frequently repeated work unnecessarily and the pages became increasingly
difficult to maintain, especially with our rapid release schedule.

To prevent this from happening again, we want to emphasize these problems
and have people update the YAML files instead of the respective Haml files.

For more information, see issues [#1334](https://gitlab.com/gitlab-com/www-gitlab-com/issues/1334)
and [#1478](https://gitlab.com/gitlab-com/www-gitlab-com/issues/1478).
